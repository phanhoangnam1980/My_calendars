package com.example.calendars.domain;

import java.time.format.DateTimeFormatter;
import javax.persistence.Column;

import com.example.calendars.domain.model.TaskMessage;

public class TaskModelex {
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E MMM dd yyyy HH:mm:ss xx");

	private Long taskid;

	@Column(nullable = false, length = 500)
	private String Title;

	@Column(length = 1000)
	private String Description;

	private String date;

	@Column(nullable = false)
	private Long UserID;

	@Column(nullable = false)
	private String IsDeleted;
	
	private int comments;
	
	private int subtask;

	public Long getTaskid() {
		return taskid;
	}

	public void setTaskid(Long taskid) {
		this.taskid = taskid;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Long getUserID() {
		return UserID;
	}

	public void setUserID(Long userID) {
		UserID = userID;
	}

	public String isIsDeleted() {
		return IsDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		IsDeleted = isDeleted;
	}

	public int getComments() {
		return comments;
	}

	public void setComments(int comments) {
		this.comments = comments;
	}

	public int getSubtask() {
		return subtask;
	}

	public void setSubtask(int subtask) {
		this.subtask = subtask;
	}

	public TaskModelex() {
		super();
	}

	public TaskModelex(TaskMessage taskdata) {
		super();
		this.taskid = taskdata.getTaskid();
		Title = taskdata.getTitle();
		Description = taskdata.getDescription();
		UserID = taskdata.getUserID();
		if (taskdata.getIsDeleted() != null) {
			IsDeleted = formatter.format(taskdata.getIsDeleted());
		}
		date = formatter.format(taskdata.getDate());
		this.comments = taskdata.getComments();
		this.subtask = taskdata.getSubcount();
	}

}
