import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {TaskMessage} from '../model/TaskMessage';

@Injectable()
export class ShareDataService implements OnDestroy {
  private isCancelRequestNewTask: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  currentCancelRequestNewTask = this.isCancelRequestNewTask.asObservable();
  private isCancelRequestNewSection: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  currentCancelRequestNewSection = this.isCancelRequestNewSection.asObservable();
  private isCompleteAddTask: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  currentStateAddTask = this.isCompleteAddTask.asObservable();
  private addTask: BehaviorSubject<TaskMessage> = new BehaviorSubject<TaskMessage>(null);
  DataAddTask = this.addTask.asObservable();
  private addProject: BehaviorSubject<any> = new BehaviorSubject<any>(false);
  DataAddProject = this.addProject.asObservable();
  private addSection: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  DataAddSection = this.addSection.asObservable();
  private newUsername: BehaviorSubject<any> = new BehaviorSubject<any>(false);
  AddNewUsername = this.newUsername.asObservable();
  private newPassword: BehaviorSubject<any> = new BehaviorSubject<any>(false);
  AddNewPassword = this.newPassword.asObservable();
  private newEmail: BehaviorSubject<any> = new BehaviorSubject<any>(false);
  AddNewEmail = this.newEmail.asObservable();
  // add Action Add Task to close
  private newDataAddByModal: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  AddTaskByModal = this.newDataAddByModal.asObservable();
  // cancel edits task
  private CancelEditsTask: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  CurrentCancelStatus = this.CancelEditsTask.asObservable();
  // send title
  private UpdateChannel: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  CurrentStatusUpdate = this.UpdateChannel.asObservable();

  ngOnDestroy(): void {
    this.isCancelRequestNewTask.unsubscribe();
    this.isCancelRequestNewSection.unsubscribe();
    this.isCompleteAddTask.unsubscribe();
    this.addProject.unsubscribe();
    this.addTask.unsubscribe();
    this.addSection.unsubscribe();
    this.newUsername.unsubscribe();
    this.newPassword.unsubscribe();
    this.newEmail.unsubscribe();
    this.newDataAddByModal.unsubscribe();
    this.CancelEditsTask.unsubscribe();
    this.UpdateChannel.unsubscribe();
  }

  setCancelRequestNewTask(CheckRequestFriend: boolean) {
    this.isCancelRequestNewTask.next(CheckRequestFriend);
  }

  setCancelRequestNewSection(CheckRequestFriend: boolean) {
    console.log(CheckRequestFriend);
    this.isCancelRequestNewSection.next(CheckRequestFriend);
  }

  setAddNewUsername(requestUser: boolean) {
    this.newUsername.next(requestUser);
  }

  setAddNewPassword(requestUser: boolean) {
    this.newPassword.next(requestUser);
  }

  setAddNewEmail(requestUser: boolean) {
    this.newPassword.next(requestUser);
  }

  setAddTask(CheckRequestFriend: boolean) {
    this.isCompleteAddTask.next(CheckRequestFriend);
  }

  setDataAddTask(DataAddTask: TaskMessage) {
    this.addTask.next(DataAddTask);
  }

  setAddProject(projectAdd: any) {
    this.addProject.next(projectAdd);
  }

  SetAddSection(sectionAdd: any) {
    this.addSection.next(sectionAdd);
  }

  SetAddDataByModal(flagAdd: boolean) {
    this.newDataAddByModal.next(flagAdd);
  }

  SetCancelStatus(status: boolean) {
    this.CancelEditsTask.next(status);
  }

  SetUpdateChannelStatus(status: boolean) {
    this.UpdateChannel.next(status);
  }
}
