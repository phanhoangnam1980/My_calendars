import {Component, OnInit} from '@angular/core';
import {User} from '../model/User';
import {Router} from '@angular/router';
import {SignupService} from '../service/Signup.service';
import {TranslocoService} from '@ngneat/transloco';
import {GoogleSigninService} from '../service/google-signin.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  user: User;
  public messages: string;
  public error: string;
  step = 1;
  public googleuser: gapi.auth2.GoogleUser;

  constructor(private signupService: SignupService, private router: Router, private translocoService: TranslocoService,
              private googleSignInService: GoogleSigninService) {
    this.user = new User();
  }

  ngOnInit(): void {
    this.signupService.getRegistration();
  }

  register() {
    if (this.validateBeforeSend()) {
      this.signupService.postRegistration(this.user).subscribe(isValid => {
        console.log(isValid.text === 'User registered successfully');
        if (isValid) {
          this.router.navigate(['login']).then();
        } else {
          this.translocoService.selectTranslate('registerError').subscribe(translate => {
            this.error = translate;
          });
        }
      });
    }
  }

  next() {
    const regex = new RegExp('^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$');
    const validation = regex.test(this.user.email);
    console.log(validation);
    if (this.user.email == null) {
      this.translocoService.selectTranslate('EmailValidatorNull').subscribe(translate => {
        this.messages = translate;
      });
    } else if (!validation) {
      this.translocoService.selectTranslate('EmailValidatorError').subscribe(translate => {
        this.messages = translate;
      });
    } else {
      this.messages = null;
      while (this.step < 2) {
        this.step++;
      }
    }
  }

  previous() {
    this.step--;
  }

  validateBeforeSend() {
    if (this.user.password.length < 8) {
      this.translocoService.selectTranslate('passwordShort').subscribe(translate => {
        this.error = translate;
      });
      return false;
    } else if (this.user.password.length > 32) {
      this.translocoService.selectTranslate('passwordTooLong').subscribe(translate => {
        this.error = translate;
      });
      return false;
    } else if (this.user.password !== this.user.passwordConfirm) {
      this.translocoService.selectTranslate('ConfirmError').subscribe(translate => {
        this.error = translate;
      });
      return false;
    } else {
      this.error = null;
      return true;
    }
  }

  signInWithGoogle() {
    this.googleSignInService.signIn();
  }
}
