package com.example.calendars.firebase;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.calendars.domain.CustomImageModel;
import com.example.calendars.domain.CustomImageTask;
import com.example.calendars.domain.Response;
import com.example.calendars.domain.TaskGroup;
import com.example.calendars.domain.model.Images;
import com.example.calendars.domain.model.Section;
import com.example.calendars.domain.model.User;
import com.example.calendars.service.TaskService;
import com.example.calendars.service.UserService;
import com.example.calendars.validator.TypeValidator;

@RestController
public class ImageController {

	@Autowired
	private IImageService imageService;
	@Autowired
	private TypeValidator typeImageValidator;
	@Autowired
	private TaskService taskService;
	@Autowired
	private UserService userService;

	@PostMapping("/image")
	public Response SendImage(@RequestParam("imageFile") MultipartFile files) {
		Response result = new Response();
		Images storage = new Images();
		typeImageValidator.ValidatorTypeImage(files.getContentType(), result);
		if (result.getCode() == 0) {
			storage = imageService.UploadToStorage(files, files.getContentType());
			return new Response(HttpStatus.OK, "AlreadyDone", "", storage);
		}
		return result;
	}

	@PostMapping("/testImage")
	public void testImage() {
		// imageService.UploadToStorage("bHZAuNZ.jpg","image/jpeg");
	}

	@PostMapping("/avatar")
	public Response ImageData(@RequestParam("avatar") MultipartFile files) {
		Response result = new Response();
		Images storage = new Images();
		typeImageValidator.ValidatorTypeImage(files.getContentType(), result);
		if (result.getCode() == 0) {
			storage = imageService.ImageUploadAvatar(files);
			return new Response(HttpStatus.OK, "AlreadyDone", "", storage);
		}
		return result;
	}

	@GetMapping("/taskImage")
	public List<CustomImageModel> ImageTaskSelect() {
		int countContinue = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		List<CustomImageModel> result = new ArrayList<>();
		List<CustomImageTask> rawImage = imageService.GetListImageByTask(CurrentUser.getId());
		List<TaskGroup> rawTask = taskService.ListTaskHaveComment(CurrentUser.getZonetime());
		for (int i = 0; i < rawImage.size(); i++) {
			for (int j = countContinue; j < rawTask.size(); j++) {
				if (rawImage.get(i).getTaskid() == rawTask.get(j).getData().getTaskid()) {
					if (j != 0) {
						countContinue = j - 1;
					} else {
						countContinue = 0;
					}
					Section current = new Section();
					String[] getdata = rawTask.get(j).getData().getTiterGroupTask().split("/");
					if (getdata.length == 2) {
						current.setId(rawTask.get(j).getData().getSectionId());
						current.setTitle(getdata[1].toString());
					} else {
						current.setId(rawTask.get(j).getData().getSectionId());
						current.setTitle("");
					}
					result.add(new CustomImageModel(rawImage.get(i).exportToImage(), rawTask.get(j), current));
					break;
				}
			}
		}
		return result;
	}
}
