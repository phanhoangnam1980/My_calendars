package com.example.calendars.service;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.calendars.domain.CommentResponse;
import com.example.calendars.domain.model.Comment;
import com.example.calendars.domain.model.Images;
import com.example.calendars.repository.CommentRepository;

@Service
public class CommentServiceImpl implements CommentService {
	@Autowired
	private CommentRepository commentRepository;
	@Autowired
	private ChannelService channelService;
	@Autowired
	private TaskService taskService;

	@Value("${firebase.baseFirst-image-url}")
	private String imageurl;

	@Value("${firebase.baselast-image-url}")
	private String lasturl;

	@Override
	public void save(Comment comment) {
		// TODO Auto-generated method stub
		if (comment.getProjectId() != null && comment.getTaskId() == null) {
			commentRepository.save(comment);
			channelService.UpdateCommentGroupChannel(1, comment.getProjectId());
		}
		if (comment.getTaskId() != null && comment.getProjectId() == null) {
			commentRepository.save(comment);
			taskService.UpdateCommentTask(1, comment.getTaskId());
		}
	}

	@Override
	public List<Comment> getListByTask(Long taskid, String zonetime, int pagenumber, int sizeInOne) {
		// TODO Auto-generated method stub
		List<Comment> result = commentRepository.getListCommentByTask(taskid, PageRequest.of(pagenumber, sizeInOne));
		result.sort(Comparator.comparing(Comment::getCreate));
		ChangeTimeByZoneUser(result, zonetime);
		return result;
	}

	@Override
	public List<Comment> getListByProject(Long projectid, String zonetime, int pagenumber, int sizeInOne) {
		// TODO Auto-generated method stub
		List<Comment> result = commentRepository.getListCommentByProject(projectid,
				PageRequest.of(pagenumber, sizeInOne));
		result.sort(Comparator.comparing(Comment::getCreate));
		ChangeTimeByZoneUser(result, zonetime);
		return result;
	}

	@Override
	public void ChangeTimeByZoneUser(List<Comment> rawdata, String zonetime) {
		// TODO Auto-generated method stub
		int lengthRawData = rawdata.size();
		for (int i = 0; i < lengthRawData; i++) {
			rawdata.get(i).setCreate(rawdata.get(i).getCreate().withZoneSameInstant(ZoneOffset.of(zonetime)));
		}
	}

	@Override
	public void updateContext(Comment comments) {
		// TODO Auto-generated method stub
		commentRepository.updateContextComment(comments.getComment(), comments.getId());
	}

	@Override
	public void deleteComment(Long id) {
		// TODO Auto-generated method stub
		Comment raw = commentRepository.findByCommentId(id);
		commentRepository.deleteById(id);
		if (raw.getProjectId() != null) {
			channelService.UpdateCommentGroupChannel(-1, raw.getProjectId());
		} else if (raw.getTaskId() != null) {
			taskService.UpdateCommentTask(-1, raw.getTaskId());
		}
	}

	@Override
	public List<CommentResponse> processCommentService(List<Comment> rawdata, List<Images> rawImage, String zonetime) {
		// TODO Auto-generated method stub
		ZonedDateTime previous = null;
		int imagecount = 0;
		List<CommentResponse> result = new ArrayList<CommentResponse>();
		int sizeRawData = rawdata.size();
		for (int i = 0; i < sizeRawData; i++) {
			if (rawdata.get(i).getCreate() != previous) {
				previous = rawdata.get(i).getCreate();
				CommentResponse newResponse = new CommentResponse();
				newResponse.setTimePost(previous);
				newResponse.createListComment(rawdata.get(i));
				newResponse.setImages(new ArrayList<Images>());
				for (int j = imagecount; j < rawImage.size(); j++) {
					if (rawImage.get(j).getComment() != rawdata.get(i).getId()) {
						imagecount = j;
						break;
					}
					Images preChange = rawImage.get(j);
					preChange.setThumbshot(imageurl + preChange.getThumbshot() + lasturl);
					preChange.setViewImage(imageurl + preChange.getViewImage() + lasturl);
					preChange.setFull(imageurl + preChange.getFull() + lasturl);
					newResponse.getImages().add(preChange);
				}
				result.add(newResponse);
			} else {
				result.get(result.size() - 1).addComment(rawdata.get(i));
			}
		}
		return result;
	}

}
