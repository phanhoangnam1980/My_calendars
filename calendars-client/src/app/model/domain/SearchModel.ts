export class SearchModel {
  url: string;
  title: string;
  description: string;
  colorSpecs: string;
  titleChannel: string;
  taskid: bigint;
}
