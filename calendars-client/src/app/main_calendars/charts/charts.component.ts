import {Component, OnInit} from '@angular/core';
import {ModelChart} from '../../model/domain/ModelChart';
import {DateTimeCalendarsService} from '../../service/DateTimeCalendars.service';
import {TranslocoService} from '@ngneat/transloco';
import {TaskService} from '../../service/task.service';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {
  public viewChart: any[] = [150, 300];
  public dataChart: Array<ModelChart>;
  public labelChart = true;
  public colorScheme = {domain: ['#FF8A80', '#EA80FC', '#8C9EFF', '#80D8FF', '#A7FFEB', '#CCFF90', '#FFFF8D', '#FF9E80']};

  constructor(private dateService: DateTimeCalendarsService, private translocoService: TranslocoService,
              private taskService: TaskService) {
    this.taskService.GetCountTaskData().subscribe((data: Array<ModelChart>) => {
      this.dataChart = data;
      this.dateService.ReplaceByLanguage(this.dataChart);
    });
    Object.assign(this, this.dataChart);
  }

  ngOnInit(): void {
  }

  testEvent(event) {
    console.log(event);
  }
}
