import {Component, OnInit} from '@angular/core';
import {TaskService} from '../../service/task.service';
import {CustomChartModel} from '../../model/domain/CustomChartModel';
import {TranslocoService} from '@ngneat/transloco';
import {ModelChart} from '../../model/domain/ModelChart';
import {UserService} from '../../service/user.service';
import {ChannelService} from '../../service/Channel.service';
import {Group} from '../../model/group';

@Component({
  selector: 'app-chart-today',
  templateUrl: './chart-today.component.html',
  styleUrls: ['./chart-today.component.css']
})
export class ChartTodayComponent implements OnInit {
  public FirstYAxisLabel: string;
  public yAxisLabel: string;
  view: any[] = [400, 250];
  public TaskGroup1: Array<ModelChart>;
  public TaskGroup2: Array<CustomChartModel>;

  public firstChartName: string;
  public secondChartName: string;

  constructor(private taskService: TaskService, private translocoService: TranslocoService,
              private userService: UserService, private channelService: ChannelService) {
    this.TaskGroup1 = new Array<ModelChart>();
    this.TaskGroup2 = new Array<CustomChartModel>();
  }

  ngOnInit(): void {
    this.translocoService.selectTranslate('TotalTaskLeft').subscribe(result => {
      this.FirstYAxisLabel = result;
    });
    this.translocoService.selectTranslate('Tasks').subscribe(result => {
      this.yAxisLabel = result;
    });
    this.channelService.GetNameChannel().subscribe((result: Array<Group>) => {
      console.log(result);
      this.firstChartName = result[0].titleGroupTask;
      this.secondChartName = result[1].titleGroupTask;
    });
    this.userService.GetDetailOptionUser().subscribe(userResult => {
      this.taskService.getDataChartGroup(userResult.firstChart, userResult.secondChart).subscribe(result => {
        this.TaskGroup1 = result[0].series;
        this.translocoService.selectTranslate('finished').subscribe(nameFinish => {
          this.TaskGroup1[0].name = nameFinish;
        });
        this.translocoService.selectTranslate('total').subscribe(nameTotal => {
          this.TaskGroup1[1].name = nameTotal;
        });
        this.translocoService.selectTranslate('TotalTaskLeft').subscribe(taskleft => {
          this.translocoService.selectTranslate('finished').subscribe(donetask => {
            result.splice(0, 1);
            this.TaskGroup2 = result.map(a => {
              a.series[0].name = donetask;
              a.series[1].name = taskleft;
              return {...a};
            });
          });
        });
      });
    });
  }

}
