import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {UserService} from './user.service';
import {Router} from '@angular/router';
import {TranslocoService} from '@ngneat/transloco';
import {LoginService} from './Login.service';
import {Response} from '../model/domain/Response';

@Injectable({
  providedIn: 'root'
})
export class GoogleSigninService {
  private auth2: gapi.auth2.GoogleAuth;
  private subject = new ReplaySubject<gapi.auth2.GoogleUser>(1);

  constructor(private loginService: LoginService ,private router: Router) {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: '133847714360-6n3f91nkk5f4hcqi15pru0r8mfklo84u.apps.googleusercontent.com'
      });
    });
  }

  public signIn() {
    this.auth2.signIn({
      scope: 'https://www.googleapis.com/auth/gmail.readonly'
    }).then(user => {
      const profile = this.auth2.currentUser.get().getBasicProfile();
      this.loginService.SigninWithGoogle(user.getAuthResponse().id_token).subscribe((isValid:Response) => {
        if (isValid) {
          localStorage.setItem(
            'token', JSON.stringify({email: profile.getEmail()})
          );
          localStorage.setItem('authentication', JSON.stringify(isValid.data));
          return this.router.navigate(['app/today']).then();
        }
      });
      this.subject.next(user);
    }).catch(() => {
      this.subject.next(null);
    });
  }

  public signOut() {
    this.auth2.signOut().then(() => {
      this.subject.next(null);
    });
  }

  public observable(): Observable<gapi.auth2.GoogleUser> {
    return this.subject.asObservable();
  }
}

