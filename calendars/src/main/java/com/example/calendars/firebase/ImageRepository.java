package com.example.calendars.firebase;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.calendars.domain.model.Images;

public interface ImageRepository extends JpaRepository<Images, Long> {
	@Transactional
	@Modifying
	@Query(value = "select u from Images u where u.comment=:current")
	List<Images> FindImageInComment(@Param("current") Long currentCommentId);

	@Query(value = "select u from Images u inner join Comment t2 on t2.id= u.comment where t2.userid=:current order by u.comment")
	List<Images> FindImageInUser(@Param("current") Long currentUserid);

	@Query(value = "select u from Images u inner join Comment t2 on t2.id=u.comment where t2.taskId=:currentTask order by u.comment")
	List<Images> GetListImageFromCommentTask(@Param("currentTask") Long taskid);

	@Query(value = "select u from Images u inner join Comment t2 on t2.id=u.comment where t2.projectId=:currentProject order by u.comment")
	List<Images> GetListImageFromCommentProject(@Param("currentProject") Long projectId);

	@Query(value = "select u.id,u.comment,u.thumbshot,u.viewImage,u.full,u.metadata,t2.taskId from Images u inner join Comment t2 on t2.id=u.comment where t2.taskId is not null and t2.userid=:current order by t2.taskId")
	List<Object[]> GetImageTaskByProject(@Param("current") Long currentUserid, Pageable pageable);
	
	@Query(value = "select u from Images u inner join DetailUser t2 on t2.idImage = u.id where t2.userid=:currentUserid")
	Images FindImageAvatar(@Param("currentUserid") Long userid);
}
