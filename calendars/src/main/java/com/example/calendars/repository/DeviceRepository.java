package com.example.calendars.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.calendars.domain.model.DeviceUser;

public interface DeviceRepository extends JpaRepository<DeviceUser, Long> {
	@Query("Select u from DeviceUser u where u.userid =:currentUser")
	List<DeviceUser> GetListDevice(@Param("currentUser") Long userid);

	@Query("Select u from DeviceUser u where u.ip =:ipFound and u.userid =:currentUser and u.agent=:infoDevice")
	DeviceUser findByIp(@Param("ipFound") String ip, @Param("currentUser") Long id, @Param("infoDevice") String deviceAgent);
	
	@Query("Select u from DeviceUser u where u.id=:currentId")
	DeviceUser GetDataDevice(@Param("currentId") String id);
	
	@Transactional
	@Modifying
	@Query(value = "Update DeviceUser u set u.language =:languageUpdate where u.id =:currentId")
	void updateLanguageDevice(@Param("currentId") String id, @Param("languageUpdate") String laguage);
	
	@Transactional
	@Modifying
	@Query(value = "Update DeviceUser u set u.token =:updateDevice where u.id =:currentId")
	void updateToken(@Param("currentId") String id, @Param("updateDevice") String device);
}
