import {AfterViewInit, Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Comments} from '../../model/Comment';
import {CommentService} from '../../service/comment.service';
import {UserService} from '../../service/user.service';
import {User} from '../../model/User';
import {CommentData} from '../../model/domain/CommentData';
import {BehaviorSubject} from 'rxjs';
import {ImageService} from '../../service/image.service';
import {Response} from '../../model/domain/Response';
import {Images} from '../../model/Images';
import {environment} from '../../../environments/environment';
import {DetailImageComponent} from '../../detail-image/detail-image.component';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit, AfterViewInit, OnDestroy {
  public DisplayChannel: string;
  public DisplayColor: string;
  private ChannelId: bigint;
  public DataComment: Array<CommentData>;
  public userPost: User;
  private flagNewComment: BehaviorSubject<boolean>;
  private ImageStorage: Array<Images>;
  public PreViewImages: Array<string>;
  @Input()
  textarea: string;

  public flagShowTime: boolean;
  public flagButtonDelete: boolean;
  public location: number;

  constructor(private dialogRef: MatDialogRef<CommentComponent>, @Inject(MAT_DIALOG_DATA) private data: any,
              private commentService: CommentService, private userService: UserService, private imageService: ImageService,
              public dialog: MatDialog) {
    this.flagNewComment = new BehaviorSubject<boolean>(false);
    this.userPost = new User();
    this.location = -1;
    this.flagShowTime = false;
    this.flagButtonDelete = false;
    this.ImageStorage = new Array<Images>();
    this.PreViewImages = new Array<string>();
  }

  ngOnInit(): void {
    this.DisplayChannel = this.data.ChannelTitle;
    this.DisplayColor = this.data.ColorChannel;
    this.ChannelId = this.data.ChannelID;
    this.commentService.GetCommentByProjectId(this.ChannelId).subscribe(result => {
      this.DataComment = result;
    });
    this.userService.getAvatarUsername().subscribe(result => {
      this.userPost = result;
    });
  }

  ngAfterViewInit(): void {
    this.flagNewComment.subscribe(data => {
      if (data === true) {
        this.commentService.GetCommentByProjectId(this.ChannelId).subscribe(result => {
          this.DataComment = result;
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.flagNewComment.unsubscribe();
  }

  autogrow() {
    const textArea = document.getElementById('textComment');
    textArea.style.overflow = 'hidden';
    textArea.style.height = 'auto';
    textArea.style.height = textArea.scrollHeight + 'px';
  }

  SaveNewComment() {
    const send = new CommentData();
    send.comments = new Array<Comments>();
    const commentSend = new Comments();
    commentSend.comment = this.textarea;
    commentSend.projectId = this.ChannelId;
    send.comments.push(commentSend);
    send.images = this.ImageStorage;
    this.commentService.SaveComment(send).subscribe();
    this.textarea = '';
    this.ImageStorage = new Array<Images>();
    this.RefreshNewComment();
  }

  RefreshNewComment() {
    this.PreViewImages = new Array<string>();
    setTimeout(() => {
      this.flagNewComment.next(true);
    }, 150);
  }

  mouseEnterShowTime(index: number) {
    this.flagShowTime = true;
    this.location = index;
  }

  mouseLeaveTime() {
    this.flagShowTime = false;
    this.location = -1;
  }

  mouseEnterShowButton(index: number) {
    this.flagButtonDelete = true;
    this.location = index;
  }

  mouseLeaveButton() {
    this.flagButtonDelete = false;
    this.location = -1;
  }

  DeletedComment(idcomment, location) {
    this.DataComment.splice(location, 1);
    this.commentService.DeleteComment(idcomment);
  }

  changeFile(event) {
    const value = event.target.files[0].name;
    // const contexttype = event.target.files[0].type;
    const fileName = typeof value === 'string' ? value.match(/[^\/\\]+$/)[0] : value[0];
    if (event.target.files[0].size < 6291456) {
      this.imageService.SendImage(event.target.files[0], fileName).subscribe((result: Response) => {
        this.AddDataImage(result.data);
      });
    }
  }

  AddDataImage(data) {
    this.ImageStorage.push(data);
    this.PreViewImages.push(environment.urlViewImage + data.thumbshot + '?alt=media');
  }

  ShowBiggerImage(image: Images) {
    this.dialog.open(DetailImageComponent, {
      width: '1200px',
      data: image
    });
  }
}
