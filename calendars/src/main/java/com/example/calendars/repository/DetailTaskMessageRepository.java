package com.example.calendars.repository;

import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.calendars.domain.model.DetailTaskGroup;

public interface DetailTaskMessageRepository extends JpaRepository<DetailTaskGroup, Long> {
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM DetailTaskGroup u Where u.task=:id_task_deleted and u.groups=:id_group_delete")
	void DeleteTaskInProject(@Param("id_task_deleted") Long idtaskDelete, @Param("id_group_delete") Long idgroupdelete);

	@Transactional
	@Modifying
	@Query(value = "DELETE FROM DetailTaskGroup u Where u.section=:id_section_delete")
	void DeleteSectionTaskInProject(@Param("id_section_delete") Long idsectiondelete);

	@Query(value = "Select u from DetailTaskGroup u where u.groups=:id_group_find")
	List<DetailTaskGroup> FindTaskInGroup(@Param("id_group_find") Long idGroup);

	@Query(value = "Select u from DetailTaskGroup u where u.groups=:id_group and u.kinds <> 1 order by u.section, u.kinds DESC")
	List<DetailTaskGroup> ListTaskInGroupBySection(@Param("id_group") Long groupid);

	@Transactional
	@Modifying
	@Query(value = "DELETE FROM DetailTaskGroup u Where u.groups = :id_group_delete")
	void DeleteTaskFromDeletedProject(@Param("id_group_delete") Long ID_group_delete);

	@Transactional
	@Modifying
	@Query(value = "UPDATE DetailTaskGroup u SET u.groups = :new_group_id Where u.task=:id_task and u.groups=:id_group")
	void UpdateNewProjectTask(@Param("new_group_id") Long newGroupStorageTask, @Param("id_task") Long idtask,
			@Param("id_group") Long oldIDGroup);

	@Transactional
	@Modifying
	@Query(value = "DELETE FROM DetailTaskGroup u Where u.task = :id_task")
	void DeleteTaskByTaskID(@Param("id_task") Long id_task);

	@Query(value = "SELECT g1.id,g1.taskid,g1.groupid,g2.color_detail_specs,g2.name_group_task,g2.type_group,"
			+ "g3.title_task, g3.task_description, g3.date_finished, g2.user_id as user_create_groups,"
			+ "g3.user_id , g4.title_section, g4.id as user_create_task FROM detail_task_group g1 inner join group_channels "
			+ "g2 on g1.groupid=g2.groupid inner join task_messages g3 on g3.taskid = g1.taskid inner join section g4 "
			+ "on g4.id = g1.sectionid where g3.user_id =:id_user"
			+ " and g1.taskid is not null and g3.date_finished <=:TimeFind and g3.was_delete is null", nativeQuery = true)
	List<Object[]> FindByUserId(@Param("id_user") Long userId, @Param("TimeFind") ZonedDateTime timezone);

	@Query(value = "select u.* from detail_task_group u inner join group_channels g on g.groupid = u.groupid"
			+ " where u.taskid is null and g.user_id=:currentUserId group by u.sectionid "
			+ "order by u.groupid ASC, u.sectionid ASC", nativeQuery = true)
	List<DetailTaskGroup> ListSectionAndGroupByUserid(@Param("currentUserId") Long userid);

	@Transactional
	@Modifying
	@Query(value = "Update DetailTaskGroup u set u.kinds = 1 where u.task =:taskid")
	void UpdateTaskFinished(@Param("taskid") Long id);

	@Query(value = "Select t2.groupid, t2.ColorSpecs, t2.TitleGroupTask, t2.UserID, t3.id, t3.title from DetailTaskGroup u inner join Group t2"
			+ " on t2.groupid = u.groups inner join Section t3 on t3.id = u.section"
			+ " where u.task is null and t2.UserID=:currentUser and t2.TitleGroupTask like %:prefix% or t3.title like %:prefix%")
	List<Object[]> searchSectionAndGroup(@Param("prefix") String prefix, @Param("currentUser") Long currentid,
			Pageable pageable);

	@Query(value = "Select t2.groupid, t2.ColorSpecs, t2.TitleGroupTask, t3.id, t3.title, t4.taskid, t4.Title, t4.Description From DetailTaskGroup u"
			+ " inner join Group t2 on t2.groupid = u.groups inner join Section t3 on t3.id = u.section inner join TaskMessage t4 on t4.taskid = u.task"
			+ " where u.task is not null and u.kinds <> 1 and t4.Title like %:prefix% or t4.Description like %:prefix% and t4.IsDeleted is null"
			+ " order by t4.date")
	List<Object[]> searchTask(@Param("prefix") String prefix, Pageable pageable);

	@Transactional
	@Modifying
	@Query(value = "Update DetailTaskGroup u set u.kinds =:kindsOrder where u.task =:taskid")
	void UpdateKindsofTask(@Param("kindsOrder") Long number, @Param("taskid") Long taskid);

	@Query(value = "Select u.id, u.task, u.groups, t1.Title, t1.Description, t1.date from DetailTaskGroup u inner join TaskMessage t1"
			+ " on t1.taskid= u.task where t1.UserID =:currentUser and u.kinds=5 order by t1.date")
	List<Object[]> getPinnedMessage(@Param("currentUser") Long userid, Pageable pageable);
	
	@Query(value = "SELECT g1.id,g1.taskid,g1.groupid,g2.color_detail_specs,g2.name_group_task,g2.type_group,"
			+ "g3.title_task, g3.task_description, g3.date_finished, g2.user_id as user_create_groups,"
			+ "g3.user_id , g4.title_section,g4.id as user_create_task FROM detail_task_group g1 inner join group_channels "
			+ "g2 on g1.groupid=g2.groupid inner join task_messages g3 on g3.taskid = g1.taskid inner join section g4 "
			+ "on g4.id = g1.sectionid where g3.parent_id is null and "
			+ "g1.kinds <> '1' order by g1.kinds,g1.taskid", nativeQuery = true)
	List<Object[]> GetUpcomingTaskData(Pageable pageable);
}
