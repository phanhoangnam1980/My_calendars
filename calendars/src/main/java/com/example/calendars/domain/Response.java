package com.example.calendars.domain;

import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "E MMM dd yyyy HH:mm:ss xx")
	private ZonedDateTime currentSend;

	private int code;

	private String status;

	private String message;

	private String stackTrace;

	private Object data;

	public Response() {
		super();
		this.currentSend = ZonedDateTime.now();
	}

	public Response(HttpStatus httpStatus, String message) {
		super();
		this.code = httpStatus.value();
		this.status = httpStatus.name();
		this.message = message;
	}

	public Response(HttpStatus httpStatus, String message, String stackTrace) {
		this(httpStatus, message);

		this.stackTrace = stackTrace;
	}

	public Response(HttpStatus httpStatus, String message, String stackTrace, Object data) {
		this(httpStatus, message, stackTrace);

		this.data = data;
	}
	
	public void setResponse(HttpStatus httpStatus, String message) {
		this.code = httpStatus.value();
		this.status = httpStatus.name();
		this.message = message;
	}
}
