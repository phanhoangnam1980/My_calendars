package com.example.calendars.domain.model;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "comments")
@Getter
@Setter
public class Comment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long projectId;
	
	@Column(name = "task_id")
	private Long taskId;
	
	@Column(name = "created_at")
	private ZonedDateTime create;
	
	@Column(columnDefinition = "TEXT")
	private String comment;
	
	private Long userid;
}
