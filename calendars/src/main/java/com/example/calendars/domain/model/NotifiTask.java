package com.example.calendars.domain.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "NotifiTask")
public class NotifiTask {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long taskid;
	
	@Column(columnDefinition = "integer(2)")
	private int type;

	private int duration;

	@Override
	public int hashCode() {
		return Objects.hash(duration, taskid, type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotifiTask other = (NotifiTask) obj;
		return duration == other.duration && Objects.equals(taskid, other.taskid) && type == other.type;
	}
}
