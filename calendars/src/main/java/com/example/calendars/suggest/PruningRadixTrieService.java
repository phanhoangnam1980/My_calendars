package com.example.calendars.suggest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PruningRadixTrieService {
	public long termCount = 0;
	public long termCountLoaded = 0;

	private PruningRadixNode tries;

	public PruningRadixTrieService(PruningRadixNode tries) {
		super();
		this.tries = tries;
	}

	
	public PruningRadixTrieService() {
		super();
		this.tries= new PruningRadixNode(0);
	}


	public void addTerm(String term, long termFrequencyCount) {
		List<PruningRadixNode> nodeList = new ArrayList<>();
		Addterm(tries, term, termFrequencyCount, 0, 0, nodeList);
	}

	public void UpdateMaxCounts(List<PruningRadixNode> nodeList, long termFrequencyCount) {
		for (PruningRadixNode node : nodeList) {
			if (termFrequencyCount > node.getTermFrequencyCountChildMax())
				node.setTermFrequencyCountChildMax(termFrequencyCount);
		}
	}

	public void Addterm(PruningRadixNode current, String term, Long termFrequencyCount, int id, int level, List<PruningRadixNode> nodeList) {
		try {
			nodeList.add(current);
			//test for common prefix (with possibly different suffix)
			int common = 0;
			if (current.getChildren() != null) {
				int sizeChildern = current.getChildren().size();
				for (int j = 0; j < sizeChildern; j++) {
					String key = current.getChildren().get(j).getKeys();
					PruningRadixNode node = current.getChildren().get(j).getNode();
					for (int i = 0; i < Math.min(term.length(), key.length()); i++) {
						if (term.charAt(i) == key.charAt(i)) {
							common = i + 1;
						} else {
							break;
						}
					}
					if (common > 0) {
						// term already existed
						if (common == term.length() && common == key.length()) {
							if (node.getTermFrequencyCount() == 0)
								termCount++;
							node.setTermFrequencyCount(node.getTermFrequencyCount() + termFrequencyCount);
							UpdateMaxCounts(nodeList, node.getTermFrequencyCount());
						} else if (common == term.length()) {
							// if new is shorter (== common), then node(count) and only 1. children add (clause2)
							PruningRadixNode child = new PruningRadixNode(termFrequencyCount);
							List<PruningRadixNodeModel> listmodel = new ArrayList<PruningRadixNodeModel>();
							listmodel.add(new PruningRadixNodeModel(key.substring(common), node));
							child.setChildren(listmodel);
							child.setTermFrequencyCountChildMax(
									Math.max(node.getTermFrequencyCountChildMax(), node.getTermFrequencyCount()));
							UpdateMaxCounts(nodeList, termFrequencyCount);
							// insert first part as key, overwrite old node
							current.getChildren().get(j).setKeys(term.substring(0, common));
							current.getChildren().get(j).setNode(child);
							// sort children descending by termFrequencyCountChildMax to start lookup with
							// most promising branch
							Collections.sort(current.getChildren(),
									Comparator.comparing((PruningRadixNodeModel e) -> e.getNode().getTermFrequencyCountChildMax())
											.reversed());
							termCount++;
						} else if (common == key.length()) {
							Addterm(node, term.substring(common), termFrequencyCount, id, level + 1, nodeList);
						} else {
							// old and new have common substrings
							PruningRadixNode child = new PruningRadixNode(0);
							List<PruningRadixNodeModel> listnode = new ArrayList<PruningRadixNodeModel>();
							listnode.add(new PruningRadixNodeModel(key.substring(common), node));
							listnode.add(new PruningRadixNodeModel(term.substring(common), new PruningRadixNode(termFrequencyCount)));
							child.setChildren(listnode);
							child.setTermFrequencyCountChildMax(Math.max(node.getTermFrequencyCountChildMax(),
									Math.max(termFrequencyCount, node.getTermFrequencyCount())));
							UpdateMaxCounts(nodeList, termFrequencyCount);
							// insert first part as key , overwrite old node
							current.getChildren().get(j).setKeys(term.substring(0, common));
							current.getChildren().get(j).setNode(child);
							// sort children descending by termFrequencyCountChildMax to start lookup with
							// most promising branch
							Collections.sort(current.getChildren(),
									Comparator.comparing((PruningRadixNodeModel e) -> e.getNode().getTermFrequencyCountChildMax())
											.reversed());
							// increment termcount by 1
							termCount++;
						}
						return;
					}
				}
			}
			if (current.getChildren() == null) {
				List<PruningRadixNodeModel> listnode = new ArrayList<>();
				listnode.add(new PruningRadixNodeModel(term, new PruningRadixNode(termFrequencyCount)));
				current.setChildren(listnode);
			} else {
				current.getChildren().add(new PruningRadixNodeModel(term, new PruningRadixNode(termFrequencyCount)));
				// sort children descending by termFrequecyCountChildMax to start looking for
				// most promising branch
				Collections.sort(current.getChildren(),
						Comparator.comparing((PruningRadixNodeModel e) -> e.getNode().getTermFrequencyCountChildMax()).reversed());
			}
			termCount++;
			UpdateMaxCounts(nodeList, termFrequencyCount);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("exception: " + term + " " + e.getMessage());
		}
	}

	public void FindAllChildTerms(String prefix, int topK, String prefixString, List<PruningRadixTermAndFrequency> results,
			Boolean pruning) {
		FindAllChildTerms(prefix, tries, topK, prefixString, results, null, pruning);
	}

	public void FindAllChildTerms(String prefix, PruningRadixNode curr, int topK, String prefixString,
			List<PruningRadixTermAndFrequency> results, BufferedWriter file, Boolean pruning) {
		try {
			if (pruning && (topK > 0) && (results.size() == topK)
					&& (curr.getTermFrequencyCountChildMax() <= results.get(topK - 1).getTermFrequencyCount())) {
				return;
			}
			Boolean noPrefix = (prefix.equals("") || prefix == null);
			if (curr.getChildren() != null) {
				for (PruningRadixNodeModel nodemodel : curr.getChildren()) {
					String key = nodemodel.getKeys();
					PruningRadixNode node = nodemodel.getNode();
					if ((pruning && (topK > 0) && (results.size() == topK))
							&& (node.getTermFrequencyCount() <= results.get(topK - 1).getTermFrequencyCount())
							&& (node.getTermFrequencyCountChildMax() <= results.get(topK - 1)
									.getTermFrequencyCount())) {
						if (!noPrefix)
							break;
						else
							continue;
					}
					if (noPrefix || key.startsWith(prefix)) {
						if (node.getTermFrequencyCount() > 0) {
							if (file != null)
								file.write(prefixString + key + "\t" + node.getTermFrequencyCount() + "\n");
							else {
								if (topK > 0)
									addTopKSuggestion(prefixString + key, node.getTermFrequencyCount(), topK, results);
								else
									results.add(new PruningRadixTermAndFrequency(prefixString + key, node.getTermFrequencyCount()));
							}
						}
						if ((node.getChildren() != null) && (node.getChildren().size() > 0)) {
							FindAllChildTerms("", node, topK, prefixString + key, results, file, pruning);
						}
						if (!noPrefix)
							break;
					} else if (prefix.startsWith(key)) {
						if ((node.getChildren() != null) && (node.getChildren().size() > 0)) {
							FindAllChildTerms(prefix.substring(key.length()), node, topK, prefixString + key, results,
									file, pruning);
						}
						break;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("exception: " + prefix + " " + e.getMessage());
		}
	}

	public List<PruningRadixTermAndFrequency> getTopkTermsForPrefix(String prefix, int topK) {
		return getTopkTermsForPrefix(prefix, topK, true);
	}

	public List<PruningRadixTermAndFrequency> getTopkTermsForPrefix(String prefix, int topK, Boolean pruning) {
		List<PruningRadixTermAndFrequency> results = new ArrayList<>();

		FindAllChildTerms(prefix, topK, "", results, pruning);

		return results;
	}

	public void writeTermsToFile(String path) {
		// save only if new terms were added
		if (termCountLoaded == termCount)
			return;
		try (BufferedWriter file = new BufferedWriter(new FileWriter(path))) {
			// long prefixCount = 0;
			FindAllChildTerms("", tries, 0, "", null, file, true);
			System.out.println(termCount + " terms written.");
		} catch (Exception e) {
			System.out.println("Writing terms exception: " + e.getMessage());
		}
	}

	public Boolean readTermsFromFile(String path, String delimiter) {
		try (BufferedReader sr = new BufferedReader(new FileReader(path, StandardCharsets.UTF_8))) {
			long startTime = System.currentTimeMillis();
			String line;

			// process a single line at a time only for memory efficiency
			while ((line = sr.readLine()) != null) {
				String[] lineParts = line.split(delimiter);
				if (lineParts.length == 2) {
					try {
						long count = Long.parseUnsignedLong(lineParts[1]);
						this.addTerm(lineParts[0], count);
					} catch (NumberFormatException e) {
						System.out.println(
								"Warning - frequency could not be extracted from a dictionary line. Skipping line.");
					}
				}
			}
			termCountLoaded = termCount;
			long elapsedMilliseconds = System.currentTimeMillis() - startTime;
			System.out.println(termCount + " terms loaded in " + elapsedMilliseconds + " ms");
		} catch (FileNotFoundException e) {
			System.out.println("Could not find file " + path);
			return false;
		} catch (Exception e) {
			System.out.println("Loading terms exception: " + e.getMessage());
		}

		return true;
	}

	public void addTopKSuggestion(String term, long termFrequencyCount, int topK, List<PruningRadixTermAndFrequency> results) {
		// at the end/highest index is the lowest value
		// > : old take precedence for equal rank
		// >= : new take precedence for equal rank
		if ((results.size() < topK) || (termFrequencyCount >= results.get(topK - 1).getTermFrequencyCount())) {
			PruningRadixTermAndFrequency termAndFrequency = new PruningRadixTermAndFrequency(term, termFrequencyCount);
			int index = Collections.binarySearch(results, termAndFrequency,
					Comparator.comparing((PruningRadixTermAndFrequency e) -> e.getTermFrequencyCount()).reversed()); // descending
																											// order
			if (index < 0)
				results.add(~index, termAndFrequency);
			else
				results.add(index, termAndFrequency);

			if (results.size() > topK)
				results.remove(topK);
		}
	}
}
