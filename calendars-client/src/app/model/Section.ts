export class Section {
  id: bigint;
  title: string;
  isDelete: boolean;
}
