package com.example.calendars.domain;

import java.util.List;

public class TaskData {
	private List<CustomTaskMessage> listtask;
	
	private List<CustomTaskMessage> subtask;

	// Title section
	private String title;
	
	private Long sectionid;
	
	public Long getSectionid() {
		return sectionid;
	}

	public void setSectionid(Long sectionid) {
		this.sectionid = sectionid;
	}

	public List<CustomTaskMessage> getListtask() {
		return listtask;
	}

	public void setListtask(List<CustomTaskMessage> listtask) {
		this.listtask = listtask;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void AddTask(CustomTaskMessage taskData) {
		this.listtask.add(taskData);
	}

	public List<CustomTaskMessage> getSubtask() {
		return subtask;
	}

	public void setSubtask(List<CustomTaskMessage> subtask) {
		this.subtask = subtask;
	}
}
