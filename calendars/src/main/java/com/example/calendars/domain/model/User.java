package com.example.calendars.domain.model;

import java.util.Set;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "users")
@Getter
@Setter
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String email;

	private String password;

	private String username;

	@Column(columnDefinition = "TEXT")
	private String image;

	private String zonetime;
	
	@Column(name = "type_login", columnDefinition = "integer default 0")
	private int typeLogin;
	
	@Transient
	private String passwordConfirm;

	@ManyToMany
	private Set<Role> roles;

}
