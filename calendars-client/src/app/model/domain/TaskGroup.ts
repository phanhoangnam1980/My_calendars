import {TaskGroupModel} from './TaskGroupModel';

export class TaskGroup {
  data: TaskGroupModel;
  overdue: boolean;
}
