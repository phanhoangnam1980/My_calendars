package com.example.calendars.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.calendars.domain.model.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {
	@Query(value = "Select u from Comment u where u.id =:commentid")
	Comment findByCommentId(@Param("commentid") Long id);

	@Query(value = "Select u from Comment u where u.taskId=:taskid and u.projectId is null order by u.create DESC")
	List<Comment> getListCommentByTask(@Param("taskid") Long id, Pageable pageable);

	@Query(value = "Select u from Comment u where u.projectId=:project and u.taskId is null order by u.create DESC")
	List<Comment> getListCommentByProject(@Param("project") Long id, Pageable pageable);

	@Query(value = "Select u from Comment u where u.comment like %:prefix% order by u.create ASC")
	List<Comment> searchCommentMatchWord(@Param("prefix") String prefix, Pageable pageable);

	@Transactional
	@Modifying
	@Query(value = "Update Comment u Set u.comment=:content where u.id=:commentId")
	List<Comment> updateContextComment(@Param("content") String data, @Param("commentId") Long id);
}
