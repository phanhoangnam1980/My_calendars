package com.example.calendars.firebase.notification;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.calendars.firebase.FirebaseImageService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;

@Configuration
public class FirebaseMessagingConfig {
	@Bean
	FirebaseMessaging firebaseMessaging() throws IOException {
		ClassLoader classLoader = FirebaseImageService.class.getClassLoader();
		File file = new File(Objects.requireNonNull(classLoader.getResource("mycalendars-firebase.json")).getFile());
	    GoogleCredentials googleCredentials = GoogleCredentials
	            .fromStream(new FileInputStream(file.getAbsolutePath()));
	    FirebaseOptions firebaseOptions = FirebaseOptions
	            .builder()
	            .setCredentials(googleCredentials)
	            .build();
	    FirebaseApp app = FirebaseApp.initializeApp(firebaseOptions, "MyCalendars");
	    return FirebaseMessaging.getInstance(app);
	}
}
