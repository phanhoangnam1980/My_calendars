import {Component, OnInit, Renderer2} from '@angular/core';
import {TranslocoService} from '@ngneat/transloco';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.css']
})
export class WelcomePageComponent implements OnInit {
  selected = 'vi';

  constructor(private translocoService: TranslocoService) {
    this.selected = translocoService.getActiveLang();
  }

  ngOnInit(): void {
  }

  ChangeLanguage() {
    if(this.selected !== this.translocoService.getActiveLang()) {
      this.translocoService.setActiveLang(this.selected);
    }
  }
}
