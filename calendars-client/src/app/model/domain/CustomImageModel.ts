import {Images} from '../Images';
import {TaskGroup} from './TaskGroup';
import {Section} from '../Section';

export class CustomImageModel {
  image: Images;
  task: TaskGroup;
  section: Section;
}
