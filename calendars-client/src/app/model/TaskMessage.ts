export class TaskMessage {
  taskid: bigint;
  title: string;
  description: string;
  date: string;
  userID: bigint;
}
