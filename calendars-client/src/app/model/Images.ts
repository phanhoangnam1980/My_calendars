export class Images {
  id: bigint;
  thumbshot: string;
  viewImage: string;
  full: string;
}
