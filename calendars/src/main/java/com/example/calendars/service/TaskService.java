package com.example.calendars.service;

import java.time.ZonedDateTime;
import java.util.List;

import com.example.calendars.domain.CountTaskPerDate;
import com.example.calendars.domain.CustomChartModel;
import com.example.calendars.domain.CustomTaskGroupByDay;
import com.example.calendars.domain.TaskData;
import com.example.calendars.domain.TaskGroup;
import com.example.calendars.domain.TaskGroupSplitDataCompare;
import com.example.calendars.domain.model.TaskMessage;

public interface TaskService {
	void savenewtask(TaskMessage newtask, String timezone);

	List<TaskData> FindAllTaskGroup(Long groupid, Long userid, String timezone);

	TaskGroupSplitDataCompare AllTodayTaskWithGroup(Long userid, String timezone);

	List<TaskMessage> FindAllFinishedTask(Long groupid, Long userid, int pagenumber);

	TaskMessage UpdateTaskFinished(Long CurrentTaskId);

	void DeleteTaskFromGroup(Long CurrentTaskId);

	void DeleteTask(Long CurrentTaskId);

	String processDateToStandardTime(String DateGive, String timezone);

	List<CountTaskPerDate> CountTaskInWeek(Long userid, String timezone);

	List<TaskGroup> GetDataTaskPinned(Long userid, String timezone);

	List<TaskData> GetAllSubTaskFollowTaskId(Long taskid, Long userid, String timezone, Long groupid);

	void UpdateCommentTask(int number, Long taskid);

	List<CountTaskPerDate> DateGraphGroup(Long groupid, int pagenumber, int limit);

	List<CountTaskPerDate> TaskGraphTotal();

	List<TaskGroup> DataUpComing(String timezone);

	List<CustomChartModel> CountFinishedTotalPerTask(Long groupid, int pagenumber, int limit);

	List<CountTaskPerDate> CountCompleteRemainTask(Long groupid, String zone);

	List<CustomChartModel> CountTotalRemain(Long groupid);

	List<TaskGroup> ListTaskHaveComment(String timezone);

	List<Object[]> getListUpcomingNotification(ZonedDateTime afterTime, ZonedDateTime beforeTime);

	void UpdateTask(TaskMessage task, String timezone);

	void UpdateParentId(TaskMessage task);

	void UpdateTimeFinished(TaskMessage task, String timezone);
	
	List<CustomTaskGroupByDay> ListTaskTomorrow(int pagenumber, Long userid, String zonetime);
	
	List<CustomTaskGroupByDay> ListTaskTomorrowNextPage(int pagenumber, Long userid, String zonetime);
}
