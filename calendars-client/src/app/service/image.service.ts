import {Injectable} from '@angular/core';
import {TokenResponse} from '../model/TokenResponse';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class ImageService {
  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));

  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    authorization: 'Bearer ' + this.token.accessToken
  });

  constructor(private http: HttpClient) {
  }

  SendImage(file: File, fileName: string) {
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', file, fileName);
    return this.http.post(environment.serverPort + '/image', uploadImageData);
  }

  SendAvatar(file: File, fileName: string) {
    const uploadAvatarData = new FormData();
    uploadAvatarData.append('avatar', file, fileName);
    return this.http.post(environment.serverPort + '/avatar', uploadAvatarData);
  }
}
