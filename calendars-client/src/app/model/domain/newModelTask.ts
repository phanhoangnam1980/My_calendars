export class NewModelTask {
  tittle: string;
  description: string;
  datedata: string;
  groupId: number;
  sectionId: number;
  zoneId: string;
  parentId: bigint;
}
