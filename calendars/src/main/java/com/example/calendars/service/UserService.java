package com.example.calendars.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashSet;

import com.example.calendars.domain.model.DeviceUser;
import com.example.calendars.domain.model.User;

public interface UserService {
	void save(User user);

	User findByEmail(String email);

	void updateNewTimezone(User currentuser, String zonetime);

	User AvatarOnly(String user);

	void updateEmail(User user);

	void updatePassword(User user);

	void updateUsername(User user);

	void updateAvatar(User user, String avatar);

	void saveDevice(DeviceUser device);

	HashSet<DeviceUser> listDeviceUser(Long userid);

	DeviceUser getIdDevice(String ip, String agent, Long userid);

	void updateLanguage(String language, String string);

	void ClientDeviceUsing(DeviceUser current);

	DeviceUser GetDeviceInfo(String id);

	void updateDeviceToken(DeviceUser device);

	Object validLoginWithGoogle(String token) throws GeneralSecurityException, IOException;

	void SetUpNewUser(Long userid, String urlAvatar);
}
