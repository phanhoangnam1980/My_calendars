import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ChannelService} from '../../service/Channel.service';
import {Group} from '../../model/group';
import {ShareDataService} from '../../service/ShareData.service';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.css']
})
export class EditProjectComponent implements OnInit {
  @Input()
  nameProject: string;
  @Input()
  colorProject: any;

  constructor(public dialogRef: MatDialogRef<EditProjectComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
              private channelService: ChannelService,private shareService: ShareDataService) {
    dialogRef.disableClose = true;
  }

  public userTestStatus = [
    {color: '#C0C0C0', namecolor: 'Silver'},
    {color: '#708090', namecolor: 'SlateGray'},
    {color: '#133337', namecolor: 'Elite Teal'},
    {color: '#8A2BE2', namecolor: 'BlueViolet'},
    {color: '#003366', namecolor: 'Prussian Blue'},
    {color: '#87CEFA', namecolor: 'LightSkyBlue'},
    {color: '#000080', namecolor: 'NavyBlue'},
    {color: '#0e2f44', namecolor: 'Blue Whale'},
    {color: '#EEB4B4', namecolor: 'RosyBrown'},
    {color: '#EBC79E', namecolor: 'New Tan'},
    {color: '#228B22', namecolor: 'ForestGreen'},
  ];

  ngOnInit(): void {
    this.colorProject = this.userTestStatus.find(u => u.color === this.data.colorSpecs);
    this.nameProject = this.data.titleGroupTask;
  }

  saveColorSelected(ColorSelected: any) {
    this.colorProject = ColorSelected;
    console.log(this.colorProject.color);
    console.log(this.colorProject.namecolor);
  }

  editProject() {
    const datagroup = new Group();
    datagroup.groupid = this.data.groupid;
    datagroup.userID = this.data.userID;
    datagroup.titleGroupTask = this.nameProject;
    datagroup.colorSpecs = this.colorProject.color;
    if (datagroup.titleGroupTask !== this.data.titleGroupTask || datagroup.colorSpecs !== this.data.colorSpecs) {
      this.channelService.updateChannel(datagroup).subscribe(response => {
        this.shareService.SetUpdateChannelStatus(true);
        console.log(response);
      });
    }
    this.dialogRef.close();
  }

  onCloseClick() {
    this.dialogRef.close();
  }

}
