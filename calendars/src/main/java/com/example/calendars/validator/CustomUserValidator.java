package com.example.calendars.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.calendars.domain.Response;
import com.example.calendars.domain.model.User;

@Component
public class CustomUserValidator {
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public void VerifyPassword(User updateUser, User user, Response errors) {

		if (!updateUser.getPasswordConfirm().equals(updateUser.getPassword())) {
			errors.setResponse(HttpStatus.NOT_ACCEPTABLE, "Diff.userForm.passwordConfirm");
		}
		
		if (updateUser.getPassword().length() < 8) {
			errors.setResponse(HttpStatus.NOT_ACCEPTABLE, "Size.userForm.password.short");
		}
		
		if(updateUser.getPassword().length() > 32) {
			errors.setResponse(HttpStatus.NOT_ACCEPTABLE, "Size.userForm.password.long");
		}

		if (!bCryptPasswordEncoder.matches(updateUser.getUsername(), user.getPassword())) {
			errors.setResponse(HttpStatus.NOT_ACCEPTABLE, "Diff.userForm.oldpassword");
		}
	}
}
