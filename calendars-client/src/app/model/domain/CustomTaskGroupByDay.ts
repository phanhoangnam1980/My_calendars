import {CustomImageModel} from './CustomImageModel';

export class CustomTaskGroupByDay {
  day: number;
  dayDetail: string
  listTask: Array<CustomImageModel>;
}
