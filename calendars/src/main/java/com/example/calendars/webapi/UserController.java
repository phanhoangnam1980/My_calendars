package com.example.calendars.webapi;

import java.io.IOException;
import java.net.URI;
import java.security.GeneralSecurityException;
import java.util.HashSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.calendars.domain.Response;
import com.example.calendars.domain.TaskSaveModel;
import com.example.calendars.domain.TokenModel;
import com.example.calendars.domain.model.DetailUser;
import com.example.calendars.domain.model.DeviceUser;
import com.example.calendars.domain.model.Images;
import com.example.calendars.domain.model.User;
import com.example.calendars.firebase.IImageService;
import com.example.calendars.service.DetailUserService;
import com.example.calendars.service.UserService;
import com.example.calendars.validator.CustomUserValidator;
import com.example.calendars.validator.JwtTokenProvider;
import com.example.calendars.validator.UserValidator;

@RestController
public class UserController {
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private UserValidator userValidator;
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	@Autowired
	private UserService userService;
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private CustomUserValidator customUserValidator;
	@Autowired
	private DetailUserService detailUserService;
	@Autowired
	private IImageService imageService;

	@Value("${firebase.baseFirst-image-url}")
	private String imageurl;

	@Value("${firebase.baselast-image-url}")
	private String lasturl;

	@Validated
	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@RequestBody User userForm) {
		TokenModel response = new TokenModel();
		UserDetails userDetails = userDetailsService.loadUserByUsername(userForm.getEmail());
		UsernamePasswordAuthenticationToken emailPasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
				userDetails, userForm.getPassword(), userDetails.getAuthorities());
		Authentication authentication = authenticationManager.authenticate(
				emailPasswordAuthenticationToken/*
												 * new UsernamePasswordAuthenticationToken(userForm.getEmail(),
												 * userForm.getPassword())
												 */);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtTokenProvider.generateToken(authentication);
		response.setAccessToken(jwt);
		return ResponseEntity.ok(response);
	}

	@PostMapping("/signin/google")
	public Response authenticateUser(@RequestBody String tokenid) {
		Object rawback = null;
		try {
			rawback = userService.validLoginWithGoogle(tokenid);
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new Response(HttpStatus.OK, "Not right", "", e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new Response(HttpStatus.OK, "error", "", e.getMessage());
		}
		if (rawback.equals("token error")) {
			return new Response(HttpStatus.OK, "", "", rawback);
		}else {
			TokenModel response = new TokenModel();
			UserDetails userDetails = userDetailsService.loadUserByUsername((String) rawback);
			UsernamePasswordAuthenticationToken emailPasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
					userDetails, "MickeyMinniePlutoHueyLouieDeweyDonaldGoofySacramento", userDetails.getAuthorities());
			Authentication authentication = authenticationManager.authenticate(
					emailPasswordAuthenticationToken/*
													 * new UsernamePasswordAuthenticationToken(userForm.getEmail(),
													 * userForm.getPassword())
													 */);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtTokenProvider.generateToken(authentication);
			response.setAccessToken(jwt);
			return new Response(HttpStatus.OK, "done", "", response);
		}
	}

	@Validated
	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@RequestBody User userForm, BindingResult bindingResult) {
		userValidator.validate(userForm, bindingResult);
		if (bindingResult.hasErrors()) {
			return ResponseEntity.ok("registration error");
		}
		userService.save(userForm);
		userService.SetUpNewUser(userForm.getId(), "");
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(userForm.getUsername()).toUri();
		return ResponseEntity.created(location).body("User registered successfully");
	}

	@GetMapping("/user")
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User result = userService.AvatarOnly(email);
		return result;
	}

	@GetMapping("/user/detail")
	public User getDetailUserInfo() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		CurrentUser.setPassword("");
		Images avatar = imageService.GetImageAvatar(CurrentUser.getId());
		if (avatar != null) {
			CurrentUser.setImage(avatar.getViewImage());
		}
		return CurrentUser;
	}

	@PatchMapping("/user/password")
	public Response UpdateUserPassword(@RequestBody User updateData, Response error) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		customUserValidator.VerifyPassword(updateData, CurrentUser, error);
		if (error.getMessage() != null) {
			return error;
		}
		userService.updatePassword(updateData);
		return new Response(HttpStatus.OK, "no error");
	}

	@PatchMapping("/user/username")
	public void UpdateUserName(@RequestBody User updateData) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		updateData.setEmail(currentUserDetail.getUsername());
		userService.updateUsername(updateData);
	}

	@PatchMapping("/user/email")
	public void UpdateEmail(@RequestBody User updateData) {
		userService.updateUsername(updateData);
	}

	@PutMapping("/user/timezone")
	public void updateTimeZone(@RequestBody TaskSaveModel timezone) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		userService.updateNewTimezone(CurrentUser, timezone.getZoneId());
	}

	@PostMapping("/identify")
	public Response checkCurrentUser(@RequestHeader("user-agent") String info, @RequestBody DeviceUser current) {
		int previousSize;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		if (current.getIp() == null) {
			return new Response(HttpStatus.NO_CONTENT, "There was no ip found from this client");
		}
		current.setAgent(info);
		current.setUserid(CurrentUser.getId());
		HashSet<DeviceUser> checkExits = userService.listDeviceUser(CurrentUser.getId());
		previousSize = checkExits.size();
		checkExits.add(current);
		if ((previousSize + 1) == checkExits.size()) {
			userService.saveDevice(current);
			return new Response(HttpStatus.OK, "new Device", "",
					new DeviceUser(current.getId(), current.getLanguage()));
		} else {
			DeviceUser found = userService.getIdDevice(current.getIp(), info, CurrentUser.getId());
			return new Response(HttpStatus.OK, "old device found", "", found);
		}
	}

	@PatchMapping("/user/language")
	public void UpdateLanguageDevice(@RequestBody DeviceUser updateData) {
		this.userService.updateLanguage(updateData.getLanguage(), updateData.getId());
	}

	@PatchMapping("/user/image")
	public void UpdateAvatar(@RequestBody Images image) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		userService.updateAvatar(CurrentUser, image.getThumbshot());
		imageService.save(image);
		detailUserService.UpdateAvatar(image.getId(), CurrentUser.getId());
	}

	@PatchMapping("/user/{currentChart}/{secondChart}")
	public void UpdateSelectChart(@PathVariable Long currentChart, @PathVariable Long secondChart) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		detailUserService.UpdateProjectChart(currentChart, secondChart, CurrentUser.getId());
	}

	@PostMapping("/user/background")
	public void UpdateBackground(@RequestParam("backgroundFile") MultipartFile files) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		String nameBackground = imageService.ImagesUploadBackground(files);
		detailUserService.Updatebackground(nameBackground, CurrentUser.getId());
	}

	@GetMapping("/user/detailChart")
	public DetailUser GetDetailUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		DetailUser result = detailUserService.FindByuserId(CurrentUser.getId());
		if (result.getBackgroundImage() != null) {
			result.setBackgroundImage(imageurl + result.getBackgroundImage() + lasturl);
		}
		return result;
	}

	@PostMapping("/user/tokenDevice")
	public void updateTokenDevice(@RequestBody DeviceUser device) {
		userService.updateDeviceToken(device);
	}

	@PatchMapping("/user/updateChart/{firstChart}/{secondChart}")
	public void UpdateChartDetail(@PathVariable Long firstChart, @PathVariable Long secondChart) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		detailUserService.UpdateProjectChart(firstChart, secondChart, CurrentUser.getId());
	}
}
