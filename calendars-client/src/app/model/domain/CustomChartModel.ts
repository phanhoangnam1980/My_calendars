import {ModelChart} from './ModelChart';

export class CustomChartModel {
  name: string;
  series: Array<ModelChart>
}
