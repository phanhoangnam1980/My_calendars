export class Response {
  code: number;
  currentSend: string;
  data: string;
  message: string;
  stackTrace: string;
  status: string;
}
