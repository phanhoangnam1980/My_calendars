package com.example.calendars.service;

import java.util.List;

import com.example.calendars.domain.CommentResponse;
import com.example.calendars.domain.model.Comment;
import com.example.calendars.domain.model.Images;

public interface CommentService {
	void save(Comment comment);

	List<Comment> getListByTask(Long taskid, String zonetime, int pagenumber, int sizeInOne);

	List<Comment> getListByProject(Long projectid, String zonetime, int pagenumber, int sizeInOne);

	void ChangeTimeByZoneUser(List<Comment> rawdata, String zonetime);

	void updateContext(Comment comments);

	void deleteComment(Long id);

	List<CommentResponse> processCommentService(List<Comment> rawdata, List<Images> rawImage, String zonetime);
}
