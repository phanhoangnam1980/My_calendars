package com.example.calendars.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.calendars.domain.model.Section;
import com.example.calendars.repository.SectionRepository;

@Service
public class SectionServicelmpl implements SectionService {

	@Autowired
	private SectionRepository sectionRepository;

	@Override
	public void save(Section section) {
		// TODO Auto-generated method stub
		sectionRepository.save(section);
	}

	@Override
	public Section GetSectionById(Long id) {
		// TODO Auto-generated method stub
		return sectionRepository.findByIdSection(id);
	}

	@Override
	public void UpdateTitleSection(Section data) {
		// TODO Auto-generated method stub
		sectionRepository.updateSectionById(data.getId(), data.getTitle());
	}

	@Override
	public void DeleteSection(Long id) {
		// TODO Auto-generated method stub
		sectionRepository.deleteById(id);
	}

	@Override
	public void HideSection(Long id, Boolean flag) {
		// TODO Auto-generated method stub
		sectionRepository.HideSectionById(flag, id);
	}

	@Override
	public Section saveDefault() {
		// TODO Auto-generated method stub
		Section result = new Section();
		result.setTitle("");
		sectionRepository.save(result);
		return result;
	}

	@Override
	public Section getDefaultSection(Long id) {
		// TODO Auto-generated method stub
		return sectionRepository.getDefaultSection(id);
	}

	@Override
	public Section FindSectionInfoByTask(Long id) {
		// TODO Auto-generated method stub
		return sectionRepository.findSectionFromTask(id);
	}
}
