package com.example.calendars.firebase.notification;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.calendars.domain.model.DeviceUser;
import com.example.calendars.domain.model.NotifiTask;
import com.example.calendars.domain.model.User;
import com.example.calendars.service.TaskService;
import com.example.calendars.service.UserService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;

@Service
public class FirebaseNotificationService implements IFirebaseNotificationService {

	private final FirebaseMessaging firebaseMessaging;

	@Autowired
	private UserService userService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private NotificationTaskRepository notificationTaskRepository;

	public FirebaseNotificationService(FirebaseMessaging firebaseMessaging) {
		this.firebaseMessaging = firebaseMessaging;
	}

	@Override
	public List<String> TestsendNotification(AppNotification note, User user) {
		// TODO Auto-generated method stub
		List<String> result = new ArrayList<>();
		HashSet<DeviceUser> devices = userService.listDeviceUser(user.getId());
		Notification notification = Notification.builder().setTitle(note.getTitle()).setBody(note.getMessage()).build();
		// String token =
		// "dHKvGZrOoA1MdBodDRjahs:APA91bHTV48fTGG2KUFlgUC8DY-OOJNAEdlPuwrn1B4NDaTiEFGlOHBKxS37Kp6HIg8QVscMOHgGdn7neeZGyWlrNx4lBrzXtXOwZxGXT9NXgpIp7lcoxuMLUUE5cqelhPqyVGcbWO_w";
		for (DeviceUser device : devices) {
			Message message = Message.builder().setToken(device.getToken()).setNotification(notification).build();
			try {
				result.add(firebaseMessaging.send(message));
			} catch (FirebaseMessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public void DefaultSendNotification() {
		// TODO Auto-generated method stub
		ZonedDateTime afterTime = ZonedDateTime.now().minusHours(1);
		ZonedDateTime beforeTime = ZonedDateTime.now().minusHours(1).minusHours(5);
		List<Object[]> rawData = taskService.getListUpcomingNotification(afterTime, beforeTime);
		for (Object[] data : rawData) {
			Notification notification;
			Message message = null;
			if (data[2].toString().equals("en")) {
				String bodyTask;
				if (data[1] == null) {
					bodyTask = " ";
				} else {
					bodyTask = "Detail:" + data[1].toString();
				}
				notification = Notification.builder()
						.setTitle("Note: This task " + data[0].toString() + "will schedule in one hours")
						.setBody(bodyTask).build();
				if (data[3] != null) {
					message = Message.builder().setToken(data[3].toString()).setNotification(notification).build();
				}
			} else if (data[2].toString().equals("vi")) {
				String bodyTask;
				if (data[1].toString().equals(null)) {
					bodyTask = " ";
				} else {
					bodyTask = "Chi tiết:" + data[1].toString();
				}
				notification = Notification.builder()
						.setTitle(
								"Chú ý: Công việc cần làm này " + data[0].toString() + "sẽ hết hạn trong một tiếng nữa")
						.setBody(bodyTask).build();
				if (data[3] != null) {
					message = Message.builder().setToken(data[3].toString()).setNotification(notification).build();
				}
			}
			try {
				firebaseMessaging.send(message);
			} catch (FirebaseMessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void saveNotifyTask(NotifiTask task) {
		// TODO Auto-generated method stub
		notificationTaskRepository.save(task);
	}

	@Override
	public void UpdateNotifyTask(NotifiTask task) {
		// TODO Auto-generated method stub
		notificationTaskRepository.UpdateTypeNotification(task.getType(), task.getDuration(), task.getTaskid());
	}

	@Override
	public void DeleteNotifyTask(NotifiTask task) {
		// TODO Auto-generated method stub
		notificationTaskRepository.DeleteNotification(task.getTaskid());
	}

	@Override
	public NotifiTask FindByTaskid(Long taskid) {
		// TODO Auto-generated method stub
		return notificationTaskRepository.findByTaskid(taskid);
	}

	@Override
	public void CustomTimeSendNotification() {
		// TODO Auto-generated method stub
		List<Object[]> rawData = notificationTaskRepository.CheckListTask();
		for (Object[] data : rawData) {
			ZonedDateTime current = ZonedDateTime.now();
			ZonedDateTime compare = (ZonedDateTime) data[4];
			switch (Integer.parseInt(data[0].toString())) {
			case 1:
				current.plusHours(Integer.parseInt(data[1].toString()));
				break;
			case 2:
				current.plusDays(Integer.parseInt(data[1].toString()));
				break;
			case 3:
				current.plusMonths(Integer.parseInt(data[1].toString()));
				break;
			}
			if (Duration.between(current, compare).toMinutesPart() < 5) {
				Notification notification;
				Message message = null;
				if (data[5].toString().equals("en")) {
					String bodyTask;
					if (data[3] == null) {
						bodyTask = " ";
					} else {
						bodyTask = "Detail:" + data[1].toString();
					}
					notification = Notification.builder()
							.setTitle("Note: This task " + data[2].toString() + "will schedule in one hours")
							.setBody(bodyTask).build();
					if (data[3] != null) {
						message = Message.builder().setToken(data[6].toString()).setNotification(notification).build();
					}
				} else if (data[5].toString().equals("vi")) {
					String bodyTask;
					if (data[3].toString().equals(null)) {
						bodyTask = " ";
					} else {
						bodyTask = "Chi tiết:" + data[1].toString();
					}
					notification = Notification.builder().setTitle(
							"Chú ý: Công việc cần làm này " + data[2].toString() + "sẽ hết hạn trong một tiếng nữa")
							.setBody(bodyTask).build();
					if (data[3] != null) {
						message = Message.builder().setToken(data[6].toString()).setNotification(notification).build();
					}
				}
				try {
					firebaseMessaging.send(message);
				} catch (FirebaseMessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
