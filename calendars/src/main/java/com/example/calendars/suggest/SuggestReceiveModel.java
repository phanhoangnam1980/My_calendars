package com.example.calendars.suggest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuggestReceiveModel {
	private String idDevice;
	
	private String prefix;
}
