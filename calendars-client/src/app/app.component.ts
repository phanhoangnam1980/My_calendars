import {Component, OnInit} from '@angular/core';
import {TranslocoService} from '@ngneat/transloco';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'calendars-client';
  private defaultLanguage = 'en-GB';
  private readonly DefaultListLanguage: Array<string>;

  constructor(private translocoService: TranslocoService) {
    this.DefaultListLanguage = ['en', 'vi'];
  }

  ngOnInit(): void {
    this.defaultLanguage = navigator.language;
    this.DefaultListLanguage.every((language) => {
      if (language === this.defaultLanguage.slice(0, 2)) {
        this.translocoService.setActiveLang(this.defaultLanguage.slice(0, 2));
        return false;
      } else {
        this.translocoService.setActiveLang('vi');
        return true;
      }
    });
  }
}
