import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './signin/signin.component';
import {SignupComponent} from './signup/signup.component';
import {MainComponent} from './main_calendars/main.component';
import {UpcomingComponent} from './upcoming_day/upcoming.component';
import {ProjectsComponent} from './projects/projects.component';
import {TodayComponent} from './today/today.component';
import {SearchComponent} from './search/search.component';
import {WelcomePageComponent} from './welcome-page/welcome-page.component';


const routes: Routes = [
  {path: '', component: WelcomePageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {
    path: 'app', component: MainComponent, children:
      [
        {path: 'upcoming', component: UpcomingComponent},
        {path: 'today', component: TodayComponent},
        {path: 'project/:id', component: ProjectsComponent},
        {path: 'search/:value', component: SearchComponent}
      ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
