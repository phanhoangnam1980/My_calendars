package com.example.calendars.suggest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PruningRadixNodeModel {
	private String keys;
	
	private PruningRadixNode node;
	
	public PruningRadixNodeModel() {
		super();
	}
	
	public PruningRadixNodeModel(String keys, PruningRadixNode node) {
		super();
		this.keys = keys;
		this.node = node;
	}
}
