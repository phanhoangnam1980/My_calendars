import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../model/User';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class SignupService {

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {
  }

  getRegistration(): Observable<any> {
    return this.http.get(environment.serverPort + '/registration', {headers: this.headers});
  }

  postRegistration(user: User): Observable<any> {
    return this.http.post(environment.serverPort + '/signup', user, {headers: this.headers, responseType: 'text'});
  }

}
