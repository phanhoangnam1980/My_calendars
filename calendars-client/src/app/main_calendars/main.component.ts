import {AfterViewInit, Component, Input, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Group} from '../model/group';
import {MatDialog} from '@angular/material/dialog';
import {AddprojectComponent} from './addproject/addproject.component';
import {UserService} from '../service/user.service';
import {User} from '../model/User';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {AuthenticationService} from '../service/authentication.service';
import {ChannelService} from '../service/Channel.service';
import {EditProjectComponent} from './edit-project/edit-project.component';
import {ShareDataService} from '../service/ShareData.service';
import {element} from 'protractor';
import {TaskService} from '../service/task.service';
import * as moment from 'moment';
import {TranslocoService} from '@ngneat/transloco';
import {DateTimeCalendarsService} from '../service/DateTimeCalendars.service';
import {UsersDetailComponent} from './users-detail/users-detail.component';
import {MatMenu, MatMenuTrigger} from '@angular/material/menu';
import {SocketService} from '../service/socket.service';
import {SearchModel} from '../model/domain/SearchModel';
import {ModalAddTaskComponent} from './modal-add-task/modal-add-task.component';
import {IpAddressService} from '../service/IpAddress.service';
import {DeviceModel} from '../model/DeviceModel';
import {Response} from '../model/domain/Response';
import SwiperCore, {Autoplay, Pagination, Navigation} from 'swiper';
import {environment} from '../../environments/environment';
import {DetailUser} from '../model/DetailUser';
import {getMessaging, getToken, onMessage} from 'firebase/messaging';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NgxSpinnerService} from 'ngx-spinner';
import {CustomImageModel} from '../model/domain/CustomImageModel';
import {DetailTaskComponent} from './detail-task/detail-task.component';
import {GoogleSigninService} from '../service/google-signin.service';

SwiperCore.use([Autoplay, Pagination, Navigation]);

@Component({
  selector: 'app-main-page',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, AfterViewInit {
  public StoreStateSide: string;
  public isDropDown: boolean;
  public ListDataGroup: Array<Group>;
  public DefaultIdChannel: number;
  private defaultDataChannel: Group;
  public IdChannelProject: bigint;
  private currentUserSubscription: Subscription;
  public dataUser: User;
  public currentUser: User;
  private defaultLanguage = 'en-GB';
  public labelChart = true;
  public menu = 1;
  public LanguageList: [];
  public searchResultData: Array<SearchModel>;
  @Input()
  public dataSearch: string;
  @ViewChild('searchResult', {static: false})
  SearchMenu: MatMenu;
  @ViewChild('chartMenuTrigger', {static: false})
  chartTrigger: MatMenuTrigger;

  // store select state
  public selectStatue: string;

  private flagChartDefault: number;
  private readonly DefaultListLanguage: Array<string>;
  public activeLanguageLocation: number;
  public activechart = false;
  public activePin = false;
  private currentDevice: DeviceModel;

  // image preview
  public imagePreview: Array<CustomImageModel>;

  // detailImage
  public imagebackground: string;

  // message notification
  message: any = null;

  constructor(public dialog: MatDialog, private userService: UserService, private router: Router,
              private authenticationService: AuthenticationService, private channelService: ChannelService,
              private shareData: ShareDataService, private taskService: TaskService, private dateService: DateTimeCalendarsService,
              private translocoService: TranslocoService, private _renderer2: Renderer2, private socketService: SocketService,
              private ipAddress: IpAddressService, private snackBar: MatSnackBar, private spinner: NgxSpinnerService,
              private googleSignin: GoogleSigninService) {
    this.spinner.show().then();
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.dataUser = user;
      if (!user) {
        this.dataUser = JSON.parse(localStorage.getItem('token'));
        if (!this.dataUser) {
          this.router.navigate(['login']).then();
        }
      }
    });
    this.imagebackground = '../../assets/image/poppy-watercolor-red-abstract-flower-olga-shvartsur-testing.jpg';
    this.currentUser = new User();
    this.DefaultListLanguage = ['en', 'vi'];
    this.searchResultData = new Array<SearchModel>();
    this.currentDevice = new DeviceModel();
    this.selectStatue = '';
    this.translocoService.selectTranslateObject('MultiLanguage').subscribe(translate => {
      this.LanguageList = translate;
    });
    this.imagePreview = new Array<CustomImageModel>();
    this.taskService.GetImageFromTask().subscribe((result: Array<CustomImageModel>) => {
      this.imagePreview = result;
    });
  }

  ngOnInit(): void {
    this.ListDataGroup = new Array<Group>();
    this.StoreStateSide = 'true';
    this.isDropDown = true;
    this.DefaultIdChannel = 1;
    this.flagChartDefault = 1;
    this.channelService.GetListCustomChannel().subscribe((data: Group[]) => {
      this.ListDataGroup = data;
    });
    this.channelService.GetDefaultChannel().subscribe((data: Group) => {
      this.DefaultIdChannel = Number(data.groupid);
      if (this.router.url.split('/').length > 3) {
        // process this was on default channel
        if (this.router.url.split('/')[3] === this.DefaultIdChannel.toString()) {
          this.selectStatue = 'DefaultGroup';
        }
      }
      this.defaultDataChannel = data;
    });
    if (this.router.url.split('/').length === 3) {
      this.selectStatue = this.router.url.split('/')[2];
    }
    this.userService.getAvatarUsername().subscribe((user: User) => {
      this.currentUser = user;
      const currentZone = moment().format('ZZ');
      if (user.zonetime !== currentZone) {
        this.userService.UpdateTimeZone(currentZone).subscribe();
      }
    });
    this.currentDevice.language = this.translocoService.getActiveLang();
    this.ipAddress.GetIpAndInfoClient().subscribe((result: { ip: string, location: object }) => {
      this.currentDevice.ip = result.ip;
      this.ipAddress.CheckInfoClient(this.currentDevice).subscribe((receive: Response) => {
        this.currentDevice = receive.data as unknown as DeviceModel;
        console.log(this.currentDevice.id);
        this.ipAddress.SetIdClient(this.currentDevice.id);
        if (receive.code === 200 && receive.message === 'old device found') {
          if (this.translocoService.getActiveLang() !== this.currentDevice.language) {
            this.translocoService.setActiveLang(this.currentDevice.language);
          }
        }
        this.DefaultListLanguage.forEach((language, index) => {
          if (this.translocoService.getActiveLang() === language) {
            this.activeLanguageLocation = index;
          }
        });
        this.socketService.setup(this.currentUser.username, this.currentDevice.id);
      });
    });
    this.userService.GetDetailOptionUser().subscribe(result => {
      if (result.backgroundImage != null) {
        this.imagebackground = result.backgroundImage;
      }
    });
    this.requestPermission();
    this.listen();
  }

  ngAfterViewInit(): void {
    this.spinner.hide().then();
    if (this.flagChartDefault === 1) {
      this.flagChartDefault = 0;
    }
    this.shareData.DataAddProject.subscribe((data: Group) => {
      this.ListDataGroup.push(data);
    });
    this.shareData.AddNewUsername.subscribe(flag => {
      if (flag === true) {
        this.userService.getAvatarUsername().subscribe((user: User) => {
          this.currentUser = user;
        });
      }
    });
    this.socketService.currentDataSearch.subscribe((data: Array<SearchModel>) => {
      this.searchResultData = data;
    });
    this.shareData.CurrentStatusUpdate.subscribe(flag => {
      if (flag === true) {
        this.channelService.GetListCustomChannel().subscribe((data: Group[]) => {
          this.ListDataGroup = data;
        });
      }
    });
  }

  routeingProject() {
    this.isDropDown = !this.isDropDown;
  }

  openAddModal() {
    this.dialog.open(AddprojectComponent, {
      width: '650px',
      data: {name: ''}
    });
  }

  exitButton() {
    this.dataUser = null;
    this.currentUser = null;
    window.localStorage.clear();
    this.googleSignin.signOut();
    this.router.navigate(['login']).then();
  }

  DataProjectIdLocation(dataProject) {
    this.IdChannelProject = dataProject;
  }

  DeleteGroupChannel() {
    this.channelService.DeleteThisChannel(this.IdChannelProject).subscribe();
    this.ListDataGroup.forEach((elements, index) => {
      if (elements.groupid === this.IdChannelProject) {
        this.ListDataGroup.splice(index, 1);
      }
    });
  }

  openEditChannelModal() {
    const foundData = this.ListDataGroup.find(x => x.groupid === this.IdChannelProject);
    this.dialog.open(EditProjectComponent, {
      width: '650px',
      data: foundData
    });
  }

  next($event) {
    while (this.menu < 2) {
      this.menu++;
      $event.stopPropagation();
    }
  }

  previous($event) {
    this.menu--;
    $event.stopPropagation();
  }

  resetStep() {
    this.menu = 1;
  }

  changeLanguage(location) {
    // this still not have send to server and identify what device this was
    if (location !== this.activeLanguageLocation) {
      this.activeLanguageLocation = location;
      this.translocoService.setActiveLang(this.DefaultListLanguage[location]);
      this.ipAddress.updateLanguage(this.translocoService.getActiveLang()).subscribe();
    }
  }

  isOpen() {
    this.activechart = true;
  }

  isCloseChart() {
    this.activechart = false;
  }

  openDetailUser(): void {
    this.dialog.open(UsersDetailComponent, {
      width: '750px',
      data: ''
    });
  }

  openAddTask() {
    this.dialog.open(ModalAddTaskComponent, {
      width: '900px',
      data: {
        channel: this.defaultDataChannel
      },
      backdropClass: 'AddTaskBackground'
    });
  }

  SearchResult(dataSearch) {
    this.socketService.SendSearchPrefix(dataSearch);
  }

  changeColor() {
    const el = document.getElementById(this.SearchMenu.panelId);
    this._renderer2.setStyle(el, 'background-color', '#2f3136');
    this._renderer2.setStyle(el, 'scrollbar-width', 'none');
  }

  OpenActivePin() {
    this.activePin = true;
  }

  CloseActivePin() {
    this.activePin = false;
  }

  RouterToHome() {
    if (this.router.url !== '/app/today') {
      this.router.navigate(['/app/today']).then();
    }
  }

  SelectActive(status: string) {
    this.selectStatue = status;
  }

  requestPermission() {
    const messaging = getMessaging();
    getToken(messaging,
      {vapidKey: environment.firebase.vapidKey}).then(
      (currentToken) => {
        if (currentToken) {
          console.log('Hurraaa!!! we got the token.....');
          this.currentDevice.token = currentToken;
          this.userService.UpdateToken(this.currentDevice);
        } else {
          console.log('No registration token available. Request permission to generate one.');
        }
      }).catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
    });
  }

  listen() {
    const messaging = getMessaging();
    onMessage(messaging, (payload) => {
      console.log('Message received. ', payload);
      this.message = payload;
      this.translocoService.selectTranslate('TaskComeToEnd').subscribe(trans => {
        this.translocoService.selectTranslate('buttonNotification').subscribe(buttonTranslate => {
          this.snackBar.open(trans + ' ' + payload.notification.title, buttonTranslate, {
            duration: 5000,
            horizontalPosition: 'center',
            verticalPosition: 'bottom'
          });
        });
      });
    });
  }

  clickImage(index) {
    const raw = this.imagePreview[index];
    this.dialog.open(DetailTaskComponent, {
      width: '900px',
      data: {
        task: raw.task,
        section: raw.section
      }
    });
  }
}
