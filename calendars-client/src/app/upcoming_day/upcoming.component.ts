import {AfterViewChecked, AfterViewInit, Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {DateTimeCalendarsService} from '../service/DateTimeCalendars.service';
import {DayDisplay} from '../model/DayDisplay';
import {ShareDataService} from '../service/ShareData.service';
import * as moment from 'moment';
import {ChannelService} from '../service/Channel.service';
import {SectionService} from '../service/Section.service';
import {Group} from '../model/group';
import {Section} from '../model/Section';
import {TranslocoService} from '@ngneat/transloco';
import {CustomChartModel} from '../model/domain/CustomChartModel';
import {TaskService} from '../service/task.service';
import {TaskGroup} from '../model/domain/TaskGroup';
import {DetailTaskComponent} from '../main_calendars/detail-task/detail-task.component';
import {MatDialog} from '@angular/material/dialog';
import {CustomTaskGroupByDay} from '../model/domain/CustomTaskGroupByDay';
import {DisplayDateData} from '../model/domain/DisplayDateData';
import {Response} from '../model/domain/Response';
import {TaskMessage} from '../model/TaskMessage';
import {NewModelTask} from '../model/domain/newModelTask';

@Component({
  selector: 'app-upcoming-day',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.css']
})
export class UpcomingComponent implements OnInit, AfterViewInit, AfterViewChecked {
  @Input()
  datePicked: any;
  private DateForward: number;
  private DateQueue: number;
  public DisplayRow: string[];
  public DisplayList: string[];
  public dayDisplay: DayDisplay;
  public hideflag: boolean;
  public numbercheck: number;
  public DefaultChannel: Group;
  public DefaultSection: Section;
  private currentLanguage: string;
  public yAxisLabel: string;

  public upcomingTask: Array<CustomChartModel>;
  // show task
  public datatask: Array<CustomTaskGroupByDay>;
  public checkfinished: boolean;
  public locationcheckFinished: bigint;
  public dateFinishTask: any;
  @ViewChild('datepickerFooter', {static: false}) datepickerFooter: ElementRef;
  public CheckAddTimeTaskState = false;
  public DisplayDate: DisplayDateData;
  public TimeSetTaskFinish: any;
  public LocationSelect: number;
  public SectionSelect: number;
  public DataSelect: NewModelTask;
  // display add task button
  public locationMouseTask: number;

  view: any[] = [1200, 150];
  public DataChart: Array<TaskGroup>;

  constructor(private shareData: ShareDataService, private ListDateService: DateTimeCalendarsService,
              private channelService: ChannelService, private sectionService: SectionService, public dialog: MatDialog,
              private transloco: TranslocoService, private taskService: TaskService,
              private calendarsService: DateTimeCalendarsService) {
    this.currentLanguage = this.transloco.getActiveLang();
    this.datePicked = new Date(Date.now());
    this.transloco.selectTranslate('PercentComplete').subscribe(result => {
      this.yAxisLabel = result;
    });
    this.DataSelect = new NewModelTask();
    this.datatask = new Array<CustomTaskGroupByDay>();
    this.taskService.UpcomingTask(0).subscribe((rawTask: Array<CustomTaskGroupByDay>) => {
      this.datatask = rawTask;
      this.datatask.splice(0, 1);
      this.datatask = this.calendarsService.AddThisWeek(this.datatask, this.currentLanguage);
    });
  }

  ngOnInit(): void {
    this.DateForward = 0;
    this.DateQueue = 2;
    this.numbercheck = -1;
    this.hideflag = false;
    this.DisplayRow = this.ListDateService.getShortWeek(this.DateForward, this.currentLanguage);
    this.DisplayList = this.ListDateService.DisplayFirstThreeWeek(this.currentLanguage);
    this.dayDisplay = this.ListDateService.DetailFirstDay(this.DateForward, this.currentLanguage);
    this.channelService.GetDefaultChannel().subscribe(channel => {
      this.DefaultChannel = channel;
      this.sectionService.GetDefaultSection(channel.groupid).subscribe(section => {
        this.DefaultSection = section;
      });
    });
    this.taskService.GetDataChartTomorrow().subscribe(result => {
      this.upcomingTask = result;
    });
    this.taskService.GetDataClick().subscribe(result => {
      this.DataChart = result;
    });
  }

  ngAfterViewChecked(): void {
  }

  ngAfterViewInit(): void {
    this.shareData.currentStateAddTask.subscribe(checkData => {

    });
    this.shareData.CurrentCancelStatus.subscribe(result => {
      if (result === true) {
        this.LocationSelect = -4;
        this.SectionSelect = -4;
      }
    });
    this.shareData.currentCancelRequestNewTask.subscribe(statusCancel => {
      if (statusCancel === true) {
        this.hideflag = false;
      }
    });
    this.transloco.langChanges$.subscribe(changing => {
      if (this.currentLanguage !== changing) {
        this.currentLanguage = changing;
        this.datatask = this.calendarsService.AddThisWeek(this.datatask, this.currentLanguage);
        this.DisplayRow = this.ListDateService.getShortWeek(this.DateForward, this.currentLanguage);
        this.DisplayList = this.ListDateService.DisplayFirstThreeWeek(this.currentLanguage);
        this.dayDisplay = this.ListDateService.DetailFirstDay(this.DateForward, this.currentLanguage);
      }
    });
    this.shareData.DataAddTask.subscribe(taskData => {
      if (taskData !== null) {
        this.taskService.UpcomingTask(0).subscribe((rawTask: Array<CustomTaskGroupByDay>) => {
          this.datatask = rawTask;
          this.datatask.splice(0, 1);
          this.datatask = this.calendarsService.AddThisWeek(this.datatask, this.currentLanguage);
          this.LocationSelect = -4;
          this.SectionSelect = -4;
        });
      }
    });
  }

  nextWeek() {
    this.DateForward++;
    this.DisplayRow = this.ListDateService.getShortWeek(this.DateForward, this.currentLanguage);
    this.dayDisplay = this.ListDateService.DetailFirstDay(this.DateForward, this.currentLanguage);
  }

  previousWeek() {
    if (this.DateForward > 0) {
      this.DateForward--;
      this.DisplayRow = this.ListDateService.getShortWeek(this.DateForward, this.currentLanguage);
      this.dayDisplay = this.ListDateService.DetailFirstDay(this.DateForward, this.currentLanguage);
    }
  }

  getWhereDisplay(idOpen) {
    this.hideflag = true;
    this.numbercheck = idOpen;
  }

  enterAddTask(location) {
    this.locationMouseTask = location;
  }

  outAddTask() {
    this.locationMouseTask = -1;
  }

  PickedDate() {
    const rawData = this.datePicked.toString().split(' ');
    this.dayDisplay.Year = rawData[3];
    this.dayDisplay.Month = moment().locale(this.currentLanguage).month(rawData[1]).format('MMMM').toString();
  }

  DynamicWidth(event) {
    this.view = [event.target.innerWidth / 1.35, 150];
  }

  onSelect(data): void {
    const location = data.name.split('#');
    const SectionData = new Section();
    const selectedTask = this.DataChart[location[2]];
    SectionData.id = selectedTask.data.sectionId;
    SectionData.title = selectedTask.data.titleSection;
    this.dialog.open(DetailTaskComponent, {
      width: '900px',
      data: {
        task: selectedTask,
        section: SectionData
      }
    });
  }

  enterCheckAnimation(idlocation) {
    this.checkfinished = true;
    this.locationcheckFinished = idlocation;
  }

  leaveCheckAnimation() {
    this.checkfinished = false;
  }

  onOpen() {
    this.appendFooter();
    this.CheckAddTimeTaskState = false;
  }

  onClose() {
    this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
  }

  private appendFooter() {
    const matCalendar = document.getElementsByClassName('mat-datepicker-content')[0] as HTMLElement;
    matCalendar.appendChild(this.datepickerFooter.nativeElement);
  }

  onclickAddTime() {
    this.CheckAddTimeTaskState = true;
  }

  cancelAddTime() {
    this.CheckAddTimeTaskState = false;
  }

  processFinishAddTask(pickerDate) {
    pickerDate.close();
  }

  PinnedTask(idtask) {
    this.taskService.TaskPinned(idtask).subscribe((result: Response) => {
      switch (result.code) {
        case 200:
          this.shareData.setDataAddTask(new TaskMessage());
          break;
        case 507:
          break;
      }
    });
  }

  DeletedTask(idtask) {
    this.taskService.DeleteTask(idtask).subscribe(() => {
      this.shareData.setDataAddTask(new TaskMessage());
    });
  }

  ChangePriority(idtask, priority: number) {
    this.taskService.SetKindForTask(idtask, BigInt(priority)).subscribe((result: Response) => {
      switch (result.code) {
        case 200:
          this.shareData.setDataAddTask(new TaskMessage());
          break;
        case 507:
          break;
      }
    });
  }

  SelectEdits(location: number, section: number) {
    this.LocationSelect = location;
    this.SectionSelect = section;
    const taskSelect = this.datatask[location].listTask[section];
    this.DataSelect.tittle = taskSelect.task.data.title;
    this.DataSelect.datedata = taskSelect.task.data.date;
    this.DataSelect.description = taskSelect.task.data.description;
    this.DataSelect.groupId = Number(taskSelect.task.data.groupid);
    this.DataSelect.sectionId = Number(taskSelect.task.data.sectionId);
    this.DataSelect.parentId = taskSelect.task.data.taskid;
  }

  TaskFinished(idlocation, group, idtask) {
    this.checkfinished = false;
    this.datatask[group].listTask.splice(idlocation, 1);
    this.taskService.TaskFinished(idtask).subscribe();
  }

  DetailTask(location, index) {
    const taskSelect = this.datatask[location].listTask[index];
    this.dialog.open(DetailTaskComponent, {
      width: '900px',
      data: {
        task: taskSelect.task,
        section: taskSelect.section
      }
    });
  }
}
