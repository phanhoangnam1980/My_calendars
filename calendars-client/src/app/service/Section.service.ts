import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DateTimeCalendarsService} from './DateTimeCalendars.service';
import {TokenResponse} from '../model/TokenResponse';
import {Injectable} from '@angular/core';
import {Section} from '../model/Section';
import {environment} from '../../environments/environment';

@Injectable()
export class SectionService {
  constructor(private http: HttpClient, private dateTimeData: DateTimeCalendarsService) {
  }

  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));

  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    authorization: 'Bearer ' + this.token.accessToken
  });

  saveNewSection(saveData: Section, GroupId: number) {
    return this.http.post(environment.serverPort + '/section/' + GroupId, JSON.stringify(saveData), {headers: this.headers});
  }

  getSectionById(SectionId: number) {
    return this.http.get(environment.serverPort + '/section/' + SectionId, {headers: this.headers});
  }

  updateSection(saveData: Section) {
    return this.http.put(environment.serverPort + '/', JSON.stringify(saveData), {headers: this.headers});
  }

  hideThisSection(SectionId: bigint) {
    return this.http.put(environment.serverPort + '/section/' + SectionId, '', {headers: this.headers});
  }

  DeleteThisSection(SectionId: bigint) {
    return this.http.delete(environment.serverPort + '/section/' + SectionId, {headers: this.headers});
  }

  GetDefaultSection(ProjectId: bigint) {
    return this.http.get<Section>(environment.serverPort + '/DefaultSection/' + ProjectId, {headers: this.headers});
  }

  GetDetailSectionTask(id: bigint) {
    return this.http.get<Section>(environment.serverPort + '/SectionTask/' + id, {headers: this.headers});
  }
}
