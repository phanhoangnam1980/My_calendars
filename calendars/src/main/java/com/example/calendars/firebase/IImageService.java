package com.example.calendars.firebase;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.example.calendars.domain.CustomImageTask;
import com.example.calendars.domain.model.Images;

public interface IImageService {
	void save(Images image);
	// Sign and return the URL for Post using credentials from above
	String getSignedUrl(String objectName, String mimeType);

	// Send POST request to the signed URL using custom headers and an empty body,
	// which returns the actual upload location
	String getLocation(String objectName, String mimeType) throws IOException;

	Images UploadToStorage(MultipartFile multipartFile, String mimeType);

	String generateFileName(String name);

	byte[] resizeImage(InputStream input, int width, int height, String mimeType);
	
	Images ImageUploadAvatar(MultipartFile multipartFile);
	
	List<Images> getListImageFromUser(Long userid);
	
	String ImagesUploadBackground(MultipartFile multipartFile);
	
	List<Images> GetListImageFromCommentTask(Long taskid);
	
	List<Images> GetListImageFromCommentProject(Long projectId);
	
	List<CustomImageTask> GetListImageByTask(Long userid);
	
	Images GetImageAvatar(Long userid);
}
