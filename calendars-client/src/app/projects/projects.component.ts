import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ShareDataService} from '../service/ShareData.service';
import {ActivatedRoute} from '@angular/router';
import {ChannelService} from '../service/Channel.service';
import {Group} from '../model/group';
import {TaskService} from '../service/task.service';
import {TaskData} from '../model/domain/TaskData';
import {SectionService} from '../service/Section.service';
import {Response} from '../model/domain/Response';
import {TaskMessage} from '../model/TaskMessage';
import {MatDialog} from '@angular/material/dialog';
import {CommentComponent} from './comment/comment.component';
import {DetailTaskComponent} from '../main_calendars/detail-task/detail-task.component';
import {TaskGroup} from '../model/domain/TaskGroup';
import {CustomTaskMessage} from '../model/domain/customTaskMessage';
import {TaskGroupModel} from '../model/domain/TaskGroupModel';
import {Section} from '../model/Section';
import {TranslocoService} from '@ngneat/transloco';
import {NewModelTask} from '../model/domain/newModelTask';

@Component({
  selector: 'app-project-task',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit, AfterViewInit {
  public Displaytitle: string;
  private Displaytitlevalue: string;
  public colorChannel: string;
  public hideflag: boolean;
  public checkfinished: boolean;
  public locationcheckFinished: bigint;
  public locationSection: number;
  public locationAddTask: number;
  public showSectionFlag: boolean;
  public AddSection: boolean;
  private lastSection: number;
  public displayListTask: Array<TaskData>;
  private channelId: number;
  public locationMouseTask: number;
  public DataSelect: NewModelTask;

  public LocationSelect: number;
  public SectionSelect: number;
  // random color background
  public colorbackgroudIndentify = ['#FFFF00', '#FFA500', '#FFC0CB', '#f5f5dc'];
  public randomBackgroundResult: Array<string>;

  constructor(private shareData: ShareDataService, private route: ActivatedRoute, private channelService: ChannelService,
              private taskService: TaskService, private sectionService: SectionService, public dialog: MatDialog,
              private transloco: TranslocoService) {
    this.transloco.selectTranslate('DefaultGroup').subscribe(result => {
      this.Displaytitle = result;
    });
    this.randomBackgroundResult = new Array<string>();
  }

  ngOnInit(): void {
    this.LayoutTask();
  }

  ngAfterViewInit(): void {
    this.shareData.currentStateAddTask.subscribe((data) => {
      console.log(data);
    });
    this.shareData.DataAddTask.subscribe(taskData => {
      if (taskData !== null) {
        this.taskService.listAllTaskGroup(this.channelId).subscribe(dataTask => {
          this.displayListTask = dataTask;
          this.lastSection = this.displayListTask.length - 1;
          for (let i = 0; i < this.lastSection; i++) {
            this.randomBackgroundResult.push(this.colorbackgroudIndentify[Math.floor(Math.random() * 4)]);
          }
          console.log(this.randomBackgroundResult);
        });
      }
    });
    this.shareData.DataAddSection.subscribe((newSection) => {
      if (newSection) {
        this.taskService.listAllTaskGroup(this.channelId).subscribe(dataTask => {
          this.displayListTask = dataTask;
          this.lastSection = this.displayListTask.length - 1;
        });
      }
    });
    this.shareData.currentCancelRequestNewTask.subscribe(statusCancel => {
      if (statusCancel === true) {
        this.hideflag = false;
      }
    });
    this.shareData.currentCancelRequestNewSection.subscribe(statusSectionCancel => {
      if (statusSectionCancel === true) {
        this.AddSection = false;
      }
    });
    this.shareData.CurrentCancelStatus.subscribe(result => {
      if (result === true) {
        this.LocationSelect = -4;
        this.SectionSelect = -4;
      }
    });
  }

  enterCheckAnimation(idlocation) {
    this.checkfinished = true;
    this.locationcheckFinished = idlocation;
  }

  leaveCheckAnimation() {
    this.checkfinished = false;
  }

  enterSection(idlocation) {
    if (this.AddSection === false) {
      this.locationSection = idlocation;
      this.showSectionFlag = true;
    }
  }

  leaveSection() {
    this.showSectionFlag = false;
  }

  checkShowSection(idlocation) {
    return !(this.showSectionFlag === true && this.locationSection === idlocation);
  }

  showSection(location) {
    this.AddSection = true;
    this.showSectionFlag = false;
    this.locationSection = location;
  }

  openDataAddTask(locationTask) {
    this.hideflag = true;
    this.locationAddTask = locationTask;
  }

  TaskFinishedOverDue(idlocation, idlocationGroup, idtask) {
    this.checkfinished = false;
    this.displayListTask[idlocationGroup].listtask.splice(idlocation, 1);
    this.taskService.TaskFinished(idtask).subscribe();
  }

  DeleteSection(idSection, deleteLocation) {
    this.sectionService.DeleteThisSection(idSection).subscribe();
    this.displayListTask.splice(deleteLocation, 1);
  }

  LayoutTask() {
    this.displayListTask = new Array<TaskData>();
    this.route.params.subscribe((params) => {
      this.channelId = +params.id;
      this.channelService.GetInfoChannel(this.channelId).subscribe((data: Group) => {
        this.Displaytitlevalue = data.titleGroupTask;
        if(data.titleGroupTask !== 'Default Group') {
          this.Displaytitle = data.titleGroupTask;
        }
        this.colorChannel = data.colorSpecs;
      });
      this.taskService.listAllTaskGroup(this.channelId).subscribe(dataTask => {
        this.displayListTask = dataTask;
        this.lastSection = this.displayListTask.length - 1;
        for (let i = 0; i < this.lastSection; i++) {
          this.randomBackgroundResult.push(this.colorbackgroudIndentify[Math.floor(Math.random() * 4)]);
        }
      });
    });
    this.hideflag = false;
    this.AddSection = false;
    // value Add select
    this.LocationSelect = -4;
    this.SectionSelect = -4;
    this.DataSelect = new NewModelTask();
  }

  clickAddSection() {
    this.AddSection = true;
    this.showSectionFlag = false;
    this.locationSection = this.lastSection;
  }

  enterAddTask(location) {
    this.locationMouseTask = location;
  }

  outAddTask() {
    this.locationMouseTask = -1;
  }

  PinnedTask(idtask) {
    this.taskService.TaskPinned(idtask).subscribe((result: Response) => {
      switch (result.code) {
        case 200:
          this.shareData.setDataAddTask(new TaskMessage());
          break;
        case 507:
          break;
      }
    });
  }

  DeletedTask(idtask) {
    this.taskService.DeleteTask(idtask).subscribe(() => {
      this.shareData.setDataAddTask(new TaskMessage());
    });
  }

  ChangePriority(idtask, priority: number) {
    this.taskService.SetKindForTask(idtask, BigInt(priority)).subscribe((result: Response) => {
      switch (result.code) {
        case 200:
          this.shareData.setDataAddTask(new TaskMessage());
          break;
        case 507:
          break;
      }
    });
  }

  openAddComment() {
    this.dialog.open(CommentComponent, {
      width: '850px',
      data: {
        ColorChannel: this.colorChannel,
        ChannelTitle: this.Displaytitlevalue,
        ChannelID: this.channelId
      }
    });
  }

  MoreDetail(zoneSelect, index) {
    const selectedTask = new TaskGroup();
    const SectionData = new Section();
    selectedTask.data = new TaskGroupModel();
    const selectedData = this.displayListTask[zoneSelect].listtask[index];
    selectedTask.overdue = selectedData.overdue;
    selectedTask.data.taskid = selectedData.task.taskid;
    selectedTask.data.title = selectedData.task.title;
    selectedTask.data.description = selectedData.task.description;
    selectedTask.data.date = selectedData.task.date;
    selectedTask.data.colorSpecs = this.colorChannel;
    selectedTask.data.titerGroupTask = this.Displaytitlevalue;
    selectedTask.data.groupid = BigInt(this.channelId);
    SectionData.id = this.displayListTask[zoneSelect].sectionid;
    SectionData.title = this.displayListTask[zoneSelect].title;
    this.dialog.open(DetailTaskComponent, {
      width: '900px',
      data: {
        task: selectedTask,
        section: SectionData
      }
    });
  }

  SelectEdits(loccation: number, section: number) {
    this.LocationSelect = loccation;
    this.SectionSelect = section;
    const selectData = this.displayListTask[section];
    const taskSelect = selectData.listtask[loccation];
    this.DataSelect.tittle = taskSelect.task.title;
    this.DataSelect.datedata = taskSelect.task.date;
    this.DataSelect.description = taskSelect.task.description;
    this.DataSelect.groupId = this.channelId;
    this.DataSelect.sectionId = Number(selectData.sectionid);
    this.DataSelect.parentId = taskSelect.task.taskid;
  }
}
