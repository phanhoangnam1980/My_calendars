package com.example.calendars.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "group_channels")
@Getter
@Setter
public class Group {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long groupid;

	@Column(name = "name_group_task", nullable = false)
	private String TitleGroupTask;

	@Column(name = "user_id", nullable = false)
	private Long UserID;

	@Column(name = "color_detail_specs")
	private String ColorSpecs;

	@Column(name = "type_group", nullable = false)
	private Long grouptypeData;
	
	@Column(name = "count", columnDefinition = "integer default 0")
	private int CountComments;
}
