package com.example.calendars.search;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchReceiveModel{
	private String username;
	
	private String prefix;

	private Long userid;
}
