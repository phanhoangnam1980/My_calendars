import {CustomTaskMessage} from './customTaskMessage';

export class TaskData {
  listtask: Array<CustomTaskMessage>;
  title: string;
  sectionid: bigint;
  subtask: Array<CustomTaskMessage>
}
