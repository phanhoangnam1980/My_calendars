package com.example.calendars.domain;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomChartModel {
	private String name;
	
	private List<CountTaskPerDate> series;

	public CustomChartModel(String name, List<CountTaskPerDate> series) {
		super();
		this.name = name;
		this.series = series;
	}
}
