import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenResponse} from '../model/TokenResponse';
import {environment} from '../../environments/environment';
import {NewModelTask} from '../model/domain/newModelTask';
import {User} from '../model/User';
import {Images} from '../model/Images';
import {DetailUser} from '../model/DetailUser';
import {DeviceModel} from '../model/DeviceModel';

@Injectable({providedIn: 'root'})
export class UserService {
  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));

  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + this.token.accessToken
  });

  constructor(private http: HttpClient) {
  }

  UpdateTimeZone(timezone: string) {
    return this.http.put(environment.serverPort + '/user/timezone', {zoneId: timezone}, {headers: this.headers});
  }

  getAvatarUsername() {
    return this.http.get<User>(environment.serverPort + '/user', {headers: this.headers});
  }

  getDetailUser() {
    return this.http.get<User>(environment.serverPort + '/user/detail', {headers: this.headers});
  }

  updateUsername(Currentusername: string) {
    return this.http.patch(environment.serverPort + '/user/username', {username: Currentusername}, {headers: this.headers});
  }

  updatePassword(Password: string, Confirm: string, oldPassword: string) {
    return this.http.patch(environment.serverPort + '/user/password', {
      password: Password,
      passwordConfirm: Confirm,
      username: oldPassword
    }, {headers: this.headers});
  }

  updateEmail(user: User) {
    return this.http.patch(environment.serverPort + '/user/email', user, {headers: this.headers});
  }

  UpdateAvatar(image: Images) {
    this.http.patch(environment.serverPort + '/user/image', image, {headers: this.headers}).subscribe();
  }

  GetDetailOptionUser() {
    return this.http.get<DetailUser>(environment.serverPort + '/user/detailChart', {headers: this.headers});
  }

  UpdateBackgroundImage(file: File, fileName: string) {
    const uploadAvatarData = new FormData();
    uploadAvatarData.append('backgroundFile', file, fileName);
    return this.http.post(environment.serverPort + '/user/background', uploadAvatarData);
  }

  UpdateChartUse(update: DetailUser) {
    return this.http.patch(environment.serverPort + '/user/updateChart/' +
      update.firstChart.toString() + '/' + update.secondChart.toString(),
      null, {headers: this.headers});
  }

  UpdateToken(device: DeviceModel) {
    this.http.post(environment.serverPort + '/user/tokenDevice', device, {headers: this.headers}).subscribe();
  }
}
