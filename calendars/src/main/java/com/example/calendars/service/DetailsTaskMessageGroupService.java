package com.example.calendars.service;

import java.sql.SQLException;
import java.util.List;

import com.example.calendars.domain.GroupSectionModel;
import com.example.calendars.domain.TaskGroupModel;
import com.example.calendars.domain.model.DetailTaskGroup;

public interface DetailsTaskMessageGroupService {
	void removeConnectTaskGroup(Long idTask, Long idGroup);

	void UpdateGroupOfTask(Long idTask, Long currentidGroup, Long IdGroupMoveIn);

	void addTaskInGroup(Long idTask, Long idGroup);

	void saveSectionGroup(Long idGroup, Long SectionId);

	void saveTaskInSection(Long idTask, Long idGroup, Long DectionId);

	List<TaskGroupModel> findDataUserToday(Long userid, String timezone);

	List<DetailTaskGroup> ListTaskInGroupBySection(Long groupid);

	void DeleteTaskSection(Long sectionId);

	List<GroupSectionModel> listGroupSectionUser(Long userid);

	void updateTaskFinished(Long taskid);

	void updateKindOfTask(Long kinds, Long taskid) throws SQLException;

	void DeleteDetailByTask(Long taskid);

	List<TaskGroupModel> getPinnedTask(Long userid, String timezone, int pagenumber);
	
	List<TaskGroupModel> getDataUpcoming(String timezone);
}
