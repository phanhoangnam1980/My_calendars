import {TaskMessage} from '../TaskMessage';

export class CustomTaskMessage {
  task: TaskMessage;
  overdue: boolean;
}
