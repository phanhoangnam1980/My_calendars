package com.example.calendars.domain;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import com.example.calendars.domain.model.Comment;
import com.example.calendars.domain.model.Images;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentResponse {
	private List<Comment> comments;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "E MMM dd yyyy HH:mm")
	private ZonedDateTime timePost;
	
	private List<Images> images;
	
	public void createListComment(Comment data) {
		this.comments = new ArrayList<Comment>();
		this.comments.add(data);
	}
	
	public void addComment(Comment data) {
		this.comments.add(data);
	}
	
	public void createListImages(Images data) {
		this.images = new ArrayList<Images>();
		this.images.add(data);
	}
}
