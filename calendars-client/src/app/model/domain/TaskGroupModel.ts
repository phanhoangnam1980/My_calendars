export class TaskGroupModel {
  id: bigint;
  taskid: bigint;
  groupid: bigint;
  colorSpecs: string;
  titerGroupTask: string;
  grouptypeData: string;
  title: string;
  description: string;
  date: string;
  usergroup: bigint;
  usertask: bigint;
  sectionId: bigint;
  titleSection: string;
}
