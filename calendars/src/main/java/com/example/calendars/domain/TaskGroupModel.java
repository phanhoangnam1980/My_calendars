package com.example.calendars.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class TaskGroupModel {
	private Long id;

	private Long taskid;

	private Long groupid;

	@Column(name = "color_detail_specs")
	private String ColorSpecs;

	@Column(name = "name_group_task", nullable = false)
	private String TiterGroupTask;

	@Column(name = "type_group", nullable = false)
	private Long grouptypeData;

	@Column(name = "tittle_task")
	private String Title;

	@Column(name = "task_description")
	private String Description;

	@Column(name = "date_created")
	private String date;

	@Column(name = "user_create_groups", nullable = false)
	private Long usergroup;

	@Column(name = "user_create_task", nullable = false)
	private Long usertask;
	
	private Long sectionId;
	
	private String titleSection;

	public TaskGroupModel(Long id, Long taskid, Long groupid, String colorSpecs, String titterGroupTask,
			Long grouptypeData, String tittle, String description, String date, Long usercreategroup,
			Long usercreatetask,Long sectionId) {
		super();
		this.id = id;
		this.taskid = taskid;
		this.groupid = groupid;
		this.ColorSpecs = colorSpecs;
		this.TiterGroupTask = titterGroupTask;
		this.grouptypeData = grouptypeData;
		this.Title = tittle;
		this.Description = description;
		this.date = date;
		this.usergroup = usercreategroup;
		this.usertask = usercreatetask;
		this.sectionId = sectionId;
	}

	public TaskGroupModel() {
		super();
	}

	public TaskGroupModel(Long id, Long taskid, Long groupid, String colorSpecs, String titerGroupTask,
			Long grouptypeData, String title, String description, String date, Long usergroup, Long usertask,
			Long sectionId, String titleSection) {
		super();
		this.id = id;
		this.taskid = taskid;
		this.groupid = groupid;
		ColorSpecs = colorSpecs;
		TiterGroupTask = titerGroupTask;
		this.grouptypeData = grouptypeData;
		Title = title;
		Description = description;
		this.date = date;
		this.usergroup = usergroup;
		this.usertask = usertask;
		this.sectionId = sectionId;
		this.titleSection = titleSection;
	}
}
