package com.example.calendars.search;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.calendars.domain.CommentResponse;
import com.example.calendars.domain.model.Comment;
import com.example.calendars.repository.CommentRepository;
import com.example.calendars.repository.DetailTaskMessageRepository;

@Service
public class SearchServiceImpl implements SearchService {
	@Autowired
	private DetailTaskMessageRepository detailRepository;
	@Autowired
	private CommentRepository commentRepository;

	@Override
	public HashSet<SearchResultModel> getGroupWithTitle(String word, int limit, Long userid) {
		// TODO Auto-generated method stub
		HashSet<SearchResultModel> result = new HashSet<SearchResultModel>();
		List<Object[]> rawData = detailRepository.searchSectionAndGroup(word, userid, PageRequest.of(0, limit));
		for (Object[] data : rawData) {
			if (data[5].toString().isEmpty()) {
				result.add(AddGroupFromObject(data));
			} else {
				result.add(AddSectionFromObject(data));
			}
		}
		limit -= result.size();
		List<Object[]> rawTaskData = detailRepository.searchTask(word, PageRequest.of(0, limit));
		for (Object[] taskData : rawTaskData) {
			result.add(AddTaskToSearchModel(taskData));
		}
		return result;
	}

	@Override
	public SearchResultModel AddGroupFromObject(Object[] data) {
		// TODO Auto-generated method stub
		SearchResultModel result = new SearchResultModel();
		result.setUrl("project/" + data[0].toString());
		result.setColorSpecs(data[1].toString());
		result.setTitle(data[2].toString());
		return result;
	}

	@Override
	public SearchResultModel AddSectionFromObject(Object[] data) {
		// TODO Auto-generated method stub
		SearchResultModel result = new SearchResultModel();
		result.setUrl("project/" + data[0].toString());
		result.setTitle(data[5].toString());
		return result;
	}

	@Override
	public SearchResultModel AddTaskToSearchModel(Object[] data) {
		// TODO Auto-generated method stub
		SearchResultModel result = new SearchResultModel();
		result.setUrl("project/" + data[0].toString());
		if (data[4] != null) {
			result.setColorSpecs(data[1].toString());
			if (!data[4].toString().isEmpty()) {
				result.setTitleChannel(data[2].toString() + "/" + data[4].toString());
			} else {
				result.setTitleChannel(data[2].toString());
			}
		} else {
			result.setTitleChannel(data[2].toString());
		}
		result.setTitle(data[6].toString());
		if (data[7] != null) {
			result.setDescription(data[7].toString());
		}
		result.setTaskid(Long.parseLong(data[5].toString()));
		return result;
	}

	@Override
	public List<CommentResponse> getListCommentByWord(String prefix, int pagenumber) {
		// TODO Auto-generated method stub
		ZonedDateTime previous = null;
		List<CommentResponse> result = new ArrayList<CommentResponse>();
		List<Comment> rawdata = commentRepository.searchCommentMatchWord(prefix, PageRequest.of(pagenumber, 10));
		int sizeRawData = rawdata.size();
		for (int i = 0; i < sizeRawData; i++) {
			if (rawdata.get(i).getCreate() != previous) {
				previous = rawdata.get(i).getCreate();
				CommentResponse newResponse = new CommentResponse();
				newResponse.setTimePost(previous);
				newResponse.createListComment(rawdata.get(i));
				result.add(newResponse);
			} else {
				result.get(result.size() - 1).addComment(rawdata.get(i));
			}
		}
		return result;
	}

	@Override
	public HashSet<SearchResultModel> getTaskByWord(String word, int limit, Long userid) {
		// TODO Auto-generated method stub
		HashSet<SearchResultModel> result = new HashSet<SearchResultModel>();
		List<Object[]> rawTaskData = detailRepository.searchTask(word, PageRequest.of(0, limit));
		for (Object[] taskData : rawTaskData) {
			result.add(AddTaskToSearchModel(taskData));
		}
		return result;
	}
}
