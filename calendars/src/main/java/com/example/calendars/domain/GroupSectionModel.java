package com.example.calendars.domain;

import java.util.ArrayList;

import com.example.calendars.domain.model.Section;

public class GroupSectionModel {
	private Long groupid;
	
	private String titleGroup;

	private String icons;

	private Long types;
	
	private Long deSection;
	
	public Long getDeSection() {
		return deSection;
	}

	public void setDeSection(Long deSection) {
		this.deSection = deSection;
	}

	private ArrayList<Section> childrenSection;

	public Long getGroupid() {
		return groupid;
	}

	public void setGroupid(Long groupid) {
		this.groupid = groupid;
	}

	public String getTitleGroup() {
		return titleGroup;
	}

	public void setTitleGroup(String titleGroup) {
		this.titleGroup = titleGroup;
	}

	public String getIcons() {
		return icons;
	}

	public void setIcons(String icons) {
		this.icons = icons;
	}

	public Long getTypes() {
		return types;
	}

	public void setTypes(Long types) {
		this.types = types;
	}

	public ArrayList<Section> getChildrenSection() {
		return childrenSection;
	}

	public void setChildrenSection(ArrayList<Section> childrenSection) {
		this.childrenSection = childrenSection;
	}
	
	public void AddChildSection(Section child) {
		this.childrenSection.add(child);
	}
}
