package com.example.calendars.suggest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.example.calendars.domain.model.DeviceUser;
import com.example.calendars.service.UserService;

@Service
public class SuggestServiceImpl implements SuggestService {
	@Autowired
	private Environment env;
	@Autowired
	private UserService userService;

	@Override
	public void SetupDatabaseSearch(List<String> suggestList, String language) {
		// TODO Auto-generated method stub
		PruningRadixTrieService serviceData = new PruningRadixTrieService();
		for (String suggestWord : suggestList) {
			serviceData.addTerm(suggestWord, 1000);
		}
		serviceData.writeTermsToFile(env.getProperty("url.storedata") + language + ".txt");
	}

	@Override
	public String wordSuggest(String prefix, String url) {
		// TODO Auto-generated method stub
		String suggestWord = null;
		int topK = 1;
		PruningRadixTrieService serviceData = new PruningRadixTrieService();
		serviceData.readTermsFromFile(url, "\t");
		List<PruningRadixTermAndFrequency> results = serviceData.getTopkTermsForPrefix(prefix, topK);
		for (PruningRadixTermAndFrequency result : results) {
			suggestWord = result.getTerm();
		}
		return suggestWord;
	}

	@Override
	public SuggestResultModel CaseLanguageSuggest(SuggestReceiveModel model) {
		// TODO Auto-generated method stub
		SuggestResultModel result = new SuggestResultModel();
		String[] currentDeviceId = model.getIdDevice().split("/");
		DeviceUser current = userService.GetDeviceInfo(currentDeviceId[1]);
		String selectPath = "";
		switch (current.getLanguage()) {
		case "en":
			selectPath = env.getProperty("url.storedata") + "en.txt";
			break;
		case "vi":
			selectPath = env.getProperty("url.storedata") + "vi.txt";
			break;
		}
		String resultWord = wordSuggest(model.getPrefix(), selectPath);
		if (resultWord != null) {
			String[] rawDataBack = resultWord.split("::");
			result.setResult(rawDataBack[0]);
			result.setSelect(rawDataBack[1]);
		}
		return result;
	}
}
