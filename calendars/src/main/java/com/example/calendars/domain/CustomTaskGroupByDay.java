package com.example.calendars.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomTaskGroupByDay {
	private int day;

	private List<CustomImageModel> listTask;

	public CustomTaskGroupByDay(int day) {
		super();
		this.day = day;
		this.listTask = new ArrayList<>();
	}

	public void addTaskToList(CustomImageModel model) {
		this.listTask.add(model);
	}
}
