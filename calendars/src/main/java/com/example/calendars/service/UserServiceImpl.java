package com.example.calendars.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.calendars.domain.model.DetailUser;
import com.example.calendars.domain.model.DeviceUser;
import com.example.calendars.domain.model.Group;
import com.example.calendars.domain.model.Images;
import com.example.calendars.domain.model.Section;
import com.example.calendars.domain.model.User;
import com.example.calendars.firebase.IImageService;
import com.example.calendars.repository.DeviceRepository;
import com.example.calendars.repository.UserRepository;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private DeviceRepository deviceRespository;
	@Autowired
	private ChannelService channelService;
	@Autowired
	private IImageService imageService;
	@Autowired
	private DetailUserService detailUserService;
	@Autowired
	private SectionService sectionService;
	@Autowired
	private DetailsTaskMessageGroupService detailService;

	@Value("${cloud.clientID}")
	private String clientID;
	
	@Value("${firebase.baseFirst-image-url}")
	private String imageurl;

	@Value("${firebase.baselast-image-url}")
	private String lasturl;

	@Override
	public void save(User user) {
		// TODO Auto-generated method stub
		if (user.getPassword() != null) {
			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		}
		// user.setRoles(new HashSet<>(roleRepository.findAll()));
		userRepository.save(user);
	}

	@Override
	public User findByEmail(String email) {
		// TODO Auto-generated method stub
		return userRepository.findByEmail(email);
	}

	@Override
	public void updateNewTimezone(User currentuser, String zonetime) {
		// TODO Auto-generated method stub
		userRepository.updateTimeZone(zonetime, currentuser.getId());
	}

	@Override
	public User AvatarOnly(String user) {
		// TODO Auto-generated method stub
		User result = new User();
		User data = findByEmail(user);
		result.setUsername(data.getUsername());
		result.setImage(imageurl + data.getImage() + lasturl);
		result.setZonetime(data.getZonetime());
		return result;
	}

	@Override
	public void updateEmail(User user) {
		// TODO Auto-generated method stub
		userRepository.updateEmail(user.getEmail(), user.getId());
	}

	@Override
	public void updatePassword(User user) {
		// TODO Auto-generated method stub
		userRepository.updatePassword(user.getPassword(), user.getEmail());
	}

	@Override
	public void updateUsername(User user) {
		// TODO Auto-generated method stub
		userRepository.updateUsername(user.getUsername(), user.getEmail());
	}

	@Override
	public void updateAvatar(User user, String avatar) {
		// TODO Auto-generated method stub
		userRepository.updateAvatar(avatar, user.getId());
	}

	@Override
	public void saveDevice(DeviceUser device) {
		// TODO Auto-generated method stub
		device.setId(String.valueOf(Math.abs(UUID.randomUUID().getLeastSignificantBits())));
		ClientDeviceUsing(device);
		deviceRespository.save(device);
	}

	@Override
	public HashSet<DeviceUser> listDeviceUser(Long userid) {
		// TODO Auto-generated method stub
		HashSet<DeviceUser> result = new HashSet<DeviceUser>();
		List<DeviceUser> rawData = deviceRespository.GetListDevice(userid);
		for (DeviceUser i : rawData) {
			result.add(i);
		}
		return result;
	}

	@Override
	public DeviceUser getIdDevice(String ip, String agent, Long userid) {
		// TODO Auto-generated method stub
		DeviceUser result = new DeviceUser();
		DeviceUser rawData = deviceRespository.findByIp(ip, userid, agent);
		result.setId(rawData.getId());
		result.setLanguage(rawData.getLanguage());
		return result;
	}

	@Override
	public void updateLanguage(String language, String id) {
		// TODO Auto-generated method stub
		deviceRespository.updateLanguageDevice(id, language);
	}

	@Override
	public void ClientDeviceUsing(DeviceUser current) {
		// TODO Auto-generated method stub
		String agent = current.getAgent();
		if (agent.contains("Firefox/") || agent.contains("Chrome/") || agent.contains("Safari/")
				|| agent.contains("OPR/") || agent.contains("Opera/") || agent.contains("Trident/")) {
			current.setTypeDevice("Browser");
		} else if (agent.contains("Android")) {
			current.setTypeDevice("android");
		} else if (agent.contains("iPad") || agent.contains("iPhone") || agent.contains("iPod")) {
			current.setTypeDevice("ios");
		}
	}

	@Override
	public DeviceUser GetDeviceInfo(String id) {
		// TODO Auto-generated method stub
		return deviceRespository.GetDataDevice(id);
	}

	@Override
	public void updateDeviceToken(DeviceUser device) {
		// TODO Auto-generated method stub
		deviceRespository.updateToken(device.getId(), device.getToken());
	}

	@Override
	public Object validLoginWithGoogle(String token) throws GeneralSecurityException, IOException {
		// TODO Auto-generated method stub
		GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new GsonFactory())
				.setAudience(Collections.singletonList(clientID)).build();
		GoogleIdToken idToken = verifier.verify(token);
		if (idToken == null) {
			return "token error";
		}
		Payload payload = idToken.getPayload();
		User checkUser = userRepository.findByEmail(payload.getEmail());
		if (checkUser == null) {
			User newForm = new User();
			newForm.setEmail(payload.getEmail());
			newForm.setImage((String) payload.get("picture"));
			newForm.setPassword("MickeyMinniePlutoHueyLouieDeweyDonaldGoofySacramento");
			newForm.setUsername((String) payload.get("name"));
			newForm.setTypeLogin(1);
			newForm.setRoles(new HashSet<>());
			save(newForm);
			SetUpNewUser(newForm.getId(), (String) payload.get("picture"));
		}
		return payload.getEmail();
	}

	@Override
	public void SetUpNewUser(Long userid, String urlAvatar) {
		// TODO Auto-generated method stub
		Group DataGiveBack = channelService.makeDefaultGroup(userid);
		Section databack = sectionService.saveDefault();
		detailService.saveSectionGroup(DataGiveBack.getGroupid(), databack.getId());
		Long first = (long) 0, second = (long) 0;
		// add 2 default custom Channel
		Group groupcreatefirst = new Group();
		groupcreatefirst.setColorSpecs("#708090");
		groupcreatefirst.setUserID(userid);
		groupcreatefirst.setTitleGroupTask("Gia đình");
		groupcreatefirst.setGrouptypeData((long) 1);
		Group channelFirst = channelService.addNewGroupChannel(groupcreatefirst);
		Section dataFirst = sectionService.saveDefault();
		detailService.saveSectionGroup(channelFirst.getGroupid(), dataFirst.getId());
		Group groupcreatesecond = new Group();
		groupcreatesecond.setColorSpecs("#C0C0C0");
		groupcreatesecond.setUserID(userid);
		groupcreatesecond.setTitleGroupTask("Công việc");
		groupcreatesecond.setGrouptypeData((long) 1);
		Group ChannelSecond = channelService.addNewGroupChannel(groupcreatesecond);
		Section dataSecond = sectionService.saveDefault();
		detailService.saveSectionGroup(ChannelSecond.getGroupid(), dataSecond.getId());
		first = groupcreatefirst.getGroupid();
		second = groupcreatesecond.getGroupid();
		DetailUser detail = new DetailUser();
		if (!urlAvatar.equals("")) {
			Images avatar = new Images();
			avatar.setThumbshot(urlAvatar);
			avatar.setViewImage(urlAvatar);
			avatar.setFull(urlAvatar);
			imageService.save(avatar);
			detail.setIdImage(avatar.getId());
		}
		detail.setFirstChart(first);
		detail.setSecondChart(second);
		detail.setUserid(userid);
		detailUserService.save(detail);
	}
}
