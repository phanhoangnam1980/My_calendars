package com.example.calendars.search;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class SearchResultModel {
	
	private String url;
	
	private String title;
	
	private String Description;
	
	private String colorSpecs;
	
	private String titleChannel;
	
	private Long taskid;
}
