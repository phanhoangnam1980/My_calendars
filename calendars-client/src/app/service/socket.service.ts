import {Injectable, OnDestroy} from '@angular/core';
import * as SockJS from 'sockjs-client';
import {environment} from '../../environments/environment';
import {Stomp} from '@stomp/stompjs';
import {BehaviorSubject} from 'rxjs';
import {SearchModel} from '../model/domain/SearchModel';
import {SuggestModel} from '../model/domain/SuggestModel';


@Injectable()
export class SocketService implements OnDestroy {
  private socket;
  private stompClient;
  private userConnect: string;

  // send Data Search
  private DataSearch: BehaviorSubject<Array<SearchModel>> = new BehaviorSubject<Array<SearchModel>>([]);
  currentDataSearch = this.DataSearch.asObservable();

  // send Data Suggest Word
  private DataSuggest: BehaviorSubject<SuggestModel> = new BehaviorSubject<SuggestModel>(null);
  currentDataSuggest = this.DataSuggest.asObservable();

  constructor() {
  }

  ngOnDestroy(): void {
    this.DataSearch.unsubscribe();
    this.DataSuggest.unsubscribe();
  }

  setup(nameUser: string, id: string) {
    this.userConnect = nameUser.concat('/' + id);
    this.socket = new SockJS(environment.socketPort + '/nodeApp');
    this.stompClient = Stomp.over(this.socket);
    // turn off console debug
    this.stompClient.debug = () => {
    };
    this.stompClient.connect({username: this.userConnect}, () => {
      this.stompClient.subscribe('/user/queue/search', (result) => {
        this.AddDataSearch(JSON.parse(result.body));
      });
      this.stompClient.subscribe('/user/queue/reply', result => {
        console.log('in' + result.body);
      });
      this.stompClient.subscribe('/user/queue/suggest', result => {
        this.AddDataSuggest(JSON.parse(result.body));
      });
    });
  }

  listenSearch() {
    this.stompClient.subscribe('/users/queue/search', (message) => {
      console.log(message);
    });
  }

  SendSearchPrefix(prefixData: string) {
    this.stompClient.send('/app/search/receive', {}, JSON.stringify({
      username: this.userConnect,
      prefix: prefixData
    }));
    this.stompClient.send('/app/message', {}, 'name');
  }

  SendSuggestPrefix(prefixData: string) {
    this.stompClient.send('/app/suggest/receive',{}, JSON.stringify({
      idDevice: this.userConnect,
      prefix: prefixData
    }))
  }

  disconnectSearch() {
    this.stompClient.disconnect();
  }

  connectSuggest() {
    this.stompClient.subscribe('/users/queue/suggest', (message) => {
      console.log(message);
    });
  }

  AddDataSearch(data: Array<SearchModel>) {
    this.DataSearch.next(data);
  }

  AddDataSuggest(data: SuggestModel) {
    this.DataSuggest.next(data);
  }
}

