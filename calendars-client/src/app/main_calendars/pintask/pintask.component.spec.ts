import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PintaskComponent } from './pintask.component';

describe('PintaskComponent', () => {
  let component: PintaskComponent;
  let fixture: ComponentFixture<PintaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PintaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PintaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
