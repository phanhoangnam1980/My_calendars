package com.example.calendars.suggest;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PruningRadixNode {
	private List<PruningRadixNodeModel> children;
	
	private long termFrequencyCount;
	
	private long termFrequencyCountChildMax;

	public PruningRadixNode(long termFrequencyCount) {
		super();
		this.termFrequencyCount = termFrequencyCount;
	}
}
