package com.example.calendars.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskGroup {
	private TaskGroupModel data;
	
	private boolean overdue;

	public TaskGroup(TaskGroupModel data, boolean overdue) {
		super();
		this.data = data;
		this.overdue = overdue;
	}
}
