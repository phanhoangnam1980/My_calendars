import {AfterViewInit, Component, OnInit} from '@angular/core';
import {User} from '../../model/User';
import {UserService} from '../../service/user.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {ChangeUsernameComponent} from './change-username/change-username.component';
import {ShareDataService} from '../../service/ShareData.service';
import {Images} from '../../model/Images';
import {ImageService} from '../../service/image.service';
import {Response} from '../../model/domain/Response';
import {environment} from '../../../environments/environment';
import {GroupSectionModel} from '../../model/domain/GroupSectionModel';
import {ChannelService} from '../../service/Channel.service';
import {DetailUser} from '../../model/DetailUser';
import {TranslocoService} from '@ngneat/transloco';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.css']
})
export class UsersDetailComponent implements OnInit, AfterViewInit {
  private DetailUser: User;
  public username: string;
  public email: string;
  public backgroundRandom: string;
  public colorScheme = {
    domain: ['#FF8A80',
      '#EA80FC',
      '#8C9EFF',
      '#80D8FF',
      '#A7FFEB',
      '#CCFF90',
      '#FFFF8D',
      '#FF9E80']
  };
  // avatar value
  public flagChange: boolean;
  private AvatarImage: Images;
  public preViewImage: string;
  // avatar animation
  public flagHint: boolean;
  // group Select
  public listgroup: Array<GroupSectionModel>;
  public selectFirstValue = '';
  public selectSecondValue = '';
  public flagChangeChart;
  // store detailUser
  public detailOptionUser: DetailUser;
  // error image
  public ErrorMessage: string;
  public flagError: boolean;

  constructor(private userService: UserService, private dialogRef: MatDialogRef<UsersDetailComponent>,
              public dialog: MatDialog, private shareService: ShareDataService, private imageService: ImageService,
              private channelService: ChannelService, private translocoService: TranslocoService) {
    this.DetailUser = new User();
    this.preViewImage = this.DetailUser.image;
    this.flagChange = false;
    this.detailOptionUser = new DetailUser();
    this.flagChangeChart = false;
    this.flagError = false;
  }

  ngAfterViewInit(): void {
    this.shareService.AddNewUsername.subscribe(flag => {
      if (flag === true) {
        this.refreshData();
      }
    });
  }

  ngOnInit(): void {
    this.userService.getDetailUser().subscribe(info => {
      this.DetailUser = info;
      this.username = info.username;
      this.email = info.email;
      this.preViewImage = this.DetailUser.image;
    });
    const random = Math.floor(Math.random() * 8);
    this.backgroundRandom = this.colorScheme.domain[random];
    this.channelService.GetListChannelSection().subscribe((data: Array<GroupSectionModel>) => {
      this.listgroup = data;
      this.listgroup.splice(0, 1);
    });
    this.userService.GetDetailOptionUser().subscribe(result => {
      this.detailOptionUser = result;
      this.selectFirstValue = this.detailOptionUser.firstChart.toString();
      this.selectSecondValue = this.detailOptionUser.secondChart.toString();
    });
    this.AvatarImage = new Images();
  }

  closingDialog() {
    this.dialogRef.close();
  }

  openEditsPassword() {
    this.dialog.open(ChangePasswordComponent, {
      width: '650px'
    });
  }

  openEditsUsername() {
    this.dialog.open(ChangeUsernameComponent, {
      width: '650px',
      data: {
        username: this.DetailUser.username
      }
    });
  }

  refreshData() {
    this.userService.getDetailUser().subscribe(info => {
      this.DetailUser = info;
      this.username = info.username;
      this.email = info.email;
    });
  }

  changeAvatar(event) {
    const value = event.target.files[0].name;
    // const contexttype = event.target.files[0].type;
    const fileName = typeof value === 'string' ? value.match(/[^\/\\]+$/)[0] : value[0];
    if (event.target.files[0].size < 6291456) {
      this.imageService.SendAvatar(event.target.files[0], fileName).subscribe((result: Response) => {
        switch (result.code) {
          case 200:
            this.SetAvatar(result.data);
            this.flagChange = true;
            this.flagError = false;
            break;
          case 406:
            this.flagError = true;
            this.translocoService.selectTranslate('UnsupportType').subscribe(translate => {
              this.ErrorMessage = translate;
            });
            break;
        }
      });
    }
  }

  ChangeBackground(event) {
    const value = event.target.files[0].name;
    // const contexttype = event.target.files[0].type;
    const fileName = typeof value === 'string' ? value.match(/[^\/\\]+$/)[0] : value[0];
    if (event.target.files[0].size < 6291456) {
      this.userService.UpdateBackgroundImage(event.target.files[0], fileName);
    }
  }

  ChangeOption() {
    this.flagChangeChart = true;
    this.flagChange = true;
  }

  SetAvatar(data) {
    this.AvatarImage = data;
    this.preViewImage = environment.urlViewImage + this.AvatarImage.viewImage + '?alt=media';
  }

  movingInAvatar() {
    this.flagHint = true;
  }

  movingOutAvatar() {
    this.flagHint = false;
  }

  SaveChangeAvatar() {
    console.log(Object.keys(this.AvatarImage).length);
    if (Object.keys(this.AvatarImage).length !== 0) {
      this.userService.UpdateAvatar(this.AvatarImage);
    }
    this.UpdateChangeChart();
  }

  UpdateChangeChart() {
    if (this.flagChangeChart === true) {
      this.detailOptionUser.firstChart = BigInt(this.selectFirstValue);
      this.detailOptionUser.secondChart = BigInt(this.selectSecondValue);
      this.userService.UpdateChartUse(this.detailOptionUser).subscribe(() => {
        this.flagChangeChart = false;
        this.flagChange = false;
      });
    }
  }
}
