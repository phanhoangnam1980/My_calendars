package com.example.calendars.firebase.notification;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppNotification {
	private String title;
	
	private String message;
}
