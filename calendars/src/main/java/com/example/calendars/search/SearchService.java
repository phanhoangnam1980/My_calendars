package com.example.calendars.search;

import java.util.HashSet;
import java.util.List;

import com.example.calendars.domain.CommentResponse;

public interface SearchService {
	HashSet<SearchResultModel> getGroupWithTitle(String word, int limit, Long userid);
	
	SearchResultModel AddGroupFromObject(Object[] data);
	
	SearchResultModel AddSectionFromObject(Object[] data);
	
	SearchResultModel AddTaskToSearchModel(Object[] data);
	
	List<CommentResponse> getListCommentByWord(String prefix, int pagenumber);
	
	HashSet<SearchResultModel> getTaskByWord(String word, int limit, Long userid);
}
