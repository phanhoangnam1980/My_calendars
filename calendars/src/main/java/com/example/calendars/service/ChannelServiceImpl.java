package com.example.calendars.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.calendars.domain.model.Group;
import com.example.calendars.repository.DetailTaskMessageRepository;
import com.example.calendars.repository.GroupRepository;

@Service
public class ChannelServiceImpl implements ChannelService {
	@Autowired
	private GroupRepository groupRepository;
	@Autowired
	private DetailTaskMessageRepository detailTaskMessageRepository;

	@Override
	public Group addNewGroupChannel(Group DataForNewChannel) {
		// TODO Auto-generated method stub
		groupRepository.save(DataForNewChannel);
		return DataForNewChannel;
	}

	@Override
	public List<Group> GetAllCustomChannel(Long userid) {
		// TODO Auto-generated method stub
		List<Group> result = groupRepository.findAllcustomProject(userid);
		return result;
	}

	@Override
	public Group GetDefaultChannel(Long userid) {
		// TODO Auto-generated method stub
		Group result = groupRepository.findDefaultProject(userid);
		return result;
	}

	@Override
	public Group UpdateTittleChannel(Group DataForUpdate, Long userid, String newGroupName) {
		// TODO Auto-generated method stub
		groupRepository.updateTittleProject(newGroupName, userid, DataForUpdate.getGroupid());
		Group resultAfterChange = groupRepository.findByGroupID(DataForUpdate.getGroupid());
		return resultAfterChange;
	}

	@Override
	public void DeleteChannel(Long ChannelDeleteID) {
		// TODO Auto-generated method stub
		detailTaskMessageRepository.DeleteTaskFromDeletedProject(ChannelDeleteID);
		groupRepository.deleteById(ChannelDeleteID);
	}

	@Override
	public void save(Group SaveChannel) {
		// TODO Auto-generated method stub
		groupRepository.save(SaveChannel);
	}

	@Override
	public Group findByGroupId(Long Groupid) {
		// TODO Auto-generated method stub
		return groupRepository.findByGroupID(Groupid);
	}

	@Override
	public Group makeDefaultGroup(Long userid) {
		// TODO Auto-generated method stub
		Group GroupDefault = new Group();
		GroupDefault.setColorSpecs("#5297ff");
		GroupDefault.setUserID(userid);
		GroupDefault.setTitleGroupTask("Default Group");
		GroupDefault.setGrouptypeData((long) 0);
		return addNewGroupChannel(GroupDefault);
	}

	@Override
	public void UpdateChannel(Group newDataChannel) {
		// TODO Auto-generated method stub
		groupRepository.updateProject(newDataChannel.getTitleGroupTask(), newDataChannel.getColorSpecs(),
				newDataChannel.getUserID(), newDataChannel.getGroupid());
	}

	@Override
	public void UpdateCommentGroupChannel(int number, Long groupid) {
		// TODO Auto-generated method stub
		Group defaultCount = groupRepository.findByGroupID(groupid);
		groupRepository.UpdateCountComments(defaultCount.getCountComments() + number, groupid);
	}

	@Override
	public void createTwoCustomChannel(Long userid, Long firstid, Long secondid) {
		// TODO Auto-generated method stub
		Group groupcreatefirst = new Group();
		groupcreatefirst.setColorSpecs("#708090");
		groupcreatefirst.setUserID(userid);
		groupcreatefirst.setTitleGroupTask("Gia đình");
		groupcreatefirst.setGrouptypeData((long) 1);
		groupRepository.save(groupcreatefirst);
		Group groupcreatesecond = new Group();
		groupcreatesecond.setColorSpecs("#C0C0C0");
		groupcreatesecond.setUserID(userid);
		groupcreatesecond.setTitleGroupTask("Công việc");
		groupcreatesecond.setGrouptypeData((long) 1);
		groupRepository.save(groupcreatesecond);
		firstid = groupcreatefirst.getGroupid();
		secondid = groupcreatefirst.getGroupid();
	}
}
