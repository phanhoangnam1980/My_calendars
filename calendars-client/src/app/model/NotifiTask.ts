export class NotifiTask {
  id: bigint;
  taskid: bigint;
  type: number;
  duration: number;
}
