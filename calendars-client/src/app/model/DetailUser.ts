export class DetailUser {
  id: bigint;
  userid: bigint;
  firstChart: bigint;
  secondChart: bigint;
  backgroundImage: string;
}
