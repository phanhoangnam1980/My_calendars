import {Component, OnInit} from '@angular/core';
import {ShareDataService} from '../service/ShareData.service';
import {Section} from '../model/Section';
import {SectionService} from '../service/Section.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-addsection',
  templateUrl: './addsection.component.html',
  styleUrls: ['./addsection.component.css']
})
export class AddsectionComponent implements OnInit {
  public remainingtitleWords = 500;
  private groupId: bigint;

  constructor(private sharedata: ShareDataService, private sectionService: SectionService, private route: ActivatedRoute) {
  }

  public textareatitle = '';

  ngOnInit(): void {
  }

  ConfirmStateAddSection() {
    const addNewSection = new Section();
    if (this.textareatitle !== '') {
      addNewSection.title = this.textareatitle;
      this.route.params.subscribe((params) => {
        console.log(+params.id);
        this.sectionService.saveNewSection(addNewSection, +params.id).subscribe((data: Section) => {
          this.sharedata.SetAddSection(data);
        });
      });
      this.sharedata.setCancelRequestNewSection(true);
    }
  }

  CancelStateAddSection() {
    this.sharedata.setCancelRequestNewSection(true);
  }

  autogrowtitle() {
    const textArea = document.getElementById('title');
    textArea.style.overflow = 'hidden';
    textArea.style.height = 'auto';
    textArea.style.height = textArea.scrollHeight + 'px';
  }
}
