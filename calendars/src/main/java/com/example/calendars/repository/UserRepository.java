package com.example.calendars.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.calendars.domain.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByEmail(String email);

	@Transactional
	@Modifying
	@Query(value = "UPDATE User u SET u.zonetime =:timezone Where u.id=:userid")
	void updateTimeZone(@Param("timezone") String timezone, @Param("userid") Long id);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE User u SET u.username =:currentUsername Where u.email=:current")
	void updateUsername(@Param("currentUsername") String username, @Param("current") String email);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE User u SET u.password =:currentPassword Where u.email=:current")
	void updatePassword(@Param("currentPassword") String password, @Param("current") String email);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE User u SET u.email =:currentEmail Where u.id=:userid")
	void updateEmail(@Param("currentEmail") String email, @Param("userid") Long id);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE User u SET u.image =:currentAvatar Where u.id=:userid")
	void updateAvatar(@Param("currentAvatar") String avatar, @Param("userid") Long id);
}
