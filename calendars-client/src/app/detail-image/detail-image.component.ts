import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Images} from '../model/Images';

@Component({
  selector: 'app-detail-image',
  templateUrl: './detail-image.component.html',
  styleUrls: ['./detail-image.component.css']
})
export class DetailImageComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) private rawData: any) {
  }

  public preview: Images;

  ngOnInit(): void {
    this.preview = this.rawData;
  }

  openOrigin() {
    window.open(this.preview.full);
  }
}
