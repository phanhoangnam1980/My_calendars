package com.example.calendars.service;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.calendars.domain.GroupSectionModel;
import com.example.calendars.domain.TaskGroupModel;
import com.example.calendars.domain.model.DetailTaskGroup;
import com.example.calendars.domain.model.Group;
import com.example.calendars.domain.model.Section;
import com.example.calendars.repository.DetailTaskMessageRepository;

@Service
public class DetailsTaskMessageGroupServiceImpl implements DetailsTaskMessageGroupService {
	@Autowired
	private DetailTaskMessageRepository detailTaskMessageRepository;
	@Autowired
	private ChannelService channelService;
	@Autowired
	private SectionService sectionService;

	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E MMM dd yyyy HH:mm:ss xx");

	@Override
	public void removeConnectTaskGroup(Long idTask, Long idGroup) {
		// TODO Auto-generated method stub
		detailTaskMessageRepository.DeleteTaskInProject(idTask, idGroup);
	}

	@Override
	public void UpdateGroupOfTask(Long idTask, Long currentidGroup, Long IdGroupMoveIn) {
		// TODO Auto-generated method stub
		detailTaskMessageRepository.UpdateNewProjectTask(idTask, currentidGroup, IdGroupMoveIn);
	}

	@Override
	public void addTaskInGroup(Long idTask, Long idGroup) {
		// TODO Auto-generated method stub
		DetailTaskGroup newtask = new DetailTaskGroup();
		newtask.setGroups(idGroup);
		newtask.setTask(idTask);
		detailTaskMessageRepository.save(newtask);
	}

	@Override
	public void saveSectionGroup(Long idGroup, Long SectionId) {
		// TODO Auto-generated method stub
		DetailTaskGroup newSectionGroup = new DetailTaskGroup();
		newSectionGroup.setGroups(idGroup);
		newSectionGroup.setSection(SectionId);
		newSectionGroup.setKinds((long) 6);
		detailTaskMessageRepository.save(newSectionGroup);
	}

	@Override
	public void saveTaskInSection(Long idTask, Long idGroup, Long SectionId) {
		// TODO Auto-generated method stub
		DetailTaskGroup newTaskInSection = new DetailTaskGroup();
		newTaskInSection.setGroups(idGroup);
		newTaskInSection.setSection(SectionId);
		newTaskInSection.setTask(idTask);
		newTaskInSection.setKinds((long) 0);
		detailTaskMessageRepository.save(newTaskInSection);
	}

	@Override
	public List<TaskGroupModel> findDataUserToday(Long userid, String timezone) {
		// TODO Auto-generated method stub
		String group;
		String description;
		List<TaskGroupModel> databack = new ArrayList<TaskGroupModel>();
		ZonedDateTime checkcurrent = ZonedDateTime.now().plusDays(1).toLocalDate().atStartOfDay(ZoneOffset.of(timezone)).minusMinutes(1);
		List<Object[]> data = detailTaskMessageRepository.FindByUserId(userid, checkcurrent);
		for (Object[] dataObject : data) {
			Timestamp date = (Timestamp) dataObject[8];
			if (!dataObject[11].toString().isEmpty()) {
				group = dataObject[4].toString() + "/" + dataObject[11].toString();
			} else {
				group = dataObject[4].toString();
			}
			if (dataObject[7] == null) {
				description = "";
			} else {
				description = dataObject[7].toString();
			}
			databack.add(new TaskGroupModel(Long.parseLong(dataObject[0].toString()),
					Long.parseLong(dataObject[1].toString()), Long.parseLong(dataObject[2].toString()),
					dataObject[3].toString(), group, Long.parseLong(dataObject[5].toString()), dataObject[6].toString(),
					description, formatter.format(date.toLocalDateTime().atOffset(ZoneOffset.of(timezone))),
					Long.parseLong(dataObject[9].toString()), Long.parseLong(dataObject[10].toString()),
					Long.parseLong(dataObject[12].toString())));
		}
		return databack;
	}

	@Override
	public List<DetailTaskGroup> ListTaskInGroupBySection(Long groupid) {
		// TODO Auto-generated method stub
		return detailTaskMessageRepository.ListTaskInGroupBySection(groupid);
	}

	@Override
	public void DeleteTaskSection(Long sectionId) {
		// TODO Auto-generated method stub
		detailTaskMessageRepository.DeleteSectionTaskInProject(sectionId);
	}

	@Override
	public List<GroupSectionModel> listGroupSectionUser(Long userid) {
		// TODO Auto-generated method stub
		List<GroupSectionModel> result = new ArrayList<>();
		List<DetailTaskGroup> data = detailTaskMessageRepository.ListSectionAndGroupByUserid(userid);
		Long previous = (long) 0;
		int index = -1;
		for (DetailTaskGroup detail : data) {
			if (detail.getGroups() != previous) {
				GroupSectionModel newSendModel = new GroupSectionModel();
				previous = detail.getGroups();
				index++;
				newSendModel.setGroupid(detail.getGroups());
				newSendModel.setDeSection(detail.getSection());
				Group infoGroup = channelService.findByGroupId(previous);
				newSendModel.setTitleGroup(infoGroup.getTitleGroupTask());
				newSendModel.setIcons(infoGroup.getColorSpecs());
				newSendModel.setTypes(infoGroup.getGrouptypeData());
				newSendModel.setChildrenSection(new ArrayList<Section>());
				if (newSendModel.getTitleGroup().equals("")) {
					result.add(0, newSendModel);
				} else {
					result.add(newSendModel);
				}
				continue;
			}
			result.get(index).AddChildSection(sectionService.GetSectionById(detail.getSection()));
		}
		return result;
	}

	@Override
	public void updateTaskFinished(Long taskid) {
		// TODO Auto-generated method stub
		detailTaskMessageRepository.UpdateTaskFinished(taskid);
	}

	@Override
	public void updateKindOfTask(Long kinds, Long taskid) throws SQLException {
		detailTaskMessageRepository.UpdateKindsofTask(kinds, taskid);
	}

	@Override
	public void DeleteDetailByTask(Long taskid) {
		// TODO Auto-generated method stub
		detailTaskMessageRepository.DeleteTaskByTaskID(taskid);
	}

	@Override
	public List<TaskGroupModel> getPinnedTask(Long userid, String timezone, int pagenumber) {
		// TODO Auto-generated method stub
		List<TaskGroupModel> result = new ArrayList<TaskGroupModel>();
		List<Object[]> rawData = detailTaskMessageRepository.getPinnedMessage(userid, PageRequest.of(pagenumber, 10));
		for (Object[] dataObject : rawData) {
			String detail;
			ZonedDateTime date = (ZonedDateTime) dataObject[5];
			if(dataObject[4] ==null) {
				detail = "";
			}else {
				detail = dataObject[4].toString();
			}
			result.add(new TaskGroupModel(Long.parseLong(dataObject[0].toString()),
					Long.parseLong(dataObject[1].toString()), Long.parseLong(dataObject[2].toString()), "", "",
					(long) 0, dataObject[3].toString(), detail,
					date.withZoneSameInstant(ZoneOffset.of(timezone)).format(formatter), userid, userid, (long) 0));
		}
		return result;
	}

	@Override
	public List<TaskGroupModel> getDataUpcoming(String timezone) {
		// TODO Auto-generated method stub
		String group;
		String description;
		List<TaskGroupModel> databack = new ArrayList<TaskGroupModel>();
		List<Object[]> data = detailTaskMessageRepository.GetUpcomingTaskData(PageRequest.of(0, 10));
		for (Object[] dataObject : data) {
			Timestamp date = (Timestamp) dataObject[8];
			group = dataObject[4].toString();
			if (dataObject[7] == null) {
				description = "";
			} else {
				description = dataObject[7].toString();
			}
			databack.add(new TaskGroupModel(Long.parseLong(dataObject[0].toString()),
					Long.parseLong(dataObject[1].toString()), Long.parseLong(dataObject[2].toString()),
					dataObject[3].toString(), group, Long.parseLong(dataObject[5].toString()), dataObject[6].toString(),
					description, formatter.format(date.toLocalDateTime().atOffset(ZoneOffset.of(timezone))),
					Long.parseLong(dataObject[9].toString()), Long.parseLong(dataObject[10].toString()),
					Long.parseLong(dataObject[12].toString()), dataObject[11].toString()));
		}
		return databack;
	}
}
