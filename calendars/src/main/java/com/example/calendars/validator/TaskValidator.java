package com.example.calendars.validator;

import java.sql.SQLException;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.example.calendars.domain.Response;

@Component
public class TaskValidator {
	public void ValidatorErrorDatabase(SQLException e, Response result) {
		if (e.getErrorCode() == 1048) {
			// 1048=NULL_VALUE_FOUND
			result.setResponse(HttpStatus.INSUFFICIENT_STORAGE, e.getMessage());
		} else if (e.getErrorCode() == 1062) {
			// 1062=DUPLICATE_KEY_FOUND
			result.setResponse(HttpStatus.INSUFFICIENT_STORAGE, e.getMessage());
		} else if (e.getErrorCode() == 1205) {
			// 1205=RECORD_HAS_BEEN_LOCKED
			result.setResponse(HttpStatus.INSUFFICIENT_STORAGE, e.getMessage());
		}
	}
}
