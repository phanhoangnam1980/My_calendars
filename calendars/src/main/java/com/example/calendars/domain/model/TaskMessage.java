package com.example.calendars.domain.model;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "task_messages")
@Getter
@Setter
public class TaskMessage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long taskid;

	@Column(name = "title_task", nullable = false, length = 500)
	private String Title;

	@Column(name = "task_description", length = 1000)
	private String Description;

	@Column(name = "date_finished")
	private ZonedDateTime date;

	@Column(name = "user_id", nullable = false)
	private Long UserID;

	@Column(name = "was_delete", nullable = true)
	private ZonedDateTime IsDeleted;

	@Column(name = "created_at")
	private ZonedDateTime created;

	@Column(name = "parent_id")
	private Long parent;

	@Column(name = "count_comment", columnDefinition = "integer default 0")
	private int comments;

	@Column(name = "count_subtask", columnDefinition = "integer default 0")
	private int subcount;
	
	@Column(name = "complete_number", columnDefinition = "integer default 0")
	private int complete;
}
