import {AfterViewChecked, AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as moment from 'moment';
import {ShareDataService} from '../service/ShareData.service';
import {TaskSplitData} from '../model/domain/taskSplitData';
import {TaskService} from '../service/task.service';
import {TaskMessage} from '../model/TaskMessage';
import {Group} from '../model/group';
import {Section} from '../model/Section';
import {ChannelService} from '../service/Channel.service';
import {SectionService} from '../service/Section.service';
import {TranslocoService} from '@ngneat/transloco';
import {TaskGroup} from '../model/domain/TaskGroup';
import {Response} from '../model/domain/Response';
import {MatDialog} from '@angular/material/dialog';
import {DetailTaskComponent} from '../main_calendars/detail-task/detail-task.component';
import {NewModelTask} from '../model/domain/newModelTask';
import {DisplayDateData} from '../model/domain/DisplayDateData';
import {DateTimeCalendarsService} from '../service/DateTimeCalendars.service';

@Component({
  selector: 'app-project-task',
  templateUrl: './today.component.html',
  styleUrls: ['./today.component.css']
})
export class TodayComponent implements OnInit, AfterViewInit, AfterViewChecked {

  public DateDayTimeCurrent: string[];
  public day: string;
  public hideflag: boolean;
  public checkfinished: boolean;
  public locationcheckFinished: bigint;
  public displayTaskOverdue: Array<TaskGroup>;
  public displayTaskOnGoing: Array<TaskGroup>;
  public OverdueTaskFlag: boolean;
  public DefaultChannel: Group;
  public DefaultSection: Section;
  public flagOnGoingAddTask: boolean;
  public zone: string;

  // value for location Chart
  public flagChartAtOverdue: boolean;
  public flagChartAtToday: boolean;
  public locationChart: number;

  public colorbackgroudIndentify = ['#FFFF00', '#FFA500', '#FFC0CB', '#f5f5dc'];
  public BROverdue: string;
  public BROnGoing: string;

  // value for edits task
  public DataSelect: NewModelTask;
  public locationEditsOverDue: number;
  public locationEditsOnGoing: number;
  public GroupSelect: string;
  public SectionSelect: string;

  // display date change
  @ViewChild('datepickerFooter', {static: false}) datepickerFooter: ElementRef;
  public CheckAddTimeTaskState = false;
  public DisplayDate: DisplayDateData;
  public dateFinishTask: any;
  public TimeSetTaskFinish: any;

  constructor(private shareData: ShareDataService, private taskService: TaskService, private translocoService: TranslocoService,
              private channelService: ChannelService, private sectionService: SectionService, private dialog: MatDialog,
              private calendarsService: DateTimeCalendarsService) {
    this.zone = '';
    this.BROverdue = this.colorbackgroudIndentify[Math.floor(Math.random() * 4)];
    this.BROnGoing = this.colorbackgroudIndentify[Math.floor(Math.random() * 4)];
  }

  ngOnInit(): void {
    this.translocoService.selectTranslate('Today').subscribe(translate => {
      this.DateDayTimeCurrent = [translate, ...moment().locale(this.translocoService.getActiveLang()).format('ddd MMM DD').toString().split(' ')];
      this.day = moment().locale(this.translocoService.getActiveLang()).format('MMM DD ‧ ').toString() + translate;
      console.log(this.day + new Date(Date.now()).toString());
    });
    this.hideflag = false;
    this.taskService.listTodayTaskGroup().subscribe((dataResult: TaskSplitData) => {
      this.displayTaskOverdue = dataResult.overDueTask;
      this.displayTaskOnGoing = dataResult.onGoingTask;
      this.OverdueTaskFlag = dataResult.overDueTask.length === 0;
      // this.LocationShowChart();
    });
    this.checkfinished = false;
    this.channelService.GetDefaultChannel().subscribe(channel => {
      this.DefaultChannel = channel;
      this.sectionService.GetDefaultSection(channel.groupid).subscribe(section => {
        this.DefaultSection = section;
      });
    });
    // constructor value
    this.locationEditsOverDue = -4;
    this.DataSelect = new NewModelTask();
    setTimeout(() => {
      this.flagChartAtToday = true;
    }, 500);
  }

  ngAfterViewChecked(): void {
  }

  ngAfterViewInit(): void {
    this.shareData.currentStateAddTask.subscribe(() => {

    });
    this.shareData.currentCancelRequestNewTask.subscribe(statusCancel => {
      if (statusCancel === true) {
        this.hideflag = false;
      }
    });
    this.shareData.DataAddTask.subscribe(taskData => {
      console.log(taskData);
      if (taskData !== null) {
        this.taskService.listTodayTaskGroup().subscribe((dataResult: TaskSplitData) => {
          this.displayTaskOverdue = dataResult.overDueTask;
          this.displayTaskOnGoing = dataResult.onGoingTask;
          this.OverdueTaskFlag = dataResult.overDueTask.length === 0;
          this.locationEditsOnGoing = -4;
          this.locationEditsOverDue = -4;
        });
      }
    });
    this.shareData.CurrentCancelStatus.subscribe(result => {
      if (result === true) {
        this.locationEditsOnGoing = -4;
        this.locationEditsOverDue = -4;
      }
    });
  }


  openDataAddTask() {
    this.hideflag = true;
  }

  enterCheckAnimation(idlocation) {
    this.checkfinished = true;
    this.locationcheckFinished = idlocation;
  }

  leaveCheckAnimation() {
    this.checkfinished = false;
  }

  TaskFinishedOverDue(idlocation, idtask) {
    console.log('in ' + idlocation);
    this.checkfinished = false;
    this.displayTaskOverdue.splice(idlocation, 1);
    this.taskService.TaskFinished(idtask).subscribe();
  }

  TaskFinishedOnGoing(idlocation, idtask) {
    this.checkfinished = false;
    this.displayTaskOnGoing.splice(idlocation, 1);
    this.taskService.TaskFinished(idtask).subscribe();
  }

  PinnedTask(idtask) {
    this.taskService.TaskPinned(idtask).subscribe((result: Response) => {
      switch (result.code) {
        case 200:
          this.shareData.setDataAddTask(new TaskMessage());
          break;
        case 507:
          break;
      }
    });
  }

  DeletedTask(idtask) {
    this.taskService.DeleteTask(idtask).subscribe(() => {
      this.shareData.setDataAddTask(new TaskMessage());
    });
  }

  enterAddTask() {
    this.flagOnGoingAddTask = true;
  }

  outAddTask() {
    this.flagOnGoingAddTask = false;
  }

  ChangePriority(idtask, priority: number) {
    console.log(priority);
    this.taskService.SetKindForTask(idtask, BigInt(priority)).subscribe((result: Response) => {
      switch (result.code) {
        case 200:
          this.shareData.setDataAddTask(new TaskMessage());
          break;
        case 507:
          break;
      }
    });
  }

  MoreDetail(zoneSelect, index) {
    let selectedTask;
    if (zoneSelect === 'overdue') {
      selectedTask = this.displayTaskOverdue[index];
    }
    if (zoneSelect === 'ongoing') {
      console.log('ongoing');
      selectedTask = this.displayTaskOnGoing[index];
    }
    this.dialog.open(DetailTaskComponent, {
      width: '900px',
      data: {
        task: selectedTask,
        section: null
      },
    });
  }


  LocationShowChart() {
    switch (true) {
      case this.displayTaskOverdue.length > 4:
        this.flagChartAtOverdue = true;
        break;
      case (this.displayTaskOnGoing.length + this.displayTaskOverdue.length) > 4:
        this.flagChartAtToday = true;
        this.locationChart = this.displayTaskOnGoing.length - this.displayTaskOverdue.length - 1;
        break;
      default:
        this.flagChartAtOverdue = false;
        this.flagChartAtToday = false;
        break;
    }
  }

  SelectEdits(loccation: number, status: string) {
    let ChoseTask = new TaskGroup();
    switch (status) {
      case 'ongoing':
        this.locationEditsOnGoing = loccation;
        ChoseTask = this.displayTaskOnGoing[loccation];
        break;
      case 'overdue':
        this.locationEditsOverDue = loccation;
        ChoseTask = this.displayTaskOverdue[loccation];
        break;
    }
    if (ChoseTask.data.titerGroupTask.indexOf('/') === -1) {
      this.GroupSelect = ChoseTask.data.titerGroupTask;
      this.SectionSelect = '';
    } else {
      const data = ChoseTask.data.titerGroupTask.split('/');
      this.GroupSelect = data[0];
      this.SectionSelect = data[1];
    }
    this.DataSelect.datedata = ChoseTask.data.date;
    this.DataSelect.groupId = Number(ChoseTask.data.groupid);
    this.DataSelect.sectionId = Number(ChoseTask.data.sectionId);
    this.DataSelect.tittle = ChoseTask.data.title;
    this.DataSelect.description = ChoseTask.data.description;
    this.DataSelect.parentId = ChoseTask.data.taskid;
  }

  // change time function
  onOpen() {
    this.appendFooter();
    this.CheckAddTimeTaskState = false;
  }

  onClose() {
    this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
  }

  onclickAddTime() {
    this.CheckAddTimeTaskState = true;
  }

  cancelAddTime() {
    this.CheckAddTimeTaskState = false;
  }

  private appendFooter() {
    const matCalendar = document.getElementsByClassName('mat-datepicker-content')[0] as HTMLElement;
    matCalendar.appendChild(this.datepickerFooter.nativeElement);
  }

  processFinishAddTask(pickerDate) {
    pickerDate.close();
  }
}
