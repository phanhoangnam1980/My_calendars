import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UserService} from '../../../service/user.service';
import {ShareDataService} from '../../../service/ShareData.service';

@Component({
  selector: 'app-change-username',
  templateUrl: './change-username.component.html',
  styleUrls: ['./change-username.component.css']
})
export class ChangeUsernameComponent implements OnInit {
  public backgroundRandom: string;
  @Input()
  public defaultUsername: string;
  @Input()
  public newUsername: string;
  public messageError: string;
  private readonly default: string;
  public colorScheme = {
    domain: ['#FF8A80',
      '#EA80FC',
      '#8C9EFF',
      '#80D8FF',
      '#A7FFEB',
      '#CCFF90',
      '#FFFF8D',
      '#FF9E80']
  };

  constructor(private dialogRef: MatDialogRef<ChangeUsernameComponent>, @Inject(MAT_DIALOG_DATA) private data: any,
              private userService: UserService, private shareService: ShareDataService) {
    this.default = data.username;
  }

  ngOnInit(): void {
    console.log(this.default);
    const random = Math.floor(Math.random() * 8);
    this.backgroundRandom = this.colorScheme.domain[random];
  }

  closingDialog() {
    this.dialogRef.close();
  }

  ConfirmChange() {
    if (this.defaultUsername !== this.default) {
      this.messageError = 'Did you double check this? This still wrong :(';
      return;
    }
    if (!this.newUsername) {
      this.messageError = 'This require New Username not null';
      return;
    }
    this.userService.updateUsername(this.newUsername).subscribe();
    this.shareService.setAddNewUsername(true);
    this.dialogRef.close();
  }
}
