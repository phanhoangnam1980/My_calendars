package com.example.calendars.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomTaskMessage {
	private TaskModelex task;
	
	private boolean overdue;

	public CustomTaskMessage(TaskModelex task, boolean overdue) {
		super();
		this.task = task;
		this.overdue = overdue;
	}
}
