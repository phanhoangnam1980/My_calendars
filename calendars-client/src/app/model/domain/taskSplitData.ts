import {TaskGroup} from './TaskGroup';

export class TaskSplitData {
  onGoingTask: Array<TaskGroup>;
  overDueTask: Array<TaskGroup>;
}
