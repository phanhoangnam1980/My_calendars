package com.example.calendars.firebase.notification;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.calendars.domain.model.NotifiTask;
import com.example.calendars.domain.model.User;
import com.example.calendars.service.UserService;

@RestController
public class NotificationController {
	@Autowired
	private IFirebaseNotificationService firebaseNotificationService;
	@Autowired
	private UserService userService;

	@PostMapping("/test/notification")
	public List<String> testNotification(@RequestBody AppNotification app) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		return firebaseNotificationService.TestsendNotification(app, CurrentUser);
	}

	@Scheduled(cron = "0 0/5 * ? * *")
	public void notificationTaskNear() {
		firebaseNotificationService.DefaultSendNotification();
		firebaseNotificationService.CustomTimeSendNotification();
		System.out.println("schedule tasks using cron jobs - " + ZonedDateTime.now().toString());
	}

	@PostMapping("/NotifiTask")
	public void CheckNotification(@RequestBody NotifiTask task) {
		if (task.getType() == -4) {
			NotifiTask confirm = firebaseNotificationService.FindByTaskid(task.getId());
			if (confirm != null) {
				firebaseNotificationService.DeleteNotifyTask(task);
			}
		} else {
			NotifiTask confirm = firebaseNotificationService.FindByTaskid(task.getId());
			if (confirm == null) {
				firebaseNotificationService.saveNotifyTask(task);
			} else {
				if (!confirm.equals(task)) {
					firebaseNotificationService.UpdateNotifyTask(task);
				}
			}
		}
	}

	@GetMapping("/task/notification/{id}")
	public NotifiTask StatusNotifycationTask(@PathVariable long id) {
		return firebaseNotificationService.FindByTaskid(id);
	}
}
