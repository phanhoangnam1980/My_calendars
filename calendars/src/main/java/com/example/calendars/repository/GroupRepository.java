package com.example.calendars.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.example.calendars.domain.model.Group;

public interface GroupRepository extends JpaRepository<Group, Long> {
	@Query(value = "SELECT u FROM Group u WHERE u.UserID=:userid and u.grouptypeData=1")
	List<Group> findAllcustomProject(@Param("userid") Long userid);

	@Query(value = "SELECT u FROM Group u WHERE u.UserID=:userid and u.grouptypeData=0")
	Group findDefaultProject(@Param("userid") Long userid);

	@Transactional
	@Modifying
	@Query(value = "UPDATE Group u SET u.TitleGroupTask =:new_name Where u.UserID=:userid and u.groupid =:change_group")
	void updateTittleProject(@Param("new_name") String newgroupname, @Param("userid") Long userid,
			@Param("change_group") Long groupNeedChange);

	@Transactional
	@Modifying
	@Query(value = "UPDATE Group u SET u.TitleGroupTask = :new_name,u.ColorSpecs =:new_color Where u.UserID=:userid and u.groupid =:change_group")
	void updateProject(@Param("new_name") String newgroupname, @Param("new_color") String newcolor,
			@Param("userid") Long userid, @Param("change_group") Long groupNeedChange);

	@Query(value = "SELECT u FROM Group u Where u.groupid=:current_group_id")
	Group findByGroupID(@Param("current_group_id") Long groupID);

	@Transactional
	@Modifying
	@Query(value = "Update Group u Set u.CountComments=:comments where u.groupid=:groupid")
	void UpdateCountComments(@Param("comments") int number, @Param("groupid") Long group);

}
