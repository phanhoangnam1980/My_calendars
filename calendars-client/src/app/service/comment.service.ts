import {TokenResponse} from '../model/TokenResponse';
import {DateTimeCalendarsService} from './DateTimeCalendars.service';
import {Comments} from '../model/Comment';
import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CommentData} from '../model/domain/CommentData';

@Injectable()
export class CommentService {
  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));

  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + this.token.accessToken
  });

  constructor(private http: HttpClient, private dateTimeData: DateTimeCalendarsService) {
  }

  testComment() {
    return this.http.get<Comments>(environment.serverPort + '/GetSingleComment/9', {headers: this.headers});
  }

  GetCommentByProjectId(channelId: bigint) {
    return this.http.get<Array<CommentData>>(environment.serverPort + '/projectcomment/' + channelId, {headers: this.headers});
  }

  GetCommentByTaskId(taskId: bigint) {
    return this.http.get<Array<CommentData>>(environment.serverPort + '/taskcomment/' + taskId, {headers: this.headers});
  }

  SaveComment(Comment: CommentData) {
    return this.http.post(environment.serverPort + '/saveComment', Comment, {headers: this.headers});
  }

  GetCommentBySearch(prefix: string) {
    return this.http.get(environment.serverPort + '/search/comment?prefix=' + prefix, {headers: this.headers});
  }

  DeleteComment(id: bigint) {
    this.http.delete(environment.serverPort + '/comment/' + id, {headers: this.headers}).subscribe();
  }
}
