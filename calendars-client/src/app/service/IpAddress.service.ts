import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {DeviceModel} from '../model/DeviceModel';
import {TokenResponse} from '../model/TokenResponse';
import {TranslocoService} from '@ngneat/transloco';
import {BehaviorSubject} from 'rxjs';
import {Response} from '../model/domain/Response';

@Injectable()
export class IpAddressService {
  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));

  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    authorization: 'Bearer ' + this.token.accessToken
  });

  private idClient: string;
  public currentLanguage: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor(private http: HttpClient, private translocoService: TranslocoService) {
  }

  GetIpAndInfoClient() {
    return this.http.get('https://api.bigdatacloud.net/data/ip-geolocation?key=' + environment.apiKeyIp);
  }

  CheckInfoClient(current: DeviceModel) {
    return this.http.post(environment.serverPort + '/identify', current, {headers: this.headers});
  }

  SetIdClient(id: string) {
    this.idClient = id;
  }

  updateLanguage(language: string) {
    const Device = new DeviceModel();
    Device.id = this.idClient;
    Device.language = language;
    return this.http.patch(environment.serverPort + '/user/language', Device, {headers: this.headers});
  }
}
