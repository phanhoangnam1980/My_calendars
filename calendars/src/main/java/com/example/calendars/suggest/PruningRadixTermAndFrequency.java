package com.example.calendars.suggest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PruningRadixTermAndFrequency {
	
	private String term;
	
	private long termFrequencyCount;
	
	@Override
	public String toString() {
		return "TermAndFrequency [term=" + term + ", termFrequencyCount=" + termFrequencyCount + "]";
	}
	public PruningRadixTermAndFrequency(String term, long termFrequencyCount) {
		super();
		this.term = term;
		this.termFrequencyCount = termFrequencyCount;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((term == null) ? 0 : term.hashCode());
		result = prime * result + (int) (termFrequencyCount ^ (termFrequencyCount >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PruningRadixTermAndFrequency other = (PruningRadixTermAndFrequency) obj;
		if (term == null) {
			if (other.term != null)
				return false;
		} else if (!term.equals(other.term))
			return false;
		if (termFrequencyCount != other.termFrequencyCount)
			return false;
		return true;
	}
}
