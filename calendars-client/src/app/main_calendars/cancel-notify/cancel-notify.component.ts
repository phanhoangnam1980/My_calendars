import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ModalAddTaskComponent} from '../modal-add-task/modal-add-task.component';

@Component({
  selector: 'app-cancel-notify',
  templateUrl: './cancel-notify.component.html',
  styleUrls: ['./cancel-notify.component.css']
})
export class CancelNotifyComponent implements OnInit {

  constructor(private dialogRefAll: MatDialog, public dialogRef: MatDialogRef<CancelNotifyComponent>) {
  }

  ngOnInit(): void {
  }

  cancel() {
    this.dialogRef.close();
  }

  confirm() {
    this.dialogRefAll.closeAll();
  }

  closingDialog() {
    this.dialogRef.close();
  }
}
