import {AfterViewInit, Component, ElementRef, Inject, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ShareDataService} from '../service/ShareData.service';
import {ActivatedRoute} from '@angular/router';
import {TaskService} from '../service/task.service';
import {NewModelTask} from '../model/domain/newModelTask';
import {TaskMessage} from '../model/TaskMessage';
import {DateTimeCalendarsService} from '../service/DateTimeCalendars.service';
import {DisplayDateData} from '../model/domain/DisplayDateData';
import {GroupSectionModel} from '../model/domain/GroupSectionModel';
import {ChannelService} from '../service/Channel.service';
import {TranslocoService} from '@ngneat/transloco';
import * as moment from 'moment/moment';
import {SocketService} from '../service/socket.service';
import {SuggestModel} from '../model/domain/SuggestModel';
import {NotifiTask} from '../model/NotifiTask';

@Component({
  selector: 'app-addtask',
  templateUrl: './addtask.component.html',
  styleUrls: ['./addtask.component.css']
})
export class AddtaskComponent implements OnInit, AfterViewInit {
  @Input()
  dateFinishTask: any;
  @Input()
  TimeSetTaskFinish: any;
  public remainingtitleWords = 500;
  public remainingDescriptionWords = 1000;
  public DisplayDate: DisplayDateData;
  @ViewChild('datepickerFooter', {static: false}) datepickerFooter: ElementRef;
  private CheckCancelState: any;
  @Input()
  public currentSection: any;
  @Input()
  public currentProject: any;
  @Input()
  public currentSectionName: any;
  @Input()
  public colorProject: string;
  @Input()
  private projectId: bigint;
  @Input()
  public flagModal: boolean;
  @Input()
  public parentId: bigint;
  @Input()
  public exitsTask: NewModelTask;
  @Input()
  public daySelect: number;

  // suggest option
  @Input()
  public suggestOption: boolean;

  public CheckAddTimeTaskState = false;
  public id: number;
  public listgroup: Array<GroupSectionModel>;
  public typezero: bigint = BigInt(0);
  public IdSection: bigint;
  public ChannelSelected: bigint;
  public DisplayGroup: string;
  public flagInbox: boolean;

  // cache time
  private timeCache: any;
  private flagCheckNextWord: boolean;
  private positionWordCheck: number;
  private currentLength: number;
  // suggest Word
  public listHighlightTexts = [];
  private flagSuggest: boolean;
  // option choose
  public optionvalue: number;
  public numberChooseBefore: number;
  @ViewChild('menuSelect') trigger;

  constructor(private shareData: ShareDataService, private route: ActivatedRoute, private taskService: TaskService,
              private channelService: ChannelService, private calendarsService: DateTimeCalendarsService,
              private translocoService: TranslocoService, private socketService: SocketService) {
    this.DisplayDate = new DisplayDateData();
    this.listgroup = new Array<GroupSectionModel>();
    this.currentLength = -1;
    this.positionWordCheck = -1;
    this.flagSuggest = false;
  }

  public textareatitle = '';

  public textareaDescription;

  ngOnInit(): void {
    if (this.exitsTask != null) {
      console.log('edits-task');
      this.textareatitle = this.exitsTask.tittle;
      this.textareaDescription = this.exitsTask.description;
      this.dateFinishTask = new Date(Date.parse(this.exitsTask.datedata));
      this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
      this.channelService.GetListChannelSection().subscribe((data: Array<GroupSectionModel>) => {
        this.listgroup = data;
      });
      this.id = this.exitsTask.groupId;
      this.IdSection = BigInt(this.exitsTask.sectionId);
      this.ChannelSelected = BigInt(+this.id);
      console.log(this.currentSectionName);
      console.log(this.currentSectionName + ' ' + this.DisplayGroup);
      if (this.currentSectionName === '' || this.currentSectionName == null) {
        this.DisplayGroup = this.currentProject;
      } else {
        this.DisplayGroup = this.currentProject + '/' + this.currentSectionName;
      }
    } else {
      this.translocoService.selectTranslate('Today').subscribe(data => {
        this.DisplayDate.displayDay = data;
      });
      this.DisplayDate.colorDay = '#058527';
      this.id = +this.route.snapshot.paramMap.get('id');
      if (this.projectId) {
        this.id = Number(this.projectId);
      }
      if (this.daySelect != null) {
        this.dateFinishTask = new Date(moment().add(this.daySelect, 'days').toDate());
        console.log(this.dateFinishTask);
        this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
      } else {
        this.dateFinishTask = new Date(Date.now());
      }
      this.channelService.GetListChannelSection().subscribe((data: Array<GroupSectionModel>) => {
        this.listgroup = data;
      });
      if (this.currentSectionName === '' || this.currentSectionName == null) {
        this.DisplayGroup = this.currentProject;
      } else {
        this.DisplayGroup = this.currentProject + '/' + this.currentSectionName;
      }
      this.flagInbox = this.currentProject === 'Default Group';
      this.ChannelSelected = BigInt(+this.id);
      this.IdSection = this.currentSection;
      console.log(this.id);
      console.log(this.currentSection);
      console.log(this.colorProject);
    }
    this.timeCache = moment(this.dateFinishTask);
    console.log(this.dateFinishTask);
  }

  ngAfterViewInit(): void {
    if (this.textareatitle.length !== 0) {
      this.shareData.setAddTask(true);
    }
    if (this.flagModal === true) {
      this.shareData.AddTaskByModal.subscribe(data => {
        if (data === true) {
          this.ConfirmAddTask();
        }
      });
    }
    this.socketService.currentDataSuggest.subscribe((result: SuggestModel) => {
      if (result !== null && result.select !== null && this.suggestOption === true) {
        this.CaseSuggestClient(result);
        this.flagSuggest = true;
      }
    });
  }

  autogrowtitle() {
    const textArea = document.getElementById('title');
    textArea.style.overflow = 'hidden';
    textArea.style.height = 'auto';
    textArea.style.height = textArea.scrollHeight + 'px';
    const result = this.SendMessagePrefix();
    if (this.flagCheckNextWord === false) {
      this.socketService.SendSuggestPrefix(result);
    }
  }

  SendMessagePrefix() {
    if (this.textareatitle.length !== 0 && this.suggestOption === true) {
      const data = this.textareatitle.split(' ');
      this.currentLength = data.length;
      if (this.currentLength > (this.positionWordCheck + 1)) {
        this.flagCheckNextWord = false;
      }
      if (this.currentLength > 3) {
        return (data[this.currentLength - 3] + ' ' + data[this.currentLength - 2] + ' ' + data[this.currentLength - 1]).trim();
      }
      if (this.flagCheckNextWord === true) {
        const raw = moment(data[this.currentLength - 1], ['hA', 'ha', 'hhA', 'hha', 'k', 'kk',
          'h:mmA', 'h:mma', 'hh:mmA', 'hh:mma', 'k:mm', 'kk:mm',
          'h:mA', 'h:ma', 'hh:mA', 'hh:ma', 'k:m', 'kk:m']);
        if (raw.isValid()) {
          this.timeCache.hour(raw.hour()).minute(raw.minute());
        }
      }
      return this.textareatitle;
    }
  }

  autogrow() {
    const textArea = document.getElementById('description');
    textArea.style.overflow = 'hidden';
    textArea.style.height = 'auto';
    textArea.style.height = textArea.scrollHeight + 'px';
  }

  ConfirmAddTask() {
    if (this.textareatitle.length !== 0) {
      this.shareData.setAddTask(true);
      this.shareData.setCancelRequestNewTask(true);
      if (this.TimeSetTaskFinish != null) {
        let timeSetByMoment;
        if (this.TimeSetTaskFinish.indexOf('AM') > -1 || this.TimeSetTaskFinish.indexOf('PM') > -1) {
          timeSetByMoment = moment(this.TimeSetTaskFinish, 'HH:mmA');
        } else if (this.TimeSetTaskFinish.indexOf(':') > -1) {
          timeSetByMoment = moment(this.TimeSetTaskFinish, 'HH:mm');
        }
        this.dateFinishTask = timeSetByMoment;
      }
      const TaskModal = new NewModelTask();
      TaskModal.description = this.textareaDescription;
      TaskModal.tittle = this.textareatitle;
      if (!this.dateFinishTask) {
        this.dateFinishTask = new Date(Date.now()).toString();
      }
      TaskModal.datedata = new Date(this.dateFinishTask).toString().substring(0, 24);
      TaskModal.groupId = Number(this.ChannelSelected);
      if (this.IdSection === this.currentSection) {
        TaskModal.sectionId = this.currentSection;
      } else {
        TaskModal.sectionId = Number(this.IdSection);
      }
      if (this.parentId === null) {
        this.taskService.saveNewTask(TaskModal).subscribe((data: TaskMessage) => {
          if (this.optionvalue != null) {
            const raw = new NotifiTask();
            raw.type = this.optionvalue;
            raw.duration = this.numberChooseBefore;
            raw.taskid = data.taskid;
            this.taskService.ExtraNotification(raw).subscribe();
            this.trigger.closeMenu();
            raw.type = null;
            raw.duration = null;
          }
          this.shareData.setDataAddTask(data);
        });
      } else {
        TaskModal.parentId = this.parentId;
        this.taskService.saveSubTask(TaskModal).subscribe((data: TaskMessage) => {
          this.shareData.setDataAddTask(data);
        });
      }
    }
  }

  CancelStateAddTask() {
    if (this.exitsTask != null) {
      this.shareData.SetCancelStatus(true);
    } else {
      this.shareData.setCancelRequestNewTask(true);
    }
  }

  onclickAddTime() {
    this.CheckAddTimeTaskState = true;
  }

  cancelAddTime() {
    this.CheckAddTimeTaskState = false;
  }

  onOpen() {
    this.appendFooter();
    this.CheckAddTimeTaskState = false;
  }

  onClose() {
    console.log(this.dateFinishTask);
    this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
    console.log(this.TimeSetTaskFinish);
  }

  processFinishAddTask(pickerDate) {
    pickerDate.close();
  }

  private appendFooter() {
    const matCalendar = document.getElementsByClassName('mat-datepicker-content')[0] as HTMLElement;
    matCalendar.appendChild(this.datepickerFooter.nativeElement);
  }

  onSelectGroupSection(groupid: bigint, sectionId: bigint, titleProject: string, titleSection: string, colorSelect: string) {
    this.ChannelSelected = groupid;
    this.IdSection = sectionId;
    this.colorProject = colorSelect;
    if (titleSection === '') {
      this.DisplayGroup = titleProject;
    } else {
      this.DisplayGroup = titleProject + '/' + titleSection;
    }
    this.flagInbox = titleProject === 'Inbox';
  }

  CaseSuggestClient(rawData: SuggestModel) {
    console.log(rawData.result);
    const checkSize = this.listHighlightTexts.length;
    this.listHighlightTexts.push(rawData.result);
    this.listHighlightTexts = this.listHighlightTexts.filter((v, i, a) => a.indexOf(v) === i);
    if (this.listHighlightTexts.length !== checkSize) {
      switch (rawData.select) {
        case 'Today':
          this.timeCache = moment().toDate();
          this.dateFinishTask = moment().toDate();
          this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
          break;
        case 'Tomorrow':
          this.dateFinishTask = this.timeCache.add(1, 'd').toDate();
          this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
          break;
        case 'Loop':
          break;
        case 'everyday':
          break;
        case 'everymorning':
          break;
        case 'everyevening':
          break;
        case 'everyweekend':
          break;
        case 'every':
          break;
        case 'nextweek':
          this.dateFinishTask = this.timeCache.add(7, 'days').toDate();
          this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
          break;
        case 'nextmonth':
          this.dateFinishTask = this.timeCache.add(1, 'M').toDate();
          this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
          break;
        case 'mid':
          break;
        case 'Todayat':
          this.flagCheckNextWord = true;
          this.timeCache = moment();
          this.positionWordCheck = JSON.parse(JSON.stringify(this.currentLength));
          this.dateFinishTask = this.timeCache.toDate();
          this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
          break;
        case 'Tomorrowat':
          this.flagCheckNextWord = true;
          this.timeCache = moment().add(1, 'd');
          this.positionWordCheck = JSON.parse(JSON.stringify(this.currentLength));
          this.dateFinishTask = this.timeCache.toDate();
          this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
          // looking date
          break;
        case 'inthemorning':
          // 9am
          this.dateFinishTask = this.timeCache.set('hour', 9).set('minute', 0).set('second', 0).set('millisecond', 0).toDate();
          this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
          break;
        case 'intheafternoon':
          // 2pm
          this.dateFinishTask = this.timeCache.set('hour', 14).set('minute', 0).set('second', 0).set('millisecond', 0).toDate();
          this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
          break;
        case 'intheevening':
          // 9pm
          this.dateFinishTask = this.timeCache.set('hour', 21).set('minute', 0).set('second', 0).set('millisecond', 0).toDate();
          this.DisplayDate = this.calendarsService.CheckTodayTomorrow(this.dateFinishTask);
          break;
      }
    }
  }

  checkIfWordBackspace(event) {
    if (!this.flagSuggest) {
      return null;
    }
    if (event.keyCode === 8 && this.listHighlightTexts.length > 0) {
      this.flagSuggest = false;
      this.listHighlightTexts.pop();
    }
  }

  RemoveIndex(index) {
    this.listHighlightTexts.splice(index, 1);
  }

  UpdateTask() {
    if (this.textareatitle.length !== 0) {
      if (this.exitsTask.tittle !== this.textareatitle || this.exitsTask.description !== this.textareaDescription ||
        new Date(this.dateFinishTask).toString().substring(0, 24) !== this.exitsTask.datedata) {
        const task = new TaskMessage();
        if (this.TimeSetTaskFinish != null) {
          let timeSetByMoment;
          if (this.TimeSetTaskFinish.indexOf('AM') > -1 || this.TimeSetTaskFinish.indexOf('PM') > -1) {
            timeSetByMoment = moment(this.TimeSetTaskFinish, 'HH:mmA');
          } else if (this.TimeSetTaskFinish.indexOf(':') > -1) {
            timeSetByMoment = moment(this.TimeSetTaskFinish, 'HH:mm');
          }
          this.dateFinishTask = timeSetByMoment;
        }
        task.taskid = this.exitsTask.parentId;
        task.description = this.textareaDescription;
        task.title = this.textareatitle;
        if (!this.dateFinishTask) {
          this.dateFinishTask = new Date(Date.now()).toString();
        }
        task.date = new Date(this.dateFinishTask).toString().substring(0, 24);
        this.taskService.UpdateTask(task).subscribe( () => {
          this.shareData.setDataAddTask(task);
        });
      }
    }
  }

  blockclose(event) {
    event.stopPropagation();
  }

  SaveNotification() {
    this.trigger.closeMenu();
  }

  CancelNotification() {
    this.trigger.closeMenu();
  }
}
