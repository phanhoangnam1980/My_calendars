import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {LoginComponent} from './signin/signin.component';
import {LoginService} from './service/Login.service';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {SignupComponent} from './signup/signup.component';
import {SignupService} from './service/Signup.service';
import {MainComponent} from './main_calendars/main.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSidenavModule} from '@angular/material/sidenav';
import {UserService} from './service/user.service';
import {DateTimeCalendarsService} from './service/DateTimeCalendars.service';
import {ProjectsComponent} from './projects/projects.component';
import {UpcomingComponent} from './upcoming_day/upcoming.component';
import {TodayComponent} from './today/today.component';
import {MatNativeDateModule, MatOptionModule} from '@angular/material/core';
import {AddtaskComponent} from './addtask/addtask.component';
import {MatInputModule} from '@angular/material/input';
import {ShareDataService} from './service/ShareData.service';
import {AddprojectComponent} from './main_calendars/addproject/addproject.component';
import {TaskService} from './service/task.service';
import {ChannelService} from './service/Channel.service';
import {AuthenticationService} from './service/authentication.service';
import {MatMenuModule} from '@angular/material/menu';
import {EditProjectComponent} from './main_calendars/edit-project/edit-project.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {AddsectionComponent} from './addsection/addsection.component';
import {SectionService} from './service/Section.service';
import {TranslocoRootModule} from './transloco-root.module';
import {CommentService} from './service/comment.service';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {MatDialogModule} from '@angular/material/dialog';
import {ChartsComponent} from './main_calendars/charts/charts.component';
import {UsersDetailComponent} from './main_calendars/users-detail/users-detail.component';
import {ChangePasswordComponent} from './main_calendars/users-detail/change-password/change-password.component';
import {ChangeUsernameComponent} from './main_calendars/users-detail/change-username/change-username.component';
import {SocketService} from './service/socket.service';
import {PintaskComponent} from './main_calendars/pintask/pintask.component';
import {CommentComponent} from './projects/comment/comment.component';
import {SearchComponent} from './search/search.component';
import {DetailTaskComponent} from './main_calendars/detail-task/detail-task.component';
import {ModalAddTaskComponent} from './main_calendars/modal-add-task/modal-add-task.component';
import {CancelNotifyComponent} from './main_calendars/cancel-notify/cancel-notify.component';
import {ChartTodayComponent} from './today/chart-today/chart-today.component';
import {WelcomePageComponent} from './welcome-page/welcome-page.component';
import {IpAddressService} from './service/IpAddress.service';
import {ImageService} from './service/image.service';
import {MatSelectModule} from '@angular/material/select';
import {SwiperModule} from 'swiper/angular';
import {environment} from '../environments/environment';
import firebase from 'firebase/compat/app';
import initializeApp = firebase.initializeApp;
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {NgxSpinnerModule} from 'ngx-spinner';
import { DetailImageComponent } from './detail-image/detail-image.component';

initializeApp(environment.firebase);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    MainComponent,
    ProjectsComponent,
    UpcomingComponent,
    TodayComponent,
    AddtaskComponent,
    AddprojectComponent,
    EditProjectComponent,
    AddsectionComponent,
    ChartsComponent,
    UsersDetailComponent,
    ChangePasswordComponent,
    ChangeUsernameComponent,
    PintaskComponent,
    CommentComponent,
    SearchComponent,
    DetailTaskComponent,
    ModalAddTaskComponent,
    CancelNotifyComponent,
    ChartTodayComponent,
    WelcomePageComponent,
    DetailImageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatSidenavModule,
    MatNativeDateModule,
    MatInputModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatTooltipModule,
    TranslocoRootModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    SwiperModule,
    MatSnackBarModule,
    NgxSpinnerModule
  ],
  providers: [
    LoginService,
    SignupService,
    UserService,
    DateTimeCalendarsService,
    ShareDataService,
    TaskService,
    ChannelService,
    AuthenticationService,
    SectionService,
    CommentService,
    SocketService,
    IpAddressService,
    ImageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
