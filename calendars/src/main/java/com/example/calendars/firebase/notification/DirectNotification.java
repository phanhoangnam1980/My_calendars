package com.example.calendars.firebase.notification;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DirectNotification extends AppNotification {
	private String title;

	private String message;
	
	private String target;
}
