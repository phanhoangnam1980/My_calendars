package com.example.calendars.domain;

import java.util.List;

public class TaskSplitDataCompare {
	private List<TaskModelex> onGoingTask;

	private List<TaskModelex> OverDueTask;

	public List<TaskModelex> getOnGoingTask() {
		return onGoingTask;
	}

	public void setOnGoingTask(List<TaskModelex> onGoingTask) {
		this.onGoingTask = onGoingTask;
	}

	public List<TaskModelex> getOverDueTask() {
		return OverDueTask;
	}

	public void setOverDueTask(List<TaskModelex> overDueTask) {
		OverDueTask = overDueTask;
	}
}
