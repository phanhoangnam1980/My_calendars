package com.example.calendars.firebase;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.calendars.domain.CustomImageTask;
import com.example.calendars.domain.model.Images;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.HttpMethod;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageException;
import com.google.cloud.storage.StorageOptions;

@Service
public class FirebaseImageService implements IImageService {

	@Value("${firebase.bucket-name}")
	private String bucketName;

	@Value("${firebase.image-url}")
	private String url;

	@Value("${firebase.baseFirst-image-url}")
	private String imageurl;

	@Value("${firebase.baselast-image-url}")
	private String lasturl;

	@Value("${firebase.projectid}")
	private String projectid;

	private ServiceAccountCredentials creds;

	@Autowired
	private ImageRepository imageRepository;

	private DecimalFormat df = new DecimalFormat("#");

	public FirebaseImageService() {
		/* Initialize credentials and service account email */
		ClassLoader classLoader = FirebaseImageService.class.getClassLoader();
		File file = new File(Objects.requireNonNull(classLoader.getResource("mycalendars-firebase.json")).getFile());
		try (InputStream inputStream = new FileInputStream(file.getAbsolutePath())) {
			this.creds = ServiceAccountCredentials.fromStream(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getSignedUrl(String objectName, String mimeType) {
		// TODO Auto-generated method stub
		String signed_url = null;
		try {
			Storage storage = StorageOptions.newBuilder().setCredentials(creds).build().getService();
			// Define resource
			BlobInfo blobInfo = BlobInfo.newBuilder(BlobId.of(bucketName, objectName)).build();
			// Generate Signed URL
			Map<String, String> extensionHeaders = new HashMap<>();
			extensionHeaders.put("content-type", "image/jpeg");

			signed_url = storage.signUrl(blobInfo, 10, TimeUnit.MINUTES,
					Storage.SignUrlOption.httpMethod(HttpMethod.POST),
					Storage.SignUrlOption.withExtHeaders(extensionHeaders), Storage.SignUrlOption.withV4Signature())
					.toString();
		} catch (StorageException ex) {
			ex.printStackTrace();
		}
		return signed_url;
	}

	@Override
	public String getLocation(String objectName, String mimeType) throws IOException {
		// TODO Auto-generated method stub
		URL myURL = new URL(getSignedUrl(objectName, mimeType));
		HttpURLConnection myURLConnection = (HttpURLConnection) myURL.openConnection();
		myURLConnection.setRequestMethod("POST");
		myURLConnection.setRequestProperty("Content-Type", mimeType);
		myURLConnection.setRequestProperty("x-goog-resumable", "start");
		// Send post request
		myURLConnection.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(myURLConnection.getOutputStream());
		wr.flush();
		wr.close();
		int responseCode = myURLConnection.getResponseCode();
		if (responseCode != 201) {
			System.out.println("Request Failed");
		}
		return myURLConnection.getHeaderField("Location");
	}

	@Override
	public Images UploadToStorage(MultipartFile multipartFile, String mimeType) {
		Images result = new Images();
		ArrayList<byte[]> imageStorage = new ArrayList<>();
		Storage storage = StorageOptions.newBuilder().setCredentials(creds).build().getService();
		String originName = multipartFile.getOriginalFilename();
		String objectName = generateFileName(originName);
		BlobId blobId = BlobId.of(bucketName, objectName);
		BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(mimeType).build();
		String objectName2 = generateFileName(originName);
		BlobId blobId2 = BlobId.of(bucketName, objectName2 + "-d");
		BlobInfo blobInfo2 = BlobInfo.newBuilder(blobId2).setContentType(mimeType).build();
		String objectName3 = generateFileName(originName);
		BlobId blobId3 = BlobId.of(bucketName, objectName3 + "-t");
		BlobInfo blobInfo3 = BlobInfo.newBuilder(blobId3).setContentType(mimeType).build();
		result.setFull(objectName);
		result.setViewImage(objectName2 + "-d");
		result.setThumbshot(objectName3 + "-t");
		try {
			String extension = originName.substring(originName.lastIndexOf(".") + 1);
			BufferedImage image = ImageIO.read(multipartFile.getInputStream());
			SizeReduceImage(imageStorage, image.getHeight(), image.getWidth(), extension, originName,
					multipartFile.getInputStream());
			storage.create(blobInfo, multipartFile.getBytes());
			storage.create(blobInfo2, imageStorage.get(0));
			storage.create(blobInfo3, imageStorage.get(1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public String generateFileName(String name) {
		return Base64.getEncoder().encodeToString(
				(new Date().getTime() + "-" + Objects.requireNonNull(name).replace(" ", "_")).getBytes());
	}

	@Override
	public byte[] resizeImage(InputStream input, int width, int height, String mimeType) {
		// TODO Auto-generated method stub
		byte[] result = null;
		try {
			BufferedImage originalImage = ImageIO.read(input);
			Image resultingImage = originalImage.getScaledInstance(width, height, Image.SCALE_DEFAULT);
			BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			outputImage.getGraphics().drawImage(resultingImage, 0, 0, null);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(outputImage, mimeType, baos);
			baos.flush();
			result = baos.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public void SizeReduceImage(ArrayList<byte[]> storage, int height, int width, String mimeType, String name,
			InputStream rawData) {
		double reduceSize;
		InputStream firstClone = null, secondClone = null;
		ByteArrayOutputStream rawBaos = new ByteArrayOutputStream();
		try {
			rawData.transferTo(rawBaos);
			firstClone = new ByteArrayInputStream(rawBaos.toByteArray());
			secondClone = new ByteArrayInputStream(rawBaos.toByteArray());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int heightD = Integer.parseInt(df.format(height * 0.7));
		int widthD = Integer.parseInt(df.format(width * 0.7));
		if (widthD > 900 || heightD > 600) {
			double reduceSizeH = Double.valueOf(height) / 600;
			double reduceSizeW = Double.valueOf(width) / 900;
			reduceSize = Math.max(reduceSizeH, reduceSizeW);
			heightD = Integer.parseInt(df.format(height / reduceSize));
			widthD = Integer.parseInt(df.format(width / reduceSize));
		}
		storage.add(resizeImage(firstClone, widthD, heightD, mimeType));
		double reduceSizeH = Double.valueOf(height) / 250;
		double reduceSizeW = Double.valueOf(width) / 305;
		reduceSize = Math.max(reduceSizeH, reduceSizeW);
		int heightT = Integer.parseInt(df.format(height / reduceSize));
		int widthT = Integer.parseInt(df.format(width / reduceSize));
		if (heightT > 250 || widthT > 305) {
			reduceSize += 0.2;
			heightT = Integer.parseInt(df.format(height / reduceSize));
			widthT = Integer.parseInt(df.format(width / reduceSize));
		}
		storage.add(resizeImage(secondClone, widthT, heightT, mimeType));
	}

	@Override
	public void save(Images image) {
		// TODO Auto-generated method stub
		imageRepository.save(image);
	}

	@Override
	public Images ImageUploadAvatar(MultipartFile multipartFile) {
		// TODO Auto-generated method stub
		Images result = new Images();
		ArrayList<byte[]> imageStorage = new ArrayList<>();
		Storage storage = StorageOptions.newBuilder().setCredentials(creds).build().getService();
		String originName = multipartFile.getOriginalFilename();
		String objectName = generateFileName(originName);
		BlobId blobId = BlobId.of(bucketName, objectName);
		BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(multipartFile.getContentType()).build();
		String objectName2 = generateFileName(originName);
		BlobId blobId2 = BlobId.of(bucketName, objectName2 + "-t");
		BlobInfo blobInfo2 = BlobInfo.newBuilder(blobId2).setContentType(multipartFile.getContentType()).build();
		// reside avatar match 128 and 32
		String extension = originName.substring(originName.lastIndexOf(".") + 1);
		InputStream firstClone = null, secondClone = null;
		ByteArrayOutputStream rawBaos = new ByteArrayOutputStream();
		try {
			multipartFile.getInputStream().transferTo(rawBaos);
			firstClone = new ByteArrayInputStream(rawBaos.toByteArray());
			secondClone = new ByteArrayInputStream(rawBaos.toByteArray());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		imageStorage.add(resizeImage(firstClone, 32, 32, extension));
		imageStorage.add(resizeImage(secondClone, 128, 128, extension));
		storage.create(blobInfo, imageStorage.get(0));
		storage.create(blobInfo2, imageStorage.get(1));
		// add back to image avatar
		result.setViewImage(objectName);
		result.setThumbshot(objectName2 + "-t");
		return result;
	}

	@Override
	public List<Images> getListImageFromUser(Long userid) {
		// TODO Auto-generated method stub
		List<Images> rawData = imageRepository.FindImageInUser(userid);
		int rawSize = rawData.size();
		for (int i = 0; i < rawSize; i++) {
			rawData.get(i).setFull(imageurl + rawData.get(i).getFull() + lasturl);
			rawData.get(i).setViewImage(imageurl + rawData.get(i).getViewImage() + lasturl);
			rawData.get(i).setThumbshot(imageurl + rawData.get(i).getThumbshot() + lasturl);
		}
		return rawData;
	}

	@Override
	public String ImagesUploadBackground(MultipartFile multipartFile) {
		// TODO Auto-generated method stub
		Storage storage = StorageOptions.newBuilder().setCredentials(creds).build().getService();
		String originName = multipartFile.getOriginalFilename();
		String objectName = generateFileName(originName);
		BlobId blobId = BlobId.of(bucketName, objectName);
		BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(multipartFile.getContentType()).build();
		try {
			storage.create(blobInfo, multipartFile.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objectName;
	}

	@Override
	public List<Images> GetListImageFromCommentTask(Long taskid) {
		// TODO Auto-generated method stub
		return imageRepository.GetListImageFromCommentTask(taskid);
	}

	@Override
	public List<Images> GetListImageFromCommentProject(Long projectId) {
		// TODO Auto-generated method stub
		return imageRepository.GetListImageFromCommentProject(projectId);
	}

	@Override
	public List<CustomImageTask> GetListImageByTask(Long userid) {
		// TODO Auto-generated method stub
		List<CustomImageTask> result = new ArrayList<>();
		List<Object[]> raw = imageRepository.GetImageTaskByProject(userid,PageRequest.of(0, 10));
		for (Object[] data : raw) {
			result.add(new CustomImageTask(Long.parseLong(data[0].toString()), Long.parseLong(data[1].toString()),
					imageurl + data[2].toString() + lasturl, imageurl + data[3].toString() + lasturl,
					imageurl + data[4].toString() + lasturl, "", Long.parseLong(data[6].toString())));
		}
		return result;
	}

	@Override
	public Images GetImageAvatar(Long userid) {
		// TODO Auto-generated method stub
		return imageRepository.FindImageAvatar(userid);
	}
}
