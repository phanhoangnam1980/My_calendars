package com.example.calendars.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DetailAvatar")
@Getter
@Setter
public class DetailUser {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long userid;

	private Long idImage;

	@Column(name = "FirstChartId")
	private Long firstChart;

	@Column(name = "SecondChartId")
	private Long secondChart;

	@Column(columnDefinition = "TEXT")
	private String backgroundImage;
	
	public DetailUser(Long userid, Long idImage) {
		super();
		this.userid = userid;
		this.idImage = idImage;
	}

	public DetailUser() {
		super();
	}

}
