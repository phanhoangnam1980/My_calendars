package com.example.calendars.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "images")
@Getter
@Setter
public class Images {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "commentid")
	private Long comment;

	@Column(name = "thumbnail", columnDefinition = "TEXT")
	private String thumbshot;

	@Column(name = "viewImage", columnDefinition = "TEXT")
	private String viewImage;

	@Column(name = "fullsize", columnDefinition = "TEXT")
	private String full;

	@Column(name = "imageSpecs")
	private String metadata;
}
