package com.example.calendars.suggest;

import java.util.List;

public interface SuggestService {
	void SetupDatabaseSearch(List<String> suggestList, String language);

	String wordSuggest(String prefix, String url);
	
	SuggestResultModel CaseLanguageSuggest(SuggestReceiveModel model);
}
