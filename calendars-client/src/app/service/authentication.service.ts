import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../model/User';

@Injectable({providedIn: 'root'})
export class AuthenticationService implements OnDestroy {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor() {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(
      'token'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  ngOnDestroy(): void {
    this.currentUserSubject.unsubscribe();
  }

}
