package com.example.calendars.firebase.notification;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.calendars.domain.model.NotifiTask;

public interface NotificationTaskRepository extends JpaRepository<NotifiTask, Long> {
	@Transactional
	@Modifying
	@Query(value = "Update NotifiTask u Set u.type=:newType ,u.duration =:newDuration Where u.taskid = :currentTask")
	void UpdateTypeNotification(@Param("newType") int type, @Param("newDuration") int duration,
			@Param("currentTask") Long taskid);

	@Transactional
	@Modifying
	@Query(value = "DELETE FROM NotifiTask u Where u.taskid = :currentTask")
	void DeleteNotification(@Param("currentTask") Long taskid);

	@Query(value = "select u from NotifiTask u where u.taskid =:currentTask")
	NotifiTask findByTaskid(@Param("currentTask") Long taskid);

	@Query(value = "select u.type,u.duration,t2.Title,t2.Description,t2.date,t3.language,t3.token from NotifiTask u inner join TaskMessage t2 on t2.taskid=u.taskid inner join DeviceUser t3 on t2.UserID = t3.userid")
	List<Object[]> CheckListTask();
}
