package com.example.calendars.service;

import com.example.calendars.domain.model.DetailUser;

public interface DetailUserService {
	void save(DetailUser image);

	// need finish more
	void UpdateAvatar(Long avatarid, Long userid);

	void UpdateProjectChart(Long firstid, Long secondid, Long userid);

	void Updatebackground(String background, Long userid);
	
	DetailUser FindByuserId(Long userid);
}
