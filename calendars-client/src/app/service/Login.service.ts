import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../model/User';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Platform} from '@angular/cdk/platform';

@Injectable({providedIn: 'root'})
export class LoginService {

  private headers = new HttpHeaders({'Content-Type': 'application/json'});


  constructor(private http: HttpClient, private platform: Platform ) {
  }

  login(user: User): Observable<any> {
    this.identifyPlatform();
    return this.http.post(environment.serverPort + '/signin', user, {headers: this.headers});
  }

  SigninWithGoogle(tokenData: string) {
    return this.http.post(environment.serverPort + '/signin/google', tokenData, {headers: this.headers});
  }

  identifyPlatform() {
    switch (true) {
      case this.platform.ANDROID:
        break;
      case this.platform.IOS:
        break;
      case this.platform.isBrowser:
        this.headers.append('platform-device',navigator.userAgent.toString());
        break;
    }
  }
}
