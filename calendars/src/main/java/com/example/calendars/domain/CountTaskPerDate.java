package com.example.calendars.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountTaskPerDate {
	private int value;
	
	private String name;

	public CountTaskPerDate() {
		super();
	}

	public CountTaskPerDate(int value, String name) {
		super();
		this.value = value;
		this.name = name;
	}
}
