package com.example.calendars.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "detail_task_group")
@Getter
@Setter
public class DetailTaskGroup {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "taskid")
	private Long task;

	@Column(name = "groupid")
	private Long groups;

	@Column(name = "sectionid")
	private Long section;
	
	@Column(name = "kinds")
	private Long kinds;
}
