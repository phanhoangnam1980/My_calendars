package com.example.calendars.service;

import com.example.calendars.domain.model.Section;

public interface SectionService {
	void save(Section section);

	Section GetSectionById(Long id);

	void UpdateTitleSection(Section data);

	void DeleteSection(Long id);

	void HideSection(Long id, Boolean flag);
	
	Section saveDefault();
	
	Section getDefaultSection(Long id);
	
	Section FindSectionInfoByTask(Long id);
}
