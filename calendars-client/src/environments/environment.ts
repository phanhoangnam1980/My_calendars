// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  baseUrl: 'http://localhost:4200',
  production: false,
  serverPort: 'http://localhost:8080',
  socketPort: 'http://localhost:8080',
  apiKeyIp: 'a35968630b1248e3810e0b0f925ca319',
  urlViewImage: 'https://firebasestorage.googleapis.com/v0/b/mycalendars-5e436.appspot.com/o/',
  firebase: {
    apiKey: 'AIzaSyAtPSB-ILrluRixRXYSPf5uMERphbqdyXM',
    authDomain: 'mycalendars-5e436.firebaseapp.com',
    projectId: 'mycalendars-5e436',
    storageBucket: 'mycalendars-5e436.appspot.com',
    messagingSenderId: '133847714360',
    appId: '1:133847714360:web:5a6cd07492ae4fd774f4cd',
    measurementId: 'G-HTYVVSSZ3S',
    vapidKey: 'BMOwzel_80t9gAESodLC3yTgvveoOYqIAJ4kAA9pHpCaWkUCA8_tRwU-NgyWgLr3LmaYMFBLJwoq9oPM9wLFKzk'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
