import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {DayDisplay} from '../model/DayDisplay';
import {DisplayDateData} from '../model/domain/DisplayDateData';
import {TranslocoService} from '@ngneat/transloco';
import {ModelChart} from '../model/domain/ModelChart';
import {CustomTaskGroupByDay} from '../model/domain/CustomTaskGroupByDay';

@Injectable({providedIn: 'root'})
export class DateTimeCalendarsService {
  // This help to calendar what day of week and month since this were simple to make this don't have library storage day
  private DayOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  private DayWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  private TotalDayPerMonth = ['31', '28', '31', '30', '31', '30', '31', '31', '30', '31', '30', '31'];
  private MonthPerYears = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
    'September', 'October', 'November', 'December'];

  constructor(private translocoService: TranslocoService) {
  }

  private currentTimezone: string;

  public getTimeZone() {
    this.currentTimezone = new Date(Date.now()).toString().split(' ')[5];
    this.currentTimezone = this.currentTimezone.slice(this.currentTimezone.length - 5, this.currentTimezone.length);
    return this.currentTimezone;
  }

  public isLeapYear(yearcheck): boolean {
    return yearcheck % 4 === 0;
  }

  public getDayOfWeek() {
    return this.DayOfWeek;
  }

  public getThisWeek(currentDate: string[], language: string): any[] {
    const ListDayWeek = [];
    const indexDay = this.DayOfWeek.findIndex(daycheck => daycheck === currentDate[0]);
    const localZone = moment().locale(language);
    for (let i = indexDay; i < 7; i++) {
      ListDayWeek[i - indexDay] = localZone.add(i,'days').format('MMM DD ‧ dddd');
    }
    return ListDayWeek;
  }

  public AddThisWeek(datatask: Array<CustomTaskGroupByDay>, language: string): any[] {
    for (let i = 0; i < 21; i++) {
      datatask[i].dayDetail = moment().locale(language).add(i,'days').format('MMM DD ‧ dddd');
    }
    const FirstNeedReplace = datatask[0].dayDetail.split('‧');
    const SecNeedReplace = datatask[1].dayDetail.split('‧');
    this.translocoService.selectTranslate('Today').subscribe(translate => {
      datatask[0].dayDetail = datatask[0].dayDetail.replace(FirstNeedReplace[1], translate);
    });
    this.translocoService.selectTranslate('Tomorrow').subscribe(translate => {
      datatask[1].dayDetail = datatask[1].dayDetail.replace(SecNeedReplace[1], translate);
    });
    return datatask;
  }

  public getShortWeek(weekAdd: number, language: string): any[] {
    const ListDayWeek = new Array(7);
    const localZone = moment().locale(language).add(weekAdd, 'week');
    for (let i = 0; i < 7; i++) {
      ListDayWeek[i] = localZone.add(i,'days').format('ddd DD').split(' ');
    }
    return ListDayWeek;
  }

  public getDisplayWeek(weekAdd: number, language: string): any[] {
    const ListDayWeek = new Array(7);
    const localTime = moment().locale(language).add(weekAdd, 'week');
    for (let i = 0; i < 7; i++) {
      ListDayWeek[i] = localTime.add(i,'days').format('MMM DD ‧ dddd');
    }
    return ListDayWeek;
  }

  public DisplayFirstThreeWeek(language: string): any[] {
    const currentDateData = new Date(Date.now()).toString().split(' ');
    const Result = this.getThisWeek(currentDateData, language);
    Result.push(...this.getDisplayWeek(1, language));
    Result.push(...this.getDisplayWeek(2, language));
    const FirstNeedReplace = Result[0].split('‧');
    const SecNeedReplace = Result[1].split('‧');
    this.translocoService.selectTranslate('Today').subscribe(translate => {
      Result[0] = Result[0].replace(FirstNeedReplace[1], translate);
    });
    this.translocoService.selectTranslate('Tomorrow').subscribe(translate => {
      Result[1] = Result[1].replace(SecNeedReplace[1], translate);
    });
    return Result;
  }

  public DetailFirstDay(stepWeek: number, language: string): DayDisplay {
    const daydisplay = new DayDisplay();
    const FisrtDayDisplay = moment().locale(language).add(stepWeek, 'week');
    const DateData = FisrtDayDisplay.format('MMMM.yyyy').toString().split('.');
    if (this.currentTimezone == null) {
      this.currentTimezone = new Date(Date.now()).toString().split(' ')[5];
      this.currentTimezone = this.currentTimezone.slice(this.currentTimezone.length - 5, this.currentTimezone.length);
    }
    daydisplay.Year = DateData[1];
    daydisplay.Month = DateData[0];
    daydisplay.Timezone = this.currentTimezone;
    return daydisplay;
  }

  public CheckTodayTomorrow(dateFinishTask: Date): DisplayDateData {
    const result = new DisplayDateData();
    const dateOnly = Number(dateFinishTask.getDate());
    const diffMonth = new Date().getMonth() - dateFinishTask.getMonth();
    const diffYear = new Date().getFullYear() - dateFinishTask.getFullYear();
    let diffDate = dateOnly - new Date().getDate();
    if (diffYear === 0) {
      if (diffMonth !== 0) {
        diffDate = dateOnly + Number(this.TotalDayPerMonth[new Date().getMonth()]) - (new Date().getDate());
      }
      switch (true) {
        case diffDate === 0:
          this.translocoService.selectTranslate('Today').subscribe(translate => {
            result.displayDay = translate;
          });
          result.colorDay = '#058527';
          return result;
        case diffDate === 1:
          this.translocoService.selectTranslate('Tomorrow').subscribe( translate => {
            result.displayDay = translate;
          });
          result.colorDay = '#dc8315';
          return result;
        case diffDate >= 2 && diffDate <= 7:
          const location = this.DayOfWeek.indexOf(dateFinishTask.toString().split(' ')[0]);
          this.translocoService.selectTranslate('FullDate').subscribe(translate => {
            result.displayDay = translate[location];
          });
          result.colorDay = '#5b55f4';
          return result;
        default:
          result.displayDay = moment(dateFinishTask).locale(this.translocoService.getActiveLang()).format('MMM DD');
          result.colorDay = '#48b4aa';
          return result;
      }
    } else {
      result.displayDay = moment(dateFinishTask).locale(this.translocoService.getActiveLang()).format('MMM DD');
      result.colorDay = '#48b4aa';
      return result;
    }
  }

  public ReplaceByLanguage(data: Array<ModelChart>) {
    this.translocoService.selectTranslateObject('shortDateCalendar').subscribe(translate => {
      const DayOfWeekChart = translate;
      data.forEach((valueDate, index) => {
        valueDate.name = DayOfWeekChart[index];
      });
    });
  }
}
