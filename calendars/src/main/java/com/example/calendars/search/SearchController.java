package com.example.calendars.search;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.calendars.domain.CommentResponse;
import com.example.calendars.domain.model.DeviceUser;
import com.example.calendars.domain.model.User;
import com.example.calendars.service.UserService;

@RestController
public class SearchController {
	@Autowired
	private SearchService searchService;
	@Autowired
	private UserService userService;

	@GetMapping("test")
	public HashSet<SearchResultModel> SearchByClick(@RequestParam String searchWord) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User currentUser = userService.findByEmail(email);
		return searchService.getGroupWithTitle(searchWord, 10, currentUser.getId());
	}

	@GetMapping("search")
	public HashSet<SearchResultModel> ResultSearch(@RequestParam String prefix) {
		return searchService.getGroupWithTitle(prefix, 10, (long) 1);
	}
	
	@GetMapping("search/comment")
	public List<CommentResponse> ResultSearchComment(@RequestParam String prefix) {
		return searchService.getListCommentByWord(prefix, 0);
	}
	
	@GetMapping("search/task")
	public HashSet<SearchResultModel> ResultSearchTask(@RequestParam String prefix) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User currentUser = userService.findByEmail(email);
		return searchService.getTaskByWord(prefix, 10, currentUser.getId());
	}
	
	@MessageMapping("/search/receive")
	@SendToUser("/queue/search")
	public HashSet<SearchResultModel> searchMappingData(Principal principal,@Payload SearchReceiveModel current) {
		String[] getData = current.getUsername().split("/");
		DeviceUser currentDevice = userService.GetDeviceInfo(getData[1]);
		return searchService.getGroupWithTitle(current.getPrefix(), 10, currentDevice.getUserid());
	}

	@MessageMapping("/message")
	@SendToUser("/queue/reply")
	public String processMessageFromClient(@Payload String message, Principal principal) throws Exception {
		//messagingTemplate.convertAndSendToUser(principal.getName(), "/queue/reply", name);
		return message;
	}
}
