package com.example.calendars.validator;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.example.calendars.domain.Response;

@Component
public class TypeValidator {
	public void ValidatorTypeImage(String contextType, Response result) {
		if (contextType.equals("image/jpeg") || contextType.equals("image/png") || contextType.equals("image/svg+xml")
				|| contextType.equals("image/svg+xml")) {
			result = new Response();
		}else {
			result.setResponse(HttpStatus.NOT_ACCEPTABLE, "Wrong context Type");
		}
	}
}
