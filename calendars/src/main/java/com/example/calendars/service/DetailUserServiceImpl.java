package com.example.calendars.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.calendars.domain.model.DetailUser;
import com.example.calendars.repository.DetailUserRepository;

@Service
public class DetailUserServiceImpl implements DetailUserService {

	@Autowired
	private DetailUserRepository detailuser;

	@Override
	public void save(DetailUser image) {
		// TODO Auto-generated method stub
		detailuser.save(image);
	}

	@Override
	public void UpdateAvatar(Long avatarid, Long userid) {
		// TODO Auto-generated method stub
		detailuser.updateAvatar(avatarid, userid);
	}

	@Override
	public void UpdateProjectChart(Long firstid, Long secondid, Long userid) {
		// TODO Auto-generated method stub
		detailuser.updateGroupChart(firstid, secondid, userid);
	}

	@Override
	public void Updatebackground(String background, Long userid) {
		// TODO Auto-generated method stub
		detailuser.UpdateBackgroundUser(background, userid);
	}

	@Override
	public DetailUser FindByuserId(Long userid) {
		// TODO Auto-generated method stub
		return detailuser.findByUserId(userid);
	}

}
