import {Comments} from '../Comment';
import {Images} from '../Images';

export class CommentData {
  comments: Array<Comments>
  timePost: string;
  images: Array<Images>
}
