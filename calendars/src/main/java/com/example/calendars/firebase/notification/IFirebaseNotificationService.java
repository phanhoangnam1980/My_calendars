package com.example.calendars.firebase.notification;

import java.util.List;

import com.example.calendars.domain.model.NotifiTask;
import com.example.calendars.domain.model.User;

public interface IFirebaseNotificationService {
	List<String> TestsendNotification(AppNotification notification, User user);
	
	void DefaultSendNotification();
	
	void saveNotifyTask(NotifiTask task);
	
	void UpdateNotifyTask(NotifiTask task);
	
	void DeleteNotifyTask(NotifiTask task);
	
	NotifiTask FindByTaskid(Long taskid);
	
	void CustomTimeSendNotification();
}
