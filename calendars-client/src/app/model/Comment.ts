export class Comments {
  id: bigint;
  projectId: bigint;
  taskId: bigint;
  create: string;
  comment: string;
}
