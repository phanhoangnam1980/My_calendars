package com.example.calendars.webapi;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.calendars.domain.GroupSectionModel;
import com.example.calendars.domain.model.DetailUser;
import com.example.calendars.domain.model.Group;
import com.example.calendars.domain.model.Section;
import com.example.calendars.domain.model.User;
import com.example.calendars.service.ChannelService;
import com.example.calendars.service.DetailUserService;
import com.example.calendars.service.DetailsTaskMessageGroupService;
import com.example.calendars.service.SectionService;
import com.example.calendars.service.TaskService;
import com.example.calendars.service.UserService;

@RestController
public class ChannelController {
	@Autowired
	private ChannelService channelService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private UserService userService;
	@Autowired
	private SectionService sectionService;
	@Autowired
	private DetailsTaskMessageGroupService detailService;
	@Autowired
	private DetailUserService detailUserService;

	@PostMapping(value = "/saveproject")
	public Group addNewChannel(@RequestBody Group DataGiveBack) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		DataGiveBack.setUserID(CurrentUser.getId());
		DataGiveBack.setGrouptypeData((long) 1);
		channelService.save(DataGiveBack);
		Section databack = sectionService.saveDefault();
		detailService.saveSectionGroup(DataGiveBack.getGroupid(), databack.getId());
		return DataGiveBack;
	}

	@PostMapping(value = "/testGroup")
	public Group addNewsChannel(@RequestBody Group DataGiveBack) {
		Group returnData = new Group();
		returnData.setColorSpecs(DataGiveBack.getColorSpecs());
		returnData.setTitleGroupTask(DataGiveBack.getTitleGroupTask());
		return DataGiveBack;
	}

	@PostMapping(value = "/updateTittleProject")
	public Group updateTittleChannel(@RequestBody Group newTittleChange) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		Group groupChange = channelService.findByGroupId(newTittleChange.getGroupid());
		groupChange = channelService.UpdateTittleChannel(groupChange, CurrentUser.getId(), newTittleChange.getTitleGroupTask());
		return groupChange;
	}

	@GetMapping(value = "/AllCustomChannel")
	public List<Group> GetAllCustomGroupByUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		return channelService.GetAllCustomChannel(CurrentUser.getId());
	}

	@GetMapping(value = "/DefaultChannel")
	public Group GetDefaultGroupByUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		return channelService.GetDefaultChannel(CurrentUser.getId());
	}

	@GetMapping(value = "/Channel/{GroupChannelId}")
	public Group GetDataGroupChannel(@PathVariable Long GroupChannelId) {
		return channelService.findByGroupId(GroupChannelId);
	}

	@PostMapping(value = "/DeleteThisProject/{GroupDeleteId}")
	public void DeleteThisProjectChannel(@PathVariable Long GroupDeleteId) {
		taskService.DeleteTaskFromGroup(GroupDeleteId);
		channelService.DeleteChannel(GroupDeleteId);
	}

	@PutMapping(value = "/Channel")
	public void UpdateChannel(@RequestBody Group DataGroup) {
		channelService.UpdateChannel(DataGroup);
	}
	
	@GetMapping(value = "/channelSection")
	public List<GroupSectionModel> listgroupsection() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		return detailService.listGroupSectionUser(CurrentUser.getId());
	}
	
	@GetMapping(value = "/getNameChart")
	public List<Group> getNameChart() {
		List<Group> result = new ArrayList<Group>();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		DetailUser data = detailUserService.FindByuserId(CurrentUser.getId());
		result.add(channelService.findByGroupId(data.getFirstChart()));
		result.add(channelService.findByGroupId(data.getSecondChart()));
		return result;
	}
}
