package com.example.calendars.webapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.calendars.domain.model.Section;
import com.example.calendars.service.DetailsTaskMessageGroupService;
import com.example.calendars.service.SectionService;

@RestController
public class SectionController {

	@Autowired
	private SectionService sectionService;
	@Autowired
	private DetailsTaskMessageGroupService detailService;

	@PostMapping("/section/{groupId}")
	public Section savenewSection(@PathVariable Long groupId, @RequestBody Section savedata) {
		savedata.setDelete(false);
		sectionService.save(savedata);
		detailService.saveSectionGroup(groupId, savedata.getId());
		return savedata;
	}

	@GetMapping("/section/{dataId}")
	public Section getSectionById(@PathVariable Long dataId) {
		return sectionService.GetSectionById(dataId);
	}

	@PutMapping("/section")
	public Section updateSection(@RequestBody Section savedata) {
		sectionService.UpdateTitleSection(savedata);
		return savedata;
	}

	@PutMapping("/section/{dataId}")
	public void hideThisSection(@PathVariable Long dataId) {
		sectionService.HideSection(dataId, true);
	}

	@DeleteMapping("/section/{dataId}")
	public void DeleteThisSection(@PathVariable Long dataId) {
		sectionService.DeleteSection(dataId);
		detailService.DeleteTaskSection(dataId);
	}
	
	@GetMapping("/DefaultSection/{projectid}")
	public Section getDefaultSectionUser(@PathVariable Long projectid) {
		return sectionService.getDefaultSection(projectid);
	}
	
	@GetMapping("/SectionTask/{currentid}")
	public Section getCurrentSectionInfo(@PathVariable Long currentid) {
		return sectionService.FindSectionInfoByTask(currentid);
	}
}
