import {Component, OnInit} from '@angular/core';
import {TaskService} from '../../service/task.service';
import {TaskGroup} from '../../model/domain/TaskGroup';

@Component({
  selector: 'app-pintask',
  templateUrl: './pintask.component.html',
  styleUrls: ['./pintask.component.css']
})
export class PintaskComponent implements OnInit {
  public displayData: Array<TaskGroup>;

  constructor(private taskService: TaskService) {
    this.taskService.GetPinnedTask().subscribe((data: Array<TaskGroup>) => {
      this.displayData = data;
    });
  }

  ngOnInit(): void {
  }

}
