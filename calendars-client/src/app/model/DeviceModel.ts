export class DeviceModel {
  id: string;
  accessToken: string;
  language: string;
  userid: bigint;
  ip: string;
  token: string;
}
