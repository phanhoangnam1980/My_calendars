package com.example.calendars.domain;

import java.util.ArrayList;
import java.util.List;

public class TaskGroupSplitDataCompare {
	private List<TaskGroup> onGoingTask;

	private List<TaskGroup> OverDueTask;

	public List<TaskGroup> getOnGoingTask() {
		return onGoingTask;
	}

	public void setOnGoingTask(List<TaskGroup> onGoingTask) {
		this.onGoingTask = onGoingTask;
	}

	public List<TaskGroup> getOverDueTask() {
		return OverDueTask;
	}

	public void setOverDueTask(List<TaskGroup> overDueTask) {
		OverDueTask = overDueTask;
	}

	public TaskGroupSplitDataCompare() {
		super();
		setOverDueTask(new ArrayList<TaskGroup>());
		setOnGoingTask(new ArrayList<TaskGroup>());
	}

	public void addOnGoing(TaskGroup data) {
		this.onGoingTask.add(data);
	}

	public void addOverdue(TaskGroup data) {
		this.OverDueTask.add(data);
	}
}
