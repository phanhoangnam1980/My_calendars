package com.example.calendars.webapi;

import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.calendars.domain.CountTaskPerDate;
import com.example.calendars.domain.CustomChartModel;
import com.example.calendars.domain.CustomTaskGroupByDay;
import com.example.calendars.domain.Response;
import com.example.calendars.domain.TaskData;
import com.example.calendars.domain.TaskGroup;
import com.example.calendars.domain.TaskGroupSplitDataCompare;
import com.example.calendars.domain.TaskSaveModel;
import com.example.calendars.domain.model.TaskMessage;
import com.example.calendars.domain.model.User;
import com.example.calendars.service.DetailsTaskMessageGroupService;
import com.example.calendars.service.TaskService;
import com.example.calendars.service.UserService;
import com.example.calendars.validator.TaskValidator;

@RestController
public class TaskController {
	@Autowired
	private TaskService taskService;
	@Autowired
	private UserService userService;
	@Autowired
	private DetailsTaskMessageGroupService detailService;
	@Autowired
	private TaskValidator taskValidator;

	@PostMapping(value = "/savenewtask")
	public TaskMessage savenewTask(@RequestBody TaskSaveModel tasksave) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User currentUser = userService.findByEmail(email);
		TaskMessage newtask = new TaskMessage();
		newtask.setDescription(tasksave.getDescription());
		newtask.setTitle(tasksave.getTittle());
		newtask.setUserID(currentUser.getId());
		taskService.savenewtask(newtask, (tasksave.getDatedata() + " " + tasksave.getZoneId()));
		detailService.saveTaskInSection(newtask.getTaskid(), tasksave.getGroupId(), tasksave.getSectionId());
		return newtask;
	}

	@PostMapping(value = "/savesubtask")
	public TaskMessage saveSubTask(@RequestBody TaskSaveModel tasksave) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User currentUser = userService.findByEmail(email);
		TaskMessage newtask = new TaskMessage();
		newtask.setDescription(tasksave.getDescription());
		newtask.setTitle(tasksave.getTittle());
		newtask.setUserID(currentUser.getId());
		newtask.setParent(tasksave.getParentId());
		taskService.savenewtask(newtask, (tasksave.getDatedata() + " " + tasksave.getZoneId()));
		detailService.saveTaskInSection(newtask.getTaskid(), tasksave.getGroupId(), tasksave.getSectionId());
		return newtask;
	}

	@GetMapping(value = "/AllFinishTask/{groupRequest}/{pagenumner}")
	public List<TaskMessage> listAllFinishedTask(@PathVariable Long groupRequest, @PathVariable int pagenumner) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		List<TaskMessage> result = taskService.FindAllFinishedTask(groupRequest, CurrentUser.getId(), pagenumner);
		return result;
	}

	@PostMapping(value = "/AllTask")
	public List<TaskData> listAllOnGoingTask(@RequestParam("groupRequest") Long groupRequest,
			@RequestBody TaskSaveModel timezone) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		return taskService.FindAllTaskGroup(groupRequest, CurrentUser.getId(), timezone.getZoneId());
	}

	@PostMapping(value = "/AllTodayTaskGroup")
	public TaskGroupSplitDataCompare AllTodayTaskCustom(@RequestBody TaskSaveModel timezone) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		return taskService.AllTodayTaskWithGroup(CurrentUser.getId(), timezone.getZoneId());
	}

	@GetMapping(value = "/AllPinnedTask")
	public List<TaskGroup> ListPinnedTask() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		List<TaskGroup> result = taskService.GetDataTaskPinned(CurrentUser.getId(), CurrentUser.getZonetime());
		return result;
	}

	@DeleteMapping(value = "/DeleteTask/{currenttaskid}")
	public void DeleteThisTask(@PathVariable Long currenttaskid) {
		taskService.DeleteTask(currenttaskid);
		detailService.DeleteDetailByTask(currenttaskid);
	}

	@PostMapping(value = "/FinishedTask/{currenttaskid}")
	public void SetThisTaskAreFinished(@PathVariable Long currenttaskid) {
		taskService.UpdateTaskFinished(currenttaskid);
		detailService.updateTaskFinished(currenttaskid);
	}

	@GetMapping(value = "/Task/week")
	public List<CountTaskPerDate> DataTaskByWeek() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		return taskService.CountTaskInWeek(CurrentUser.getId(), CurrentUser.getZonetime());
	}

	@PatchMapping(value = "/Task/kinds")
	public Response SetNewOrderTask(@RequestParam("order") Long kinds, @RequestParam("taskId") Long task) {
		Response result = new Response();
		try {
			detailService.updateKindOfTask(kinds, task);
			result.setResponse(HttpStatus.OK, "done");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			taskValidator.ValidatorErrorDatabase(e, result);
		}
		return result;
	}

	@PatchMapping(value = "/Task/pin")
	public Response CreatePinTask(@RequestParam("taskId") Long task) {
		Response result = new Response();
		try {
			detailService.updateKindOfTask((long) 5, task);
			result.setResponse(HttpStatus.OK, "done");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			taskValidator.ValidatorErrorDatabase(e, result);
		}
		return result;
	}

	@PostMapping(value = "/DetailTask")
	public List<TaskData> DetailSubTask(@RequestBody TaskSaveModel data) {
		List<TaskData> rawData = new ArrayList<TaskData>();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		rawData = this.taskService.GetAllSubTaskFollowTaskId(data.getParentId(), CurrentUser.getId(),
				CurrentUser.getZonetime(), data.getGroupId());
		return rawData;
	}

	@GetMapping(value = "/TaskChart/{currentChart}/{secondChart}")
	public List<CustomChartModel> ChartByGroup(@PathVariable Long currentChart, @PathVariable Long secondChart) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		List<CustomChartModel> result = new ArrayList<CustomChartModel>();
		// result.addAll(taskService.CountFinishedTotalPerTask(currentChart, 0, 10));
		result.add(new CustomChartModel("firstChart",
				this.taskService.CountCompleteRemainTask(currentChart, CurrentUser.getZonetime())));
		// result.add(new CustomChartModel("percent",
		// taskService.DateGraphGroup(secondChart, 0, 10)));
		result.addAll(taskService.CountTotalRemain(secondChart));
		return result;
	}

	@GetMapping(value = "/TaskChart/tomorrow")
	public List<CustomChartModel> ChartTomorrow() {
		List<CustomChartModel> result = new ArrayList<CustomChartModel>();
		result.add(new CustomChartModel("percent", taskService.TaskGraphTotal()));
		return result;
	}

	@GetMapping("/TaskData/tomorrow")
	public List<TaskGroup> DataTomorrow() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		return this.taskService.DataUpComing(CurrentUser.getZonetime());
	}

	@GetMapping("/TestTaskChart")
	public List<CountTaskPerDate> testData() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		Long grouptest = (long) 1;
		return this.taskService.CountCompleteRemainTask(grouptest, CurrentUser.getZonetime());
	}

	@PutMapping("/Task")
	public void UpdateTask(@RequestBody TaskSaveModel tasksave) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		TaskMessage task = new TaskMessage();
		task.setTaskid(tasksave.getParentId());
		task.setTitle(tasksave.getTittle());
		task.setDescription(tasksave.getDescription());
		task.setDate(ZonedDateTime.parse(tasksave.getDatedata() + ' ' + CurrentUser.getZonetime(), DateTimeFormatter.ofPattern("E MMM dd yyyy HH:mm:ss xx")));
		taskService.UpdateTask(task, CurrentUser.getZonetime());
	}

	@PatchMapping("/Task/parent")
	public void UpdateParent(@RequestBody TaskMessage task) {
		taskService.UpdateParentId(task);
	}

	@PatchMapping("/Task/time")
	public void UpdateTimeFinished(@RequestBody TaskSaveModel tasksave) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		TaskMessage task = new TaskMessage();
		task.setTaskid(tasksave.getParentId());
		task.setTitle(tasksave.getTittle());
		task.setDescription(tasksave.getDescription());
		task.setDate(ZonedDateTime.parse(tasksave.getDatedata(), DateTimeFormatter.ofPattern("E MMM dd yyyy HH:mm:ss xx")));
		taskService.UpdateTimeFinished(task, CurrentUser.getZonetime());
	}

	@GetMapping("/Task/upcoming/{part}")
	public List<CustomTaskGroupByDay> upcomingTask(@PathVariable int part) {
		List<CustomTaskGroupByDay> result = new ArrayList<>();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		if (part == 0) {
			result = taskService.ListTaskTomorrow(part, CurrentUser.getId(), CurrentUser.getZonetime());
		} else {
			result = taskService.ListTaskTomorrowNextPage(part, CurrentUser.getId(), CurrentUser.getZonetime());
		}
		return result;
	}
}
