import {LoginService} from '../service/Login.service';
import {User} from '../model/User';
import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TranslocoService} from '@ngneat/transloco';
import {GoogleSigninService} from '../service/google-signin.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class LoginComponent implements OnInit {
  @Input()
  users: User;
  public messages: string;
  public error: string;
  public user: gapi.auth2.GoogleUser;

  constructor(private loginService: LoginService, private router: Router, private translocoService: TranslocoService,
              private googleSignInService: GoogleSigninService, private ref: ChangeDetectorRef) {
    this.users = new User();
  }

  ngOnInit(): void {
    this.googleSignInService.observable().subscribe(user => {
      this.user = user;
      this.ref.detectChanges();
    });
  }

  login() {
    this.loginService.login(this.users).subscribe(isValid => {
      if (isValid) {
        localStorage.setItem(
          'token', JSON.stringify({email: this.users.email})
        );
        localStorage.setItem('authentication', JSON.stringify(isValid));
        this.router.navigate(['app/today']).then();
      } else {
        this.translocoService.selectTranslate('errorLogin').subscribe(translate => {
          this.error = translate;
        });
      }
    });
  }

  signInWithGoogle() {
    this.googleSignInService.signIn();
  }
}
