package com.example.calendars.service;

import java.util.List;

import com.example.calendars.domain.model.Group;

public interface ChannelService {
	Group addNewGroupChannel(Group DataForNewChannel);

	List<Group> GetAllCustomChannel(Long userid);

	Group GetDefaultChannel(Long userid);

	Group UpdateTittleChannel(Group DataForUpdate, Long userid, String newGroupName);

	void DeleteChannel(Long ChannelDeleteID);

	void save(Group SaveChannel);

	Group findByGroupId(Long Groupid);

	Group makeDefaultGroup(Long userid);

	void UpdateChannel(Group newDataChannel);

	void UpdateCommentGroupChannel(int number, Long groupid);

	void createTwoCustomChannel(Long userid, Long firstid, Long secondid);
}
