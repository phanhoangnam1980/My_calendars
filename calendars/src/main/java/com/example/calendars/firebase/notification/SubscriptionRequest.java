package com.example.calendars.firebase.notification;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubscriptionRequest {
	private String subscriber;
	
	private String topic;
}
