package com.example.calendars.suggest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SuggestController {
	@Autowired
	private SuggestService suggestService;

	@MessageMapping("/suggest/receive")
	@SendToUser("/queue/suggest")
	public SuggestResultModel searchMappingData(Principal principal, @Payload SuggestReceiveModel current) {
		if (current.getPrefix() != null) {
			return suggestService.CaseLanguageSuggest(current);
		}
		return new SuggestResultModel();
	}

	@GetMapping("/testSuggestSetup")
	public void SetupSuggest() {
		List<String> testList = new ArrayList<String>();
		testList.add("Hôm nay::Today");
		testList.add("Ngày mai::Tomorrow");
		testList.add("lặp::Loop");
		testList.add("Mỗi ngày::everyday");
		testList.add("Mỗi buổi sáng::everymorning");
		testList.add("Mỗi buổi tối::everyevening");
		testList.add("Mỗi ngày cuối tuần::everyweekend");
		testList.add("Mỗi::every");
		testList.add("Tuần tới::nextweek");
		testList.add("Tháng tới::nextmonth");
		testList.add("giữa::mid");
		testList.add("Hôm nay lúc::Todayat");
		testList.add("Ngày mai lúc::Tomorrowat");
		testList.add("Vào buổi sáng::inthemorning");
		testList.add("Vào buổi chiều::intheafternoon");
		testList.add("Vào buổi tối::intheevening");
		suggestService.SetupDatabaseSearch(testList, "vi");
	}
}
