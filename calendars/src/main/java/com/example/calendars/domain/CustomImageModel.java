package com.example.calendars.domain;

import com.example.calendars.domain.model.Images;
import com.example.calendars.domain.model.Section;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomImageModel {
	private Images image;
	
	private TaskGroup task;
	
	private Section section;

	public CustomImageModel(Images image, TaskGroup task, Section section) {
		super();
		this.image = image;
		this.task = task;
		this.section = section;
	}

	public CustomImageModel(TaskGroup task, Section section) {
		super();
		this.task = task;
		this.section = section;
	}
}
