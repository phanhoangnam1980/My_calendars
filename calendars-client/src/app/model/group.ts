export class Group {
  groupid: bigint;
  titleGroupTask: string;
  colorSpecs: string;
  userID: bigint;
}
