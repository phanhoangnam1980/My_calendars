package com.example.calendars.webapi;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.calendars.domain.CommentResponse;
import com.example.calendars.domain.model.Comment;
import com.example.calendars.domain.model.Images;
import com.example.calendars.domain.model.User;
import com.example.calendars.firebase.IImageService;
import com.example.calendars.service.CommentService;
import com.example.calendars.service.UserService;

@RestController
public class CommentController {
	@Autowired
	private CommentService commentService;
	@Autowired
	private UserService userService;
	@Autowired
	private IImageService imageService;

	@PostMapping(value = "/saveComment")
	public CommentResponse SaveNewComment(@RequestBody CommentResponse datareceive) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		ZonedDateTime current = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.of(CurrentUser.getZonetime()));
		datareceive.getComments().get(0).setCreate(current);
		datareceive.getComments().get(0).setUserid(CurrentUser.getId());
		commentService.save(datareceive.getComments().get(0));
		for (int i = 0; i < datareceive.getImages().size(); i++) {
			datareceive.getImages().get(i).setComment(datareceive.getComments().get(0).getId());
			imageService.save(datareceive.getImages().get(i));
		}
		return datareceive;
	}

	@GetMapping(value = "/taskcomment/{postid}")
	public List<CommentResponse> GetByTaskId(@PathVariable Long postid) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		List<Comment> rawdata = commentService.getListByTask(postid, CurrentUser.getZonetime(), 0, 10);
		List<Images> rawImage = imageService.GetListImageFromCommentTask(postid);
		List<CommentResponse> result = commentService.processCommentService(rawdata, rawImage,
				CurrentUser.getZonetime());
		return result;
	}

	@GetMapping(value = "/projectcomment/{id}")
	public List<CommentResponse> GetByProject(@PathVariable Long id) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		org.springframework.security.core.userdetails.User currentUserDetail = (org.springframework.security.core.userdetails.User) authentication
				.getPrincipal();
		String email = currentUserDetail.getUsername();
		User CurrentUser = userService.findByEmail(email);
		List<Comment> rawdata = commentService.getListByProject(id, CurrentUser.getZonetime(), 0, 10);
		List<Images> rawImage = imageService.GetListImageFromCommentProject(id);
		List<CommentResponse> result = commentService.processCommentService(rawdata, rawImage,
				CurrentUser.getZonetime());
		return result;
	}

	@PutMapping(value = "/comment")
	public void updateComment(@RequestBody Comment comments) {
		commentService.updateContext(comments);
	}

	@DeleteMapping(value = "/comment/{id}")
	public void deleteComment(@PathVariable Long id) {
		commentService.deleteComment(id);
	}
}
