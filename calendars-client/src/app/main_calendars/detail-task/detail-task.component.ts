import {AfterViewInit, Component, Inject, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {TaskGroup} from '../../model/domain/TaskGroup';
import {Response} from '../../model/domain/Response';
import {TaskMessage} from '../../model/TaskMessage';
import {TaskService} from '../../service/task.service';
import {ShareDataService} from '../../service/ShareData.service';
import {Comments} from '../../model/Comment';
import {CommentService} from '../../service/comment.service';
import {BehaviorSubject} from 'rxjs';
import {SectionService} from '../../service/Section.service';
import {Section} from '../../model/Section';
import {NewModelTask} from '../../model/domain/newModelTask';
import {TaskData} from '../../model/domain/TaskData';
import {CommentData} from '../../model/domain/CommentData';
import {CustomTaskMessage} from '../../model/domain/customTaskMessage';
import {UserService} from '../../service/user.service';
import {User} from '../../model/User';
import {Images} from '../../model/Images';
import {ImageService} from '../../service/image.service';
import {environment} from '../../../environments/environment';
import {DetailImageComponent} from '../../detail-image/detail-image.component';
import {NotifiTask} from '../../model/NotifiTask';
import {translate, TranslocoService} from '@ngneat/transloco';

@Component({
  selector: 'app-detail-task',
  templateUrl: './detail-task.component.html',
  styleUrls: ['./detail-task.component.css']
})
export class DetailTaskComponent implements OnInit, OnDestroy, AfterViewInit {
  public displayTasK: TaskGroup;
  public checkfinished: boolean;
  public locationcheckFinished: bigint;
  public activeZone: number;
  public hideflag: boolean;
  public flagOnGoingAddTask: boolean;
  public SectionData: Section;
  public SubTask: Array<TaskData>;
  public CommentData: Array<CommentData>;
  public userPost: User;
  @Input()
  textarea: string;
  private flagNewComment: BehaviorSubject<boolean>;

  private ImageStorage: Array<Images>;
  public PreViewImages: Array<string>;
  // flag task finished
  public finishedMainTask: boolean;
  // in comment tab
  public flagShowTime: boolean;
  public flagButtonDelete: boolean;
  public location: number;
  // option choose
  public optionvalue: number;
  public numberChooseBefore: number;
  @ViewChild('menuSelect') trigger;
  // taskNotification
  private DataNotification: NotifiTask;
  public viewNotification: string;
  // edits task
  public DataSelect: NewModelTask;
  public statusSelect: string;
  public locationEditsTask: number;
  public locationsubTask: number;

  constructor(@Inject(MAT_DIALOG_DATA) private rawData: any, private taskService: TaskService,
              private shareData: ShareDataService, private dialogRef: MatDialogRef<DetailTaskComponent>,
              private commentService: CommentService, private sectionService: SectionService, private userService: UserService,
              private imageService: ImageService, public dialog: MatDialog, private translocoService: TranslocoService) {
    this.flagNewComment = new BehaviorSubject<boolean>(false);
    this.PreViewImages = new Array<string>();
    this.ImageStorage = new Array<Images>();
    this.statusSelect = '';
  }

  ngOnInit(): void {
    this.checkfinished = false;
    this.rawData.task.data.groupid = this.rawData.task.data.groupid.toString();
    console.log(this.rawData);
    this.displayTasK = JSON.parse(JSON.stringify(this.rawData.task));
    console.log(this.displayTasK);
    this.activeZone = 1;
    if (this.rawData.section == null) {
      this.sectionService.GetDetailSectionTask(this.displayTasK.data.id).subscribe((data: Section) => {
        this.SectionData = data;
      });
    } else {
      this.SectionData = this.rawData.section;
    }
    this.userService.getAvatarUsername().subscribe(result => {
      this.userPost = result;
    });
    this.refreshTask();
    // value Add select
    this.DataSelect = new NewModelTask();
    this.locationEditsTask = -4;
    this.locationsubTask = -4;
  }

  ngOnDestroy(): void {
    this.flagNewComment.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.shareData.currentCancelRequestNewTask.subscribe(statusCancel => {
      if (statusCancel === true) {
        this.DataSelect = new NewModelTask();
        this.statusSelect = '';
        this.locationEditsTask = -4;
        this.locationsubTask = -4;
      }
    });
    this.shareData.currentCancelRequestNewTask.subscribe(statusCancel => {
      if (statusCancel === true) {
        this.hideflag = false;
      }
    });
    this.shareData.CurrentCancelStatus.subscribe(result => {
      if (result === true) {
        this.statusSelect = '';
        this.locationEditsTask = -4;
        this.locationsubTask = -4;
      }
    });
    this.shareData.DataAddTask.subscribe((taskData:TaskMessage) => {
      if (taskData !== null) {
        this.refreshTask();
        if(this.statusSelect === 'currentDetail') {
          this.displayTasK.data.taskid = taskData.taskid;
          this.displayTasK.data.title = taskData.title;
          this.displayTasK.data.description = taskData.description;
          this.displayTasK.data.date = taskData.date;
          this.statusSelect='';
        }
      }
    });
    this.flagNewComment.subscribe(data => {
      if (data === true) {
        this.commentService.GetCommentByTaskId(this.displayTasK.data.taskid).subscribe(result => {
          this.CommentData = result;
        });
      }
    });
  }

  ChangeActiveSelect(zoneselect: number) {
    if (this.activeZone !== zoneselect) {
      this.activeZone = zoneselect;
      if (zoneselect === 1) {
        const data = new NewModelTask();
        data.groupId = Number(this.displayTasK.data.groupid);
        data.parentId = this.displayTasK.data.taskid;
        this.taskService.getSubTask(data).subscribe((result: Array<TaskData>) => {
          this.SubTask = result;
        });
      } else if (zoneselect === 2) {
        this.commentService.GetCommentByTaskId(this.displayTasK.data.taskid).subscribe((result: Array<CommentData>) => {
          this.CommentData = result;
        });
      }
    }
  }

  PinnedTask(idtask) {
    this.taskService.TaskPinned(idtask).subscribe((result: Response) => {
      switch (result.code) {
        case 200:
          this.shareData.setDataAddTask(new TaskMessage());
          break;
        case 507:
          break;
      }
    });
  }

  ChangePriority(idtask, priority: number) {
    console.log(priority);
    this.taskService.SetKindForTask(idtask, BigInt(priority)).subscribe((result: Response) => {
      switch (result.code) {
        case 200:
          this.shareData.setDataAddTask(new TaskMessage());
          break;
        case 507:
          break;
      }
    });
  }

  DeletedTask(idtask) {
    this.taskService.DeleteTask(idtask).subscribe(() => {
      this.shareData.setDataAddTask(new TaskMessage());
    });
  }

  enterCheckAnimation(idlocation) {
    this.checkfinished = true;
    this.locationcheckFinished = idlocation;
  }

  leaveCheckAnimation() {
    this.checkfinished = false;
  }

  TaskFinished(idtask) {
    console.log('in ');
    this.checkfinished = false;
    this.finishedMainTask = true;
    const taskCall = new TaskMessage();
    taskCall.taskid = idtask;
    this.shareData.setDataAddTask(taskCall);
    this.taskService.TaskFinished(idtask).subscribe();
  }

  TaskFinishedWithSub(idlocation, idlocationGroup, status, idtask) {
    console.log(idlocation + ' ' + idlocationGroup + ' ' + status + ' ' + idtask);
    if (status === 'first') {
      this.SubTask.splice(idlocation, 1);
      this.taskService.TaskFinished(idtask).subscribe();
    } else if (status === 'second') {
    }
  }

  closingDialog() {
    this.dialogRef.close();
  }

  autogrow() {
    const textArea = document.getElementById('textComment');
    textArea.style.overflow = 'hidden';
    textArea.style.height = 'auto';
    textArea.style.height = textArea.scrollHeight + 'px';
  }

  SaveNewComment() {
    const send = new CommentData();
    send.comments = new Array<Comments>();
    const commentSend = new Comments();
    commentSend.comment = this.textarea;
    commentSend.taskId = this.displayTasK.data.taskid;
    send.comments.push(commentSend);
    send.images = this.ImageStorage;
    this.commentService.SaveComment(send).subscribe();
    this.textarea = '';
    this.ImageStorage = new Array<Images>();
    this.RefreshNewComment();
  }

  RefreshNewComment() {
    this.PreViewImages = new Array<string>();
    setTimeout(() => {
      this.flagNewComment.next(true);
    }, 100);
  }

  openDataAddTask() {
    this.hideflag = true;
  }

  enterAddTask() {
    this.flagOnGoingAddTask = true;
  }

  outAddTask() {
    this.flagOnGoingAddTask = false;
  }

  ClickSubTask(idlocation, idlocationGroup, status) {
    let selectedData = new CustomTaskMessage();
    switch (status) {
      case 'first':
        selectedData = this.SubTask[idlocationGroup].listtask[idlocation];
        break;
      case 'second':
        selectedData = this.SubTask[idlocationGroup].subtask[idlocation];
        break;
    }
    this.displayTasK.overdue = selectedData.overdue;
    this.displayTasK.data.taskid = selectedData.task.taskid;
    this.displayTasK.data.title = selectedData.task.title;
    this.displayTasK.data.description = selectedData.task.description;
    this.displayTasK.data.date = selectedData.task.date;
    this.refreshTask();
  }

  refreshTask() {
    const dataSubTask = new NewModelTask();
    dataSubTask.groupId = Number(this.displayTasK.data.groupid);
    dataSubTask.parentId = this.displayTasK.data.taskid;
    this.taskService.getSubTask(dataSubTask).subscribe((result: Array<TaskData>) => {
      this.SubTask = result;
    });
    this.taskService.GetExtraNotification(this.displayTasK.data.taskid).subscribe((result: NotifiTask) => {
      this.DataNotification = result;
      if (result != null) {
        switch (result.type) {
          case 1:
            this.translocoService.selectTranslate('OptionHour').subscribe(hour => {
              this.viewNotification = result.duration + ' ' + hour;
            });
            break;
          case 2:
            this.translocoService.selectTranslate('OptionDay').subscribe(day => {
              this.viewNotification = result.duration + ' ' + day;
            });
            break;
          case 3:
            this.translocoService.selectTranslate('OptionMonth').subscribe(month => {
              this.viewNotification = result.duration + ' ' + month;
            });
            break;
        }
        this.translocoService.selectTranslate('BeforeDate').subscribe(translateDate => {
          this.viewNotification = translateDate + ' ' + this.viewNotification;
        });
      }
    });
  }

  // add image
  changeFileComment(event) {
    const value = event.target.files[0].name;
    // const contexttype = event.target.files[0].type;
    const fileName = typeof value === 'string' ? value.match(/[^\/\\]+$/)[0] : value[0];
    if (event.target.files[0].size < 6291456) {
      this.imageService.SendImage(event.target.files[0], fileName).subscribe((result: Response) => {
        this.AddDataImage(result.data);
      });
    }
  }

  AddDataImage(data) {
    this.ImageStorage.push(data);
    this.PreViewImages.push(environment.urlViewImage + data.thumbshot + '?alt=media');
  }

  mouseEnterShowTime(index: number) {
    this.flagShowTime = true;
    this.location = index;
  }

  mouseLeaveTime() {
    this.flagShowTime = false;
    this.location = -1;
  }

  mouseEnterShowButton(index: number) {
    this.flagButtonDelete = true;
    this.location = index;
  }

  mouseLeaveButton() {
    this.flagButtonDelete = false;
    this.location = -1;
  }

  DeletedComment(idcomment, location) {
    this.CommentData.splice(location, 1);
    this.commentService.DeleteComment(idcomment);
  }

  ShowBiggerImage(image: Images) {
    this.dialog.open(DetailImageComponent, {
      width: '1200px',
      data: image
    });
  }

  blockclose(event) {
    event.stopPropagation();
  }

  SaveNotification() {
    if (this.optionvalue != null) {
      const raw = new NotifiTask();
      raw.type = this.optionvalue;
      raw.duration = this.numberChooseBefore;
      raw.taskid = this.displayTasK.data.taskid;
      this.taskService.ExtraNotification(raw).subscribe();
      this.trigger.closeMenu();
      raw.type = null;
      raw.duration = null;
    }
  }

  CancelNotification() {
    this.trigger.closeMenu();
  }

  SelectEdits(status: string, loccation: number, section: number) {
    this.statusSelect = status;
    switch (status) {
      case 'currentDetail':
        this.DataSelect.tittle = this.displayTasK.data.title;
        this.DataSelect.datedata = this.displayTasK.data.date;
        this.DataSelect.description = this.displayTasK.data.description;
        this.DataSelect.groupId = Number(this.displayTasK.data.groupid);
        this.DataSelect.sectionId = this.rawData.section.id;
        this.DataSelect.parentId = this.displayTasK.data.taskid;
        break;
      case 'firstSubLayout':
        this.locationEditsTask = loccation;
        this.locationsubTask = section;
        const taskSelect = this.SubTask[loccation].listtask[section];
        this.DataSelect.tittle = taskSelect.task.title;
        this.DataSelect.datedata = taskSelect.task.date;
        this.DataSelect.description = taskSelect.task.description;
        this.DataSelect.groupId = Number(this.displayTasK.data.groupid);
        this.DataSelect.sectionId = this.rawData.section.id;
        this.DataSelect.parentId = taskSelect.task.taskid;
        break;
      case 'secondSubLayout':
        this.locationEditsTask = loccation;
        this.locationsubTask = section;
        const taskSecSelect = this.SubTask[loccation].subtask[section];
        this.DataSelect.tittle = taskSecSelect.task.title;
        this.DataSelect.datedata = taskSecSelect.task.date;
        this.DataSelect.description = taskSecSelect.task.description;
        this.DataSelect.groupId = Number(this.displayTasK.data.groupid);
        this.DataSelect.sectionId = this.rawData.section.id;
        this.DataSelect.parentId = taskSecSelect.task.taskid;
        break;
    }
  }
}
