import {Component, Input, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {ShareDataService} from '../../../service/ShareData.service';
import {UserService} from '../../../service/user.service';
import {Response} from '../../../model/domain/Response';
import {TranslocoService} from '@ngneat/transloco';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  public backgroundRandom: string;
  public messageError: string;
  @Input()
  public OldPassword: string;
  @Input()
  public password: string;
  @Input()
  public confirm: string;
  public colorScheme = {
    domain: ['#FF8A80',
      '#EA80FC',
      '#8C9EFF',
      '#80D8FF',
      '#A7FFEB',
      '#CCFF90',
      '#FFFF8D',
      '#FF9E80']
  };

  constructor(private dialogRef: MatDialogRef<ChangePasswordComponent>, private shareService: ShareDataService,
              private userService: UserService, private translocoService: TranslocoService) {
  }

  ngOnInit(): void {
    const random = Math.floor(Math.random() * 8);
    this.backgroundRandom = this.colorScheme.domain[random];
  }

  closingDialog() {
    this.dialogRef.close();
  }

  ConfirmChange() {
    this.userService.updatePassword(this.password, this.confirm, this.OldPassword).subscribe((result: Response) => {
      switch (result.code) {
        case 200:
          this.dialogRef.close();
          break;
        case 406:
          switch (result.message) {
            case 'Size.userForm.password.short':
              this.translocoService.selectTranslate('passwordShort').subscribe(translate => {
                this.messageError = translate;
              });
              break;
            case 'Size.userForm.password.long':
              this.translocoService.selectTranslate('passwordTooLong').subscribe(translate => {
                this.messageError = translate;
              });
              break;
            case 'Diff.userForm.passwordConfirm':
              this.translocoService.selectTranslate('ConfirmError').subscribe(translate => {
                this.messageError = translate;
              });
              break;
            case 'Diff.userForm.oldpassword':
              this.translocoService.selectTranslate('NotSameCurrentPassword').subscribe(translate => {
                this.messageError = translate;
              });
              break;
          }
          break;
      }
    });
  }
}
