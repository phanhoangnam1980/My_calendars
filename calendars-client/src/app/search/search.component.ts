import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CommentData} from '../model/domain/CommentData';
import {UserService} from '../service/user.service';
import {User} from '../model/User';
import {CommentService} from '../service/comment.service';
import {TaskService} from '../service/task.service';
import {SearchModel} from '../model/domain/SearchModel';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  public valueSearch: string;
  public active: number;
  public DataComment: Array<CommentData>;
  public userPost: User;
  public DataTask: Array<SearchModel>;
  public checkfinished: boolean;
  public locationcheckFinished: string;

  constructor(private route: ActivatedRoute, private userService: UserService, private commentService: CommentService,
              private taskService: TaskService) {
    this.active = 1;
    this.checkfinished = false;
  }

  ngOnInit(): void {
    this.valueSearch = this.route.snapshot.paramMap.get('value');
    console.log(this.valueSearch);
    this.userService.getAvatarUsername().subscribe(result => {
      this.userPost = result;
    });
    this.taskService.SearchDataTask(this.valueSearch).subscribe((result: Array<SearchModel>) => {
      this.DataTask = result;
    });
  }

  ChangeActive(order: number) {
    if (this.active !== order) {
      this.active = order;
      if (order === 1) {
        this.taskService.SearchDataTask(this.valueSearch).subscribe((result: Array<SearchModel>) => {
          this.DataTask = result;
        });
      } else if (order === 2) {
        this.commentService.GetCommentBySearch(this.valueSearch).subscribe((result: Array<CommentData>) => {
          this.DataComment = result;
        });
      }
    }
  }

  enterCheckAnimation(idlocation) {
    this.checkfinished = true;
    this.locationcheckFinished = idlocation;
  }

  leaveCheckAnimation() {
    this.checkfinished = false;
  }

  TaskFinishedOverDue(idtask) {
    console.log('in ');
    this.checkfinished = false;
    this.taskService.TaskFinished(idtask).subscribe();
  }
}
