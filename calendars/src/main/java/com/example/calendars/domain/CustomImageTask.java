package com.example.calendars.domain;

import com.example.calendars.domain.model.Images;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomImageTask {
	private Long id;

	private Long comment;

	private String thumbshot;

	private String viewImage;

	private String full;

	private String metadata;

	private Long taskid ;

	public CustomImageTask(Long id, Long comment, String thumbshot, String viewImage, String full, String metadata,
			Long taskid) {
		super();
		this.id = id;
		this.comment = comment;
		this.thumbshot = thumbshot;
		this.viewImage = viewImage;
		this.full = full;
		this.metadata = metadata;
		this.taskid = taskid;
	}
	
	public Images exportToImage() {
		Images result = new Images();
		result.setId(this.id);
		result.setComment(this.comment);
		result.setThumbshot(this.thumbshot);
		result.setViewImage(this.viewImage);
		result.setFull(this.full);
		result.setMetadata(this.metadata);
		return result;
	}
}
