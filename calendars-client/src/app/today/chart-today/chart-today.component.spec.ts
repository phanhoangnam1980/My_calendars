import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartTodayComponent } from './chart-today.component';

describe('ChartTodayComponent', () => {
  let component: ChartTodayComponent;
  let fixture: ComponentFixture<ChartTodayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartTodayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartTodayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
