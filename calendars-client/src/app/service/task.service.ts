import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenResponse} from '../model/TokenResponse';
import {Injectable} from '@angular/core';
import {NewModelTask} from '../model/domain/newModelTask';
import {DateTimeCalendarsService} from './DateTimeCalendars.service';
import {TaskSplitData} from '../model/domain/taskSplitData';
import {TaskData} from '../model/domain/TaskData';
import {environment} from '../../environments/environment';
import {SearchModel} from '../model/domain/SearchModel';
import {CustomChartModel} from '../model/domain/CustomChartModel';
import {TaskGroup} from '../model/domain/TaskGroup';
import {TaskMessage} from '../model/TaskMessage';
import {NotifiTask} from '../model/NotifiTask';

@Injectable()
export class TaskService {

  constructor(private http: HttpClient, private dateTimeData: DateTimeCalendarsService) {
  }

  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));

  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + this.token.accessToken
  });

  saveNewTask(setData: NewModelTask) {
    setData.zoneId = this.dateTimeData.getTimeZone();
    return this.http.post(environment.serverPort + '/savenewtask', JSON.stringify(setData), {headers: this.headers});
  }

  saveSubTask(setData: NewModelTask) {
    setData.zoneId = this.dateTimeData.getTimeZone();
    return this.http.post(environment.serverPort + '/savesubtask', JSON.stringify(setData), {headers: this.headers});
  }

  listAllFinishedTaskInGroup(GroudId: bigint, page: number) {
    return this.http.get<TaskSplitData>(environment.serverPort + '/AllFinishTask/' + GroudId + '/' + page, {headers: this.headers});
  }

  listAllTaskGroup(GroudId: number) {
    return this.http.post<Array<TaskData>>(environment.serverPort + '/AllTask' + '?groupRequest=' + GroudId
      , {zoneId: this.dateTimeData.getTimeZone()}, {headers: this.headers});
  }

  listTodayTaskGroup() {
    return this.http.post<TaskSplitData>(environment.serverPort + '/AllTodayTaskGroup', {zoneId: this.dateTimeData.getTimeZone()},
      {headers: this.headers});
  }

  DeleteTask(currenttaskid: bigint) {
    return this.http.delete(environment.serverPort + '/DeleteTask/' + currenttaskid, {headers: this.headers});
  }

  TaskFinished(currenttaskid: bigint) {
    return this.http.post(environment.serverPort + '/FinishedTask/' + currenttaskid, '', {headers: this.headers});
  }

  GetCountTaskData() {
    return this.http.get(environment.serverPort + '/Task/week', {headers: this.headers});
  }

  TaskPinned(task: bigint) {
    return this.http.patch(environment.serverPort + '/Task/pin?taskId=' + task, {headers: this.headers});
  }

  SetKindForTask(task: bigint, order: bigint) {
    return this.http.patch(environment.serverPort + '/Task/kinds?order=' + order + '&taskId=' + task, {header: this.headers});
  }

  GetPinnedTask() {
    return this.http.get(environment.serverPort + '/AllPinnedTask', {headers: this.headers});
  }

  SearchData(prefix: string) {
    return this.http.get<Array<SearchModel>>(environment.serverPort + '/search?prefix=' + prefix, {headers: this.headers});
  }

  SearchDataTask(prefix: string) {
    return this.http.get<Array<SearchModel>>(environment.serverPort + '/search/task?prefix=' + prefix, {headers: this.headers});
  }

  getSubTask(data: NewModelTask) {
    return this.http.post<Array<TaskData>>(environment.serverPort + '/DetailTask', data, {headers: this.headers});
  }

  getDataChartGroup(group: bigint, secondGroup: bigint) {
    return this.http.get<Array<CustomChartModel>>(environment.serverPort + '/TaskChart/' + group + '/' + secondGroup,
      {headers: this.headers});
  }

  GetDataChartTomorrow() {
    return this.http.get<Array<CustomChartModel>>(environment.serverPort + '/TaskChart/tomorrow', {headers: this.headers});
  }

  GetDataClick() {
    return this.http.get<Array<TaskGroup>>(environment.serverPort + '/TaskData/tomorrow', {headers: this.headers});
  }

  GetImageFromTask() {
    return this.http.get(environment.serverPort + '/taskImage', {headers: this.headers});
  }

  UpdateTask(task: TaskMessage) {
    const taskData = new NewModelTask();
    taskData.parentId = task.taskid;
    taskData.tittle = task.title;
    taskData.description = task.description;
    taskData.datedata = task.date;
    return this.http.put(environment.serverPort + '/Task', taskData, {headers: this.headers});
  }

  UpdateParent(task: TaskMessage) {
    return this.http.patch(environment.serverPort + '/Task', task, {headers: this.headers});
  }

  UpcomingTask(page: number) {
    return this.http.get(environment.serverPort + '/Task/upcoming/' + page, {headers: this.headers});
  }

  ExtraNotification(raw: NotifiTask) {
    return this.http.post(environment.serverPort + '/NotifiTask', raw, {headers: this.headers});
  }

  GetExtraNotification(id: bigint) {
    return this.http.get(environment.serverPort + '/task/notification/' + id, {headers: this.headers});
  }
}
