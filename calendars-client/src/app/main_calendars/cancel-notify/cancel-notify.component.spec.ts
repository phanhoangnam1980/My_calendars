import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelNotifyComponent } from './cancel-notify.component';

describe('CancelNotifyComponent', () => {
  let component: CancelNotifyComponent;
  let fixture: ComponentFixture<CancelNotifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancelNotifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelNotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
