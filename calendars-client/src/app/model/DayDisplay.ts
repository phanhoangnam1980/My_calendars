export class DayDisplay {
  Year: string;
  Month: string;
  Timezone: string;
}
