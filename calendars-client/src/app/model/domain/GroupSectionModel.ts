import {Section} from '../Section';

export class GroupSectionModel {
  groupid: bigint;
  titleGroup: string;
  icons: string;
  types: bigint;
  deSection: bigint;
  childrenSection: Array<Section>;
}
