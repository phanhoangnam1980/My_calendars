package com.example.calendars.domain.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "device_user")
@Getter
@Setter
public class DeviceUser {
	@Id
	private String id;

	@Column(columnDefinition = "TEXT")
	private String accessToken;

	@Column(name = "type_device")
	private String typeDevice;
	
	@Column(name = "language_device")
	private String language;
	
	@Column(name = "user_id")
	private Long userid;
	
	@Column(name ="ip_device")
	private String ip;
	
	@Column(name="user_agent")
	private String agent;
	
	@Column(name="tokenNotifi", columnDefinition = "TEXT")
	private String token;

	public DeviceUser() {
		super();
	}

	public DeviceUser(String id, String language) {
		super();
		this.id = id;
		this.language = language;
	}

	@Override
	public int hashCode() {
		return Objects.hash(accessToken, agent, ip, userid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeviceUser other = (DeviceUser) obj;
		return Objects.equals(accessToken, other.accessToken) && Objects.equals(agent, other.agent)
				&& Objects.equals(ip, other.ip) && Objects.equals(userid, other.userid);
	}
}
