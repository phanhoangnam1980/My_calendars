package com.example.calendars.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.example.calendars.domain.model.DetailUser;

public interface DetailUserRepository extends JpaRepository<DetailUser, Long> {
	@Query(value = "Select u from DetailUser u where u.userid=:current")
	DetailUser findByUserId(@Param("current") Long userid);

	@Transactional
	@Modifying
	@Query(value = "Update DetailUser u SET u.idImage=:avatar where u.userid=:current")
	void updateAvatar(@Param("avatar") Long avatarid, @Param("current") Long userid);

	@Transactional
	@Modifying
	@Query(value = "Update DetailUser u SET u.firstChart=:firstGroup , u.secondChart=:secondGroup where u.userid=:current")
	void updateGroupChart(@Param("firstGroup") Long firstid, @Param("secondGroup") Long secondid,
			@Param("current") Long userid);

	@Transactional
	@Modifying
	@Query(value = "Update DetailUser u SET u.backgroundImage=:background where u.userid=:current")
	void UpdateBackgroundUser(@Param("background") String background, @Param("current") Long userid);
}
