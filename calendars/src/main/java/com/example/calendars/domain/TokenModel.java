package com.example.calendars.domain;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TokenModel {
	private String accessToken;

	private String tokenType = "Bearer";

}
