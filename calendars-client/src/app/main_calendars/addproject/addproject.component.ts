import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Group} from '../../model/group';
import {ChannelService} from '../../service/Channel.service';
import {ShareDataService} from '../../service/ShareData.service';

@Component({
  selector: 'app-add-project',
  templateUrl: './addproject.component.html',
  styleUrls: ['./addproject.component.css']
})
export class AddprojectComponent implements OnInit {
  @Input()
  nameProject: string;
  @Input()
  colorProject: any;

  constructor(public dialogRef: MatDialogRef<AddprojectComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
              private channelService: ChannelService, private shareData: ShareDataService) {
    dialogRef.disableClose = true;
  }

  public userTestStatus = [
    {color: '#C0C0C0', namecolor: 'Silver'},
    {color: '#708090', namecolor: 'SlateGray'},
    {color: '#133337', namecolor: 'Elite Teal'},
    {color: '#8A2BE2', namecolor: 'BlueViolet'},
    {color: '#003366', namecolor: 'Prussian Blue'},
    {color: '#87CEFA', namecolor: 'LightSkyBlue'},
    {color: '#000080', namecolor: 'NavyBlue'},
    {color: '#0e2f44', namecolor: 'Blue Whale'},
    {color: '#EEB4B4', namecolor: 'RosyBrown'},
    {color: '#EBC79E', namecolor: 'New Tan'},
    {color: '#228B22', namecolor: 'ForestGreen'},
  ];

  ngOnInit(): void {
    this.colorProject = this.userTestStatus[0];
  }

  saveColorSelected(ColorSelected: any) {
    this.colorProject = ColorSelected;
    console.log(this.colorProject.color);
    console.log(this.colorProject.namecolor);
  }

  addNewProject() {
    const datagroup = new Group();
    datagroup.titleGroupTask = this.nameProject;
    datagroup.colorSpecs = this.colorProject.color;
    console.log(datagroup);
    this.channelService.saveNewChannel(datagroup).subscribe(result => {
      this.shareData.setAddProject(result);
    });
    this.dialogRef.close();
  }

  onCloseClick() {
    this.dialogRef.close();
  }
}
