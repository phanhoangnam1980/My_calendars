package com.example.calendars.repository;

import java.time.ZonedDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.calendars.domain.model.TaskMessage;

public interface TaskMessageRepository extends JpaRepository<TaskMessage, Long> {
	@Query(value = "Select u.* from task_messages u inner join detail_task_group d on u.taskid = d.taskid where d.groupid =:currentNeedFind and u.user_id = :currentuserid and u.was_delete is null and u.parent_id is null order by d.sectionid, d.kinds DESC", nativeQuery = true)
	List<TaskMessage> FindAllTaskInGroup(@Param("currentNeedFind") long groupID, @Param("currentuserid") long userid);

	@Query(value = "Select u.* from task_messages u inner join inner join detail_task_group d on u.taskid = d.taskid where d.groupid= :currentNeedFind and u.user_id = :currentuserid and u.was_delete is not null and u.parent_id is null", nativeQuery = true)
	List<TaskMessage> FindAllTaskFinishedInGroup(@Param("currentNeedFind") long groupID,
			@Param("currentuserid") long userid, Pageable pageable);

	@Query(value = "Select u from TaskMessage u Where u.UserID=:currentuserid")
	List<TaskMessage> FindAllTaskToday(@Param("currentuserid") long currentID);

	@Transactional
	@Modifying
	@Query(value = "UPDATE TaskMessage u SET u.IsDeleted = :wasfinish where u.taskid = :currenttaskid ")
	void UpdateTaskFinish(@Param("wasfinish") ZonedDateTime flagFinished, @Param("currenttaskid") Long CurrentTaskId);

	@Transactional
	@Modifying
	@Query(value = "DELETE from TaskMessage where taskid = :task_id")
	void DeleteTask(@Param("task_id") Long taskid);

	@Query(value = "Select count(*), u.date_finished from task_messages u where u.date_finished <:afterDate "
			+ "and u.date_finished >:beforeDate and u.user_id=:id and u.was_delete is null group by DATE(u.date_finished)", nativeQuery = true)
	List<Object[]> GetNumberTaskInWeek(@Param("beforeDate") ZonedDateTime beforeDate,
			@Param("afterDate") ZonedDateTime afterDate, @Param("id") Long userid);

	@Query(value = "Select u from TaskMessage u where u.parent=:currentFind")
	List<TaskMessage> FindSubTaskByParentId(@Param("currentFind") Long parentId);

	@Query(value = "Select u from TaskMessage u inner join DetailTaskGroup t2 on u.taskid = t2.task where u.parent is not null and t2.groups =:currentGroup and t2.kinds <> 1 order by t2.section,u.parent,t2.kinds")
	List<TaskMessage> GetSubTaskInGroup(@Param("currentGroup") Long currentGroup);

	@Query(value = "Select u from TaskMessage u where u.parent =:parrentIdTask order by u.parent , u.IsDeleted")
	List<TaskMessage> GetSubTaskByTaskId(@Param("parrentIdTask") Long parrent);

	@Transactional
	@Modifying
	@Query(value = "Update TaskMessage u Set u.comments=:currentnumber where u.taskid=:taskid")
	void UpdateCountComments(@Param("currentnumber") int number, @Param("taskid") Long tasks);

	@Transactional
	@Modifying
	@Query(value = "Update TaskMessage u Set u.subcount=:current where u.taskid=:taskid")
	void UpdateCountTask(@Param("current") int number, @Param("taskid") Long taskid);

	@Transactional
	@Modifying
	@Query(value = "Update TaskMessage u Set u.complete=:current where u.taskid=:taskid")
	void UpdateFinishedCount(@Param("current") int number, @Param("taskid") Long taskid);

	@Query(value = "Select u from TaskMessage u inner join DetailTaskGroup t2 on u.taskid = t2.task where u.parent is null and t2.groups =:currentGroup and t2.kinds <> 1 order by t2.kinds")
	List<TaskMessage> GetMaintaskWithGroup(@Param("currentGroup") Long currentGroup, Pageable pageable);

	@Query(value = "Select u from TaskMessage u inner join DetailTaskGroup t2 on u.taskid = t2.task where u.parent is null and t2.kinds <> 1 order by t2.kinds, u.taskid")
	List<TaskMessage> GetMaintask(Pageable pageable);

	@Query(value = "Select count(u.IsDeleted) as countData,count(*) as total from TaskMessage u inner join DetailTaskGroup t2 on u.taskid = t2.task where t2.groups=:currentGroup and u.date <:beforeDate and u.date >:afterDate")
	Object GetCountTaskByMonth(@Param("currentGroup") Long groupid, @Param("beforeDate") ZonedDateTime beforeDate,
			@Param("afterDate") ZonedDateTime afterDate);

	@Query(value = "Select u from TaskMessage u inner join DetailTaskGroup t2 on u.taskid = t2.task where t2.groups=:currentGroup and u.parent is null and t2.kinds <> 1 order by t2.kinds, u.taskid")
	List<TaskMessage> GetMaintaskByGroup(@Param("currentGroup") Long groupid, Pageable pageable);

	@Query(value = "Select t2.id,t2.task,t2.groups,t4.ColorSpecs,t4.TitleGroupTask,t4.grouptypeData,u.Title,u.Description,u.date,t4.UserID,u.UserID,t3.title,t3.id"
			+ " from TaskMessage u inner join DetailTaskGroup t2 on u.taskid= t2.task"
			+ " inner join Section t3 on t3.id = t2.section inner join Group t4 on t4.groupid=t2.groups where u.comments <> 0"
			+ " order by u.taskid")
	List<Object[]> GetListTaskHaveComment();

	@Query(value = "Select t2.id,t2.task,t2.groups,t4.ColorSpecs,t4.TitleGroupTask,t4.grouptypeData,u.Title,u.Description,u.date,t4.UserID,u.UserID,t3.title,t3.id"
			+ " from TaskMessage u inner join DetailTaskGroup t2 on u.taskid= t2.task"
			+ " inner join Section t3 on t3.id = t2.section inner join Group t4 on t4.groupid=t2.groups where u.date <:beforeDate and u.IsDeleted is null"
			+ " and u.UserID=:CurrentUser order by u.date")
	List<Object[]> GetListUpcoming(@Param("beforeDate") ZonedDateTime beforeDate, @Param("CurrentUser") Long user);

	@Query(value = "Select t2.id,t2.task,t2.groups,t4.ColorSpecs,t4.TitleGroupTask,t4.grouptypeData,u.Title,u.Description,u.date,t4.UserID,u.UserID,t3.title,t3.id"
			+ " from TaskMessage u inner join DetailTaskGroup t2 on u.taskid= t2.task"
			+ " inner join Section t3 on t3.id = t2.section inner join Group t4 on t4.groupid=t2.groups where u.date <:beforeDate and u.date >:afterDate and u.IsDeleted is null"
			+ " and u.UserID=:CurrentUser order by u.date")
	List<Object[]> GetListUpcomingWithDayAdd(@Param("beforeDate") ZonedDateTime beforeDate,
			@Param("afterDate") ZonedDateTime afterDate, @Param("CurrentUser") Long user);

	@Query(value = "Select u.Title,u.Description,t2.language,t2.token from TaskMessage u inner join DeviceUser t2 on u.UserID = t2.userid where t2.token is not null and u.IsDeleted"
			+ " is null and u.date >=:afterDate and u.date <=:beforeDate")
	List<Object[]> GetTaskNeedNotification(@Param("beforeDate") ZonedDateTime beforeDate,
			@Param("afterDate") ZonedDateTime afterDate);

	@Transactional
	@Modifying
	@Query(value = "Update TaskMessage u Set u.Title =:TitleTask ,u.Description=:DescriptionTask,u.date=:DateFinished where u.taskid =:CurrentTask")
	void UpdateTask(@Param("DateFinished") ZonedDateTime date, @Param("TitleTask") String title,
			@Param("DescriptionTask") String description, @Param("CurrentTask") Long taskid);

	@Transactional
	@Modifying
	@Query(value = "Update TaskMessage u Set u.parent=:newParent where u.taskid=:CurrentTask")
	void UpdateParent(@Param("newParent") Long parentid, @Param("CurrentTask") Long currentid);

	@Transactional
	@Modifying
	@Query(value = "Update TaskMessage u Set u.date=:timeFinished where u.taskid=:CurrentTask")
	void UpdateTaskFinished(@Param("timeFinished") ZonedDateTime date, @Param("CurrentTask") Long currentid);
}
