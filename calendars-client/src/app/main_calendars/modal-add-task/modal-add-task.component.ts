import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {SectionService} from '../../service/Section.service';
import {Group} from '../../model/group';
import {Section} from '../../model/Section';
import {CancelNotifyComponent} from '../cancel-notify/cancel-notify.component';
import {ShareDataService} from '../../service/ShareData.service';

@Component({
  selector: 'app-modal-add-task',
  templateUrl: './modal-add-task.component.html',
  styleUrls: ['./modal-add-task.component.css']
})
export class ModalAddTaskComponent implements OnInit {
  public DataChannelDefault: Group;
  public SectionDefault: Section;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any, private sectionService: SectionService, private shareData: ShareDataService,
              public dialogRef: MatDialogRef<ModalAddTaskComponent>, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.DataChannelDefault = this.data.channel;
    this.sectionService.GetDefaultSection(this.DataChannelDefault.groupid).subscribe(dataSection => {
      this.SectionDefault = dataSection;
    });
  }

  saveTask() {
    this.shareData.SetAddDataByModal(true);
    this.dialogRef.close();
  }

  DiscardTask() {
    this.dialog.open(CancelNotifyComponent, {
      width: '550px',
    })
  }
}
