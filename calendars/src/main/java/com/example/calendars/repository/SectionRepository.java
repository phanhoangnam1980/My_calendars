package com.example.calendars.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.calendars.domain.model.Section;

public interface SectionRepository extends JpaRepository<Section, Long> {
	@Query(value = "select u from Section u  where u.id=:currentFindId")
	Section findByIdSection(@Param("currentFindId") Long id);

	@Transactional
	@Modifying
	@Query(value = "update Section u set u.title =:newTitle where u.id=:currentSectionId")
	void updateSectionById(@Param("currentSectionId") Long id, @Param("newTitle") String newTitleSection);

	@Transactional
	@Modifying
	@Query(value = "update Section u set u.isDelete =:sectionHide where u.id=:currentSectionId")
	void HideSectionById(@Param("sectionHide") Boolean isHide, @Param("currentSectionId") Long id);

	@Transactional
	@Modifying
	@Query(value = "Delete from Section u where u.id=:currentSectionId ")
	void DeleteSection(@Param("currentSectionId") Long id);

	@Query(value = "select u from Section u inner join DetailTaskGroup t2 on t2.section = u.id "
			+ "where u.title = '' and t2.groups =:id and t2.task is null")
	Section getDefaultSection(@Param("id") Long projectid);
	
	
	@Query(value="Select u from Section u inner join DetailTaskGroup t2 on t2.groups = u.id where t2.id =:currentIDTask")
	Section findSectionFromTask(@Param("currentIDTask") Long id);
}
