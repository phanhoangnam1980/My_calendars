package com.example.calendars.service;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.calendars.domain.CountTaskPerDate;
import com.example.calendars.domain.CustomChartModel;
import com.example.calendars.domain.CustomImageModel;
import com.example.calendars.domain.CustomTaskGroupByDay;
import com.example.calendars.domain.CustomTaskMessage;
import com.example.calendars.domain.TaskData;
import com.example.calendars.domain.TaskGroup;
import com.example.calendars.domain.TaskGroupModel;
import com.example.calendars.domain.TaskGroupSplitDataCompare;
import com.example.calendars.domain.TaskModelex;
import com.example.calendars.domain.model.DetailTaskGroup;
import com.example.calendars.domain.model.Section;
import com.example.calendars.domain.model.TaskMessage;
import com.example.calendars.repository.DetailTaskMessageRepository;
import com.example.calendars.repository.TaskMessageRepository;

@Service
public class TaskServiceImpl implements TaskService {
	@Autowired
	private TaskMessageRepository taskMessageRepository;
	@Autowired
	private DetailTaskMessageRepository detailTaskMessageRepository;
	@Autowired
	private DetailsTaskMessageGroupService detailService;
	@Autowired
	private SectionService sectionService;

	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E MMM dd yyyy HH:mm:ss xx");

	private static final DateTimeFormatter formatterDateOnly = DateTimeFormatter.ofPattern("E MMM dd yyyy");

	@Override
	public void savenewtask(TaskMessage newtask, String timezone) {
		// TODO Auto-generated method stub
		newtask.setDate(ZonedDateTime.parse(timezone, formatter));
		taskMessageRepository.save(newtask);
		if (newtask.getParent() != null) {
			TaskMessage result = taskMessageRepository.getById(newtask.getParent());
			taskMessageRepository.UpdateCountTask(result.getSubcount() + 1, newtask.getParent());
		}
	}

	// remember data send as task in domain and date by formatter
	@Override
	public List<TaskData> FindAllTaskGroup(Long groupid, Long userid, String timezone) {
		// TODO Auto-generated method stub
		Long preSection = (long) -14;
		int indexSection = -1;
		List<TaskData> result = new ArrayList<TaskData>();
		List<TaskMessage> databack = taskMessageRepository.FindAllTaskInGroup(groupid, userid);
		List<DetailTaskGroup> dataDetailList = detailService.ListTaskInGroupBySection(groupid);
		int SizeData = dataDetailList.size();
		int SizeDataTask = databack.size();
		ZonedDateTime currentDateCheck = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.of(timezone));
		for (int index = 0; index < SizeData; index++) {
			// because data already sort by section so just need check if this was new and
			// put task in it
			if (dataDetailList.get(index).getSection() != preSection) {
				// if this new get data section and create new section
				TaskData newtaskData = new TaskData();
				preSection = dataDetailList.get(index).getSection();
				indexSection++;
				Section dataSection = sectionService.GetSectionById(preSection);
				newtaskData.setTitle(dataSection.getTitle());
				newtaskData.setSectionid(dataSection.getId());
				newtaskData.setListtask(new ArrayList<CustomTaskMessage>());
				result.add(newtaskData);
				// only check time and put task in
				continue;
			}
			if (SizeDataTask != 0 && SizeDataTask > (index - indexSection - 1)) {
				databack.get(index - indexSection - 1).setDate(
						databack.get(index - indexSection - 1).getDate().withZoneSameInstant(ZoneOffset.of(timezone)));
				if (databack.get(index - indexSection - 1).getDate().isBefore(currentDateCheck)) {
					result.get(indexSection).AddTask(
							new CustomTaskMessage(new TaskModelex(databack.get(index - indexSection - 1)), true));
				} else {
					result.get(indexSection).AddTask(
							new CustomTaskMessage(new TaskModelex(databack.get(index - indexSection - 1)), false));
				}
			}
		}
		return result;
	}

	@Override
	public List<TaskMessage> FindAllFinishedTask(Long groupid, Long userid, int pagenumber) {
		// TODO Auto-generated method stub
		List<TaskMessage> result = taskMessageRepository.FindAllTaskFinishedInGroup(groupid, userid,
				PageRequest.of(pagenumber, 20));
		return result;
	}

	@Override
	public TaskMessage UpdateTaskFinished(Long CurrentTaskId) {
		// TODO Auto-generated method stub
		taskMessageRepository.UpdateTaskFinish(ZonedDateTime.now(), CurrentTaskId);
		TaskMessage result = taskMessageRepository.getById(CurrentTaskId);
		if (result.getParent() != null) {
			TaskMessage raw = taskMessageRepository.getById(result.getParent());
			taskMessageRepository.UpdateFinishedCount(raw.getComplete() + 1, raw.getTaskid());
		}
		return result;
	}

	@Override
	public void DeleteTaskFromGroup(Long groupIdDelete) {
		// TODO Auto-generated method stub
		List<DetailTaskGroup> datatask = detailTaskMessageRepository.FindTaskInGroup(groupIdDelete);
		if (datatask.size() != 0) {
			for (DetailTaskGroup i : datatask) {
				taskMessageRepository.DeleteTask(i.getTask());
			}
		}
	}

	@Override
	public void DeleteTask(Long CurrentTaskId) {
		// TODO Auto-generated method stub
		taskMessageRepository.DeleteTask(CurrentTaskId);
	}

	@Override
	public String processDateToStandardTime(String DateGive, String timezone) {
		// TODO Auto-generated method stub
		ZonedDateTime timeclientzone = ZonedDateTime.parse(DateGive + " " + timezone, formatter);
		return timeclientzone.withZoneSameInstant(ZoneOffset.of("+00:00")).format(formatter);
	}

	@Override
	public TaskGroupSplitDataCompare AllTodayTaskWithGroup(Long userid, String timezone) {
		// TODO Auto-generated method stub
		TaskGroupSplitDataCompare result = new TaskGroupSplitDataCompare();
		List<TaskGroupModel> databack = detailService.findDataUserToday(userid, timezone);
		ZonedDateTime currentDateCheck = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.of(timezone));
		LocalDate todayDate = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.of(timezone)).toLocalDate();
		for (TaskGroupModel i : databack) {
			if (LocalDate.parse(i.getDate().substring(0, 15), formatterDateOnly).isEqual(todayDate)) {
				if (ZonedDateTime.parse(i.getDate(), formatter).isBefore(currentDateCheck)) {
					result.addOnGoing(new TaskGroup(i, true));
				} else {
					result.addOnGoing(new TaskGroup(i, false));
				}
			} else {
				result.addOverdue(new TaskGroup(i, true));
			}
		}
		return result;
	}

	@Override
	public List<CountTaskPerDate> CountTaskInWeek(Long userid, String timezone) {
		// TODO Auto-generated method stub
		List<CountTaskPerDate> result = new ArrayList<>();
		int DayInWeek = ZonedDateTime.now().getDayOfWeek().getValue();
		ZonedDateTime currentTime = LocalDate.now().atStartOfDay(ZoneOffset.of(timezone));
		ZonedDateTime BeforeDate = currentTime.minusDays(DayInWeek - 1);
		ZonedDateTime AfterDate = currentTime.plusDays(7 - DayInWeek).plusDays(1).toLocalDate()
				.atStartOfDay(ZoneOffset.of(timezone)).minusMinutes(1);
		result.add(new CountTaskPerDate(0, "Mon"));
		result.add(new CountTaskPerDate(0, "Tue"));
		result.add(new CountTaskPerDate(0, "Wed"));
		result.add(new CountTaskPerDate(0, "Thu"));
		result.add(new CountTaskPerDate(0, "Fri"));
		result.add(new CountTaskPerDate(0, "Sat"));
		result.add(new CountTaskPerDate(0, "Sun"));
		List<Object[]> rawData = taskMessageRepository.GetNumberTaskInWeek(BeforeDate, AfterDate, userid);
		for (Object[] dataObject : rawData) {
			Timestamp date = (Timestamp) dataObject[1];
			int replace = date.toLocalDateTime().getDayOfWeek().getValue() - 1;
			result.set(replace, new CountTaskPerDate(Integer.parseInt(dataObject[0].toString()), formatterDateOnly
					.format(date.toLocalDateTime().atOffset(ZoneOffset.of(timezone))).substring(0, 4)));
		}
		return result;
	}

	@Override
	public List<TaskGroup> GetDataTaskPinned(Long userid, String timezone) {
		// TODO Auto-generated method stub
		List<TaskGroup> result = new ArrayList<TaskGroup>();
		ZonedDateTime currentDateCheck = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.of(timezone));
		List<TaskGroupModel> raw = detailService.getPinnedTask(userid, timezone, 0);
		for (TaskGroupModel i : raw) {
			if (ZonedDateTime.parse(i.getDate(), formatter).isBefore(currentDateCheck)) {
				result.add(new TaskGroup(i, true));
			} else {
				result.add(new TaskGroup(i, false));
			}
		}
		return result;
	}

	@Override
	public List<TaskData> GetAllSubTaskFollowTaskId(Long taskid, Long userid, String timezone, Long groupid) {
		// TODO Auto-generated method stub
		List<TaskData> result = new ArrayList<TaskData>();
		HashMap<Long, Integer> sortData = new HashMap<Long, Integer>();
		int sizerawData, atSub = -1;
		List<TaskMessage> subTaskfirstLayer = taskMessageRepository.GetSubTaskByTaskId(taskid);
		List<TaskMessage> rawDataSubTask = taskMessageRepository.GetSubTaskInGroup(groupid);
		sizerawData = rawDataSubTask.size();
		ZonedDateTime currentDateCheck = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.of(timezone));
		if (subTaskfirstLayer.size() != 0) {
			for (int i = 0; i < sizerawData; i++) {
				if (taskid == rawDataSubTask.get(i).getParent()) {
					atSub++;
					TaskData newtaskData = new TaskData();
					newtaskData.setListtask(new ArrayList<CustomTaskMessage>());
					result.add(newtaskData);
					subTaskfirstLayer.get(atSub).setDate(
							subTaskfirstLayer.get(atSub).getDate().withZoneSameInstant(ZoneOffset.of(timezone)));
					if (subTaskfirstLayer.get(atSub).getDate().isBefore(currentDateCheck)) {
						result.get(atSub)
								.AddTask(new CustomTaskMessage(new TaskModelex(subTaskfirstLayer.get(atSub)), true));
					} else {
						result.get(atSub)
								.AddTask(new CustomTaskMessage(new TaskModelex(subTaskfirstLayer.get(atSub)), false));
					}
					result.get(atSub).setSubtask(new ArrayList<CustomTaskMessage>());
					sortData.put(subTaskfirstLayer.get(atSub).getTaskid(), atSub);
				}
				if (taskid != rawDataSubTask.get(i).getParent()) {
					if (sortData.get(rawDataSubTask.get(i).getParent()) != null) {
						int location = sortData.get(rawDataSubTask.get(i).getParent());
						rawDataSubTask.get(i)
								.setDate(rawDataSubTask.get(i).getDate().withZoneSameInstant(ZoneOffset.of(timezone)));
						if (rawDataSubTask.get(i).getDate().isBefore(currentDateCheck)) {
							result.get(location).getSubtask()
									.add(new CustomTaskMessage(new TaskModelex(rawDataSubTask.get(i)), true));
						} else {
							result.get(location).getSubtask()
									.add(new CustomTaskMessage(new TaskModelex(rawDataSubTask.get(i)), false));
						}
					}
				}
			}
		}
		return result;
	}

	@Override
	public void UpdateCommentTask(int number, Long taskid) {
		// TODO Auto-generated method stub
		TaskMessage raw = taskMessageRepository.getById(taskid);
		taskMessageRepository.UpdateCountComments(raw.getComments() + number, taskid);
	}

	@Override
	public List<CountTaskPerDate> DateGraphGroup(Long groupid, int pagenumber, int limit) {
		// TODO Auto-generated method stub
		List<CountTaskPerDate> result = new ArrayList<CountTaskPerDate>();
		List<TaskMessage> rawData = taskMessageRepository.GetMaintaskWithGroup(groupid,
				PageRequest.of(pagenumber, limit));
		for (TaskMessage i : rawData) {
			double percent;
			if (i.getSubcount() != 0) {
				percent = ((double) i.getComplete()) / i.getSubcount() * 100;
			} else {
				percent = 0;
			}
			result.add(new CountTaskPerDate((int) percent, i.getTitle() + "#" + i.getTaskid()));
		}
		return result;
	}

	@Override
	public List<CountTaskPerDate> TaskGraphTotal() {
		// TODO Auto-generated method stub
		List<CountTaskPerDate> result = new ArrayList<CountTaskPerDate>();
		List<TaskMessage> rawData = taskMessageRepository.GetMaintask(PageRequest.of(0, 10));
		for (int i = 0; i < rawData.size(); i++) {
			double percent;
			if (rawData.get(i).getSubcount() != 0) {
				percent = ((double) rawData.get(i).getComplete()) / (rawData.get(i).getSubcount() + 1) * 100;
			} else {
				percent = 1;
			}
			result.add(new CountTaskPerDate((int) percent,
					rawData.get(i).getTitle() + "#" + rawData.get(i).getTaskid() + "#" + i));
		}
		return result;
	}

	@Override
	public List<CustomChartModel> CountTotalRemain(Long groupid) {
		// TODO Auto-generated method stub
		List<CustomChartModel> result = new ArrayList<>();
		List<TaskMessage> rawData = taskMessageRepository.GetMaintaskByGroup(groupid, PageRequest.of(0, 10));
		for (int i = 0; i < rawData.size(); i++) {
			int complete = 0, total = 0;
			if (rawData.get(i).getSubcount() != 0) {
				complete = rawData.get(i).getComplete();
				total = rawData.get(i).getSubcount() + 1 - complete;
			} else {
				total = 1;
			}
			List<CountTaskPerDate> series = new ArrayList<>();
			series.add(new CountTaskPerDate(complete, "complete"));
			series.add(new CountTaskPerDate(total, "totalleft"));
			result.add(new CustomChartModel(rawData.get(i).getTitle() + "#" + rawData.get(i).getTaskid(), series));
		}
		return result;
	}

	@Override
	public List<TaskGroup> DataUpComing(String timezone) {
		// TODO Auto-generated method stub
		List<TaskGroup> result = new ArrayList<TaskGroup>();
		List<TaskGroupModel> rawData = detailService.getDataUpcoming(timezone);
		for (TaskGroupModel i : rawData) {
			result.add(new TaskGroup(i, false));
		}
		return result;
	}

	@Override
	public List<CustomChartModel> CountFinishedTotalPerTask(Long groupid, int pagenumber, int limit) {
		// TODO Auto-generated method stub
		List<CustomChartModel> result = new ArrayList<CustomChartModel>();
		List<TaskMessage> rawData = taskMessageRepository.GetMaintaskWithGroup(groupid,
				PageRequest.of(pagenumber, limit));
		result.add(new CustomChartModel("finished", new ArrayList<CountTaskPerDate>()));
		result.add(new CustomChartModel("Total", new ArrayList<CountTaskPerDate>()));
		for (int i = 0; i < rawData.size(); i++) {
			result.get(0).getSeries().add(new CountTaskPerDate(rawData.get(i).getComplete(),
					rawData.get(i).getTitle() + "#" + rawData.get(i).getTaskid()));
			result.get(1).getSeries().add(new CountTaskPerDate(rawData.get(i).getSubcount() + 1,
					rawData.get(i).getTitle() + "#" + rawData.get(i).getTaskid()));
		}
		return result;
	}

	@Override
	public List<CountTaskPerDate> CountCompleteRemainTask(Long groupid, String zone) {
		// TODO Auto-generated method stub
		int valueCount = 0, total;
		List<CountTaskPerDate> result = new ArrayList<CountTaskPerDate>();
		ZonedDateTime lastDateOfMonth = ZonedDateTime.now(ZoneOffset.of(zone));
		ZonedDateTime firstDateOfMonth = ZonedDateTime.now(ZoneOffset.of(zone));
		TemporalAdjuster temporalLastDay = TemporalAdjusters.lastDayOfMonth();
		TemporalAdjuster temporalFirstDay = TemporalAdjusters.firstDayOfMonth();
		firstDateOfMonth = firstDateOfMonth.with(temporalFirstDay);
		lastDateOfMonth = lastDateOfMonth.with(temporalLastDay);
		Object[] rawData = (Object[]) taskMessageRepository.GetCountTaskByMonth(groupid, lastDateOfMonth,
				firstDateOfMonth);
		valueCount = Integer.parseInt(rawData[0].toString());
		total = Integer.parseInt(rawData[1].toString());
		result.add(new CountTaskPerDate(valueCount, "task complete"));
		result.add(new CountTaskPerDate(total - valueCount, "task left"));
		return result;
	}

	@Override
	public List<TaskGroup> ListTaskHaveComment(String timezone) {
		// TODO Auto-generated method stub
		String group;
		String description;
		List<TaskGroup> result = new ArrayList<>();
		List<Object[]> raw = taskMessageRepository.GetListTaskHaveComment();
		for (Object[] dataObject : raw) {
			ZonedDateTime date = (ZonedDateTime) dataObject[8];
			date = date.withZoneSameInstant(ZoneOffset.of(timezone));
			if (!dataObject[11].toString().isEmpty()) {
				group = dataObject[4].toString() + "/" + dataObject[11].toString();
			} else {
				group = dataObject[4].toString();
			}
			if (dataObject[7] == null) {
				description = "";
			} else {
				description = dataObject[7].toString();
			}
			result.add(new TaskGroup(new TaskGroupModel(Long.parseLong(dataObject[0].toString()),
					Long.parseLong(dataObject[1].toString()), Long.parseLong(dataObject[2].toString()),
					dataObject[3].toString(), group, Long.parseLong(dataObject[5].toString()), dataObject[6].toString(),
					description, formatter.format(date), Long.parseLong(dataObject[9].toString()),
					Long.parseLong(dataObject[10].toString()), Long.parseLong(dataObject[12].toString())), false));
		}
		return result;
	}

	@Override
	public List<Object[]> getListUpcomingNotification(ZonedDateTime afterTime, ZonedDateTime beforeTime) {
		// TODO Auto-generated method stub
		return taskMessageRepository.GetTaskNeedNotification(beforeTime, afterTime);
	}

	@Override
	public void UpdateTask(TaskMessage task, String timezone) {
		// TODO Auto-generated method stub
		taskMessageRepository.UpdateTask(task.getDate(), task.getTitle(), task.getDescription(), task.getTaskid());
	}

	@Override
	public void UpdateParentId(TaskMessage task) {
		// TODO Auto-generated method stub
		taskMessageRepository.UpdateParent(task.getParent(), task.getTaskid());
	}

	@Override
	public void UpdateTimeFinished(TaskMessage task, String timezone) {
		// TODO Auto-generated method stub
		taskMessageRepository.UpdateTaskFinished(task.getDate(), task.getTaskid());
	}

	@Override
	public List<CustomTaskGroupByDay> ListTaskTomorrow(int pagenumber, Long userid, String zonetime) {
		// TODO Auto-generated method stub
		int day = 1;
		String group;
		String description;
		ZonedDateTime previous = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.of(zonetime));
		List<CustomTaskGroupByDay> result = new ArrayList<>();
		ZonedDateTime lastday = ZonedDateTime.now().plusDays((pagenumber + 1) * 21);
		List<Object[]> raw = taskMessageRepository.GetListUpcoming(lastday, userid);
		result = createDayPart(0, 21);
		for (Object[] dataObject : raw) {
			ZonedDateTime date = (ZonedDateTime) dataObject[8];
			date = date.withZoneSameInstant(ZoneOffset.of(zonetime));
			if (!dataObject[11].toString().isEmpty()) {
				group = dataObject[4].toString() + "/" + dataObject[11].toString();
			} else {
				group = dataObject[4].toString();
			}
			if (dataObject[7] == null) {
				description = "";
			} else {
				description = dataObject[7].toString();
			}
			TaskGroup data = new TaskGroup(new TaskGroupModel(Long.parseLong(dataObject[0].toString()),
					Long.parseLong(dataObject[1].toString()), Long.parseLong(dataObject[2].toString()),
					dataObject[3].toString(), group, Long.parseLong(dataObject[5].toString()), dataObject[6].toString(),
					description, formatter.format(date), Long.parseLong(dataObject[9].toString()),
					Long.parseLong(dataObject[10].toString()), Long.parseLong(dataObject[12].toString())), false);
			Section section = new Section();
			section.setId(data.getData().getSectionId());
			section.setTitle(data.getData().getTitleSection());
			if (date.toLocalDate().isBefore(previous.toLocalDate())) {
				result.get(0).addTaskToList(new CustomImageModel(data, section));
			} else {
				if (!date.toLocalDate().isEqual(previous.toLocalDate())) {
					long durationTime = Duration
							.between(previous.toLocalDate().atStartOfDay(), date.toLocalDate().atStartOfDay())
							.toDaysPart();
					day = (int) (1 + durationTime);
					result.get(day).addTaskToList(new CustomImageModel(data, section));
				} else {
					result.get(day).addTaskToList(new CustomImageModel(data, section));
				}
			}
		}
		return result;
	}

	@Override
	public List<CustomTaskGroupByDay> ListTaskTomorrowNextPage(int pagenumber, Long userid, String zonetime) {
		// TODO Auto-generated method stub
		int day = 0;
		String group;
		String description;
		List<CustomTaskGroupByDay> result = new ArrayList<>();
		ZonedDateTime firstday = ZonedDateTime.now().plusDays((pagenumber) * 21);
		ZonedDateTime lastday = ZonedDateTime.now().plusDays((pagenumber + 1) * 21);
		List<Object[]> raw = taskMessageRepository.GetListUpcomingWithDayAdd(lastday, firstday, userid);
		ZonedDateTime previous = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.of(zonetime));
		result = createDayPart(pagenumber * 21, (pagenumber + 1) * 21);
		for (Object[] dataObject : raw) {
			ZonedDateTime date = (ZonedDateTime) dataObject[8];
			date = date.withZoneSameInstant(ZoneOffset.of(zonetime));
			if (!dataObject[11].toString().isEmpty()) {
				group = dataObject[4].toString() + "/" + dataObject[11].toString();
			} else {
				group = dataObject[4].toString();
			}
			if (dataObject[7] == null) {
				description = "";
			} else {
				description = dataObject[7].toString();
			}
			TaskGroup data = new TaskGroup(new TaskGroupModel(Long.parseLong(dataObject[0].toString()),
					Long.parseLong(dataObject[1].toString()), Long.parseLong(dataObject[2].toString()),
					dataObject[3].toString(), group, Long.parseLong(dataObject[5].toString()), dataObject[6].toString(),
					description, formatter.format(date), Long.parseLong(dataObject[9].toString()),
					Long.parseLong(dataObject[10].toString()), Long.parseLong(dataObject[12].toString())), false);
			Section section = new Section();
			section.setId(data.getData().getSectionId());
			section.setTitle(data.getData().getTitleSection());
			if (!date.toLocalDate().isEqual(previous.toLocalDate())) {
				day++;
				result.add(new CustomTaskGroupByDay(day));
				result.get(day).addTaskToList(new CustomImageModel(data, section));
			} else {
				result.get(day).addTaskToList(new CustomImageModel(data, section));
			}
		}
		return result;
	}

	private List<CustomTaskGroupByDay> createDayPart(int dayfirst, int daysecond) {
		List<CustomTaskGroupByDay> result = new ArrayList<>();
		if (dayfirst == 0) {
			result.add(new CustomTaskGroupByDay(-1));
		}
		for (int i = dayfirst; i < daysecond; i++) {
			result.add(new CustomTaskGroupByDay(i));
		}
		return result;
	}
}
