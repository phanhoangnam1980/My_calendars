package com.example.calendars.suggest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuggestResultModel {
	private String result;
	
	private String select;
}
