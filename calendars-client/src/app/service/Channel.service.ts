import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenResponse} from '../model/TokenResponse';
import {Observable} from 'rxjs';
import {Group} from '../model/group';
import {environment} from '../../environments/environment';

@Injectable()
export class ChannelService {
  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));

  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + this.token.accessToken
  });

  constructor(private http: HttpClient) {
  }

  saveNewChannel(DataGroup: Group): Observable<object> {
    return this.http.post<Group>(environment.serverPort + '/saveproject', JSON.stringify({
      colorSpecs: DataGroup.colorSpecs,
      titleGroupTask: DataGroup.titleGroupTask
    }), {headers: this.headers});
  }

  updatetitleChannel(currentGroupId: bigint, newtitle: string) {
    return this.http.post<Group>(environment.serverPort + '/updatetitleProject', JSON.stringify({
      groupid: currentGroupId,
      titleGroupTask: newtitle
    }), {headers: this.headers});
  }

  updateChannel(dataGroup: Group) {
    return this.http.put(environment.serverPort + '/Channel', JSON.stringify(dataGroup), {headers: this.headers});
  }

  DeleteThisChannel(currentGroupId: bigint) {
    return this.http.post(environment.serverPort + '/DeleteThisProject/' + currentGroupId, '', {headers: this.headers});
  }

  GetListCustomChannel() {
    return this.http.get<Group[]>(environment.serverPort + '/AllCustomChannel', {headers: this.headers});
  }

  GetDefaultChannel() {
    return this.http.get<Group>(environment.serverPort + '/DefaultChannel', {headers: this.headers});
  }

  GetInfoChannel(currentId: number) {
    return this.http.get(environment.serverPort + '/Channel/' + currentId, {headers: this.headers});
  }

  GetDataTaskByWeek() {
    return this.http.get(environment.serverPort + '/Task/week', {headers: this.headers});
  }

  GetListChannelSection() {
    return this.http.get(environment.serverPort + '/channelSection', {headers: this.headers});
  }

  GetNameChannel() {
    return this.http.get(environment.serverPort + '/getNameChart', {headers: this.headers});
  }
}
